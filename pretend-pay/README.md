# Pretend-Pay Single-Page App

## Usage

The application will login automatically upon first use in the current session. Subsequent logins can be achieved using any email and password combination. No data is actually persisted. Other features are intuitive.

## Motivation and Overview

Pretend-Pay is a Single Page App which mimics an online banking/payment-processing service. It runs within the Angular 8 framework.

Pretend-Pay is a small application, and consequently some of the patterns and architectural techniques that I have utilised may appear to be overkill, however the objective is to demonstrate both my competency to develop larger maintainable, robust and professionally architected single-page client applications using Angular, and the use of a store within an Angular application. Look out for structural design patterns such as Facade and Bridge, and behavioural patterns such as Mediator. Note also adherence to the single responsibility principle.

The user-interface is responsive and tidy, however the intended emphasis is good architecture and best-practice coding. Given that objective, I may have decided to omit certain other generally unexceptional features (such as paged results) for the sake of expediency.

The app has been "internationalized", with US English, British English, German, and Hebrew language support (if you're multilingual, be warned that the Google translations are unlikely to be strictly accurate). Numbers, dates, and currencies are all formatted in accordance with locales associated with each language. Hebrew is a right-to-left language, and the UI handles transitons between ltr and rtl seamlessly.

The app UI supports two modes, "phone" and "tablet", and reformats itself dynamically depending upon the size of the viewport. A production application might be implemented with more granular viewport size support. 

Integrating a store was a major motivation. The store maintains application state including not only model entities, but also application modes such as whether the app is initialising, whether the current user is authenticated, and the display characteristics of the host device. Evidence of the store can be seen when account balances rendered in the UI change in response to external transactions being processed, including in active controls (dismiss any active notifications, then open the "From Account" select drop-down in the "Pay & Transfer" view and watch the balances change as accounts are updated in response to receipt notifications).

The app is developed using asynchronous programming techniques, and stores and retrieves persistent data through an abstract asynchronous API defined as a Typescript interface. In practice, the concrete API might be a GraphQL, standard RPC, or even REST-like implementation, however in this case it is mocked entirely in-memory. The mock sleeps asynchronously with each API operation in order to mimic network latency.

The application performs extensive trace-logging, which can be viewed in the Javascript console window (F12 or Ctrl+Shift+I in Chrome and Edge).

## Limitations

It's not possible to be all things to all people. This is a simple demo-application, and there will inevitably be better ways to do things in other applications. With that in mind:

I'm familiar with other Angular techniques and types such as guards, resolvers, HttpClient, Formly, etc, but have not demonstrated their usage here.

This application is written in an Object Orientated rather than a Functional Programming style. I'm familiar with both RxJS and Functional Programming techniques, however the use of RxJS in this application is limited to basic event publication and subscription (the Observer pattern). I chose to define the API to return promises so as to utilise the Typescript/Javascript async/await syntax, but of course a concrete API implementation could use a common function to map Observable responses from HttpClient requests to promises. An alternative (and arguably preferrable) approach might be to use observables exclusively (observables are obviously a native best-fit for an RxJS-centric app, and are also "cancellable" whereas promises are not), however that was not the approach I took in this example.

I chose to abstract the underlying API implmentation with an interface (which allows it to be mocked easily). I'm generally not a fan of REST APIs, and I don't believe I've ever seen an example of a truly RESTful implementation of a web-based Single Page Application. In my view, the REST Uniform Interface Constraint (in particular the HATEOAS sub-constraint) is quite incompatible with modern SPAs (explaining why would require an extended commentary that I won't enter into here, but reduces to the fact that the client application MUST NOT have ANY prior knowledge of the backend application/web service/API OTHER THAN an initial entry URI), and therefore the abstract API interface I have implemented doesn't lend itself to a purely REST backend. It would however suit a GraphQL implementation, a REST-like API that works around the HATEOAS constraint, and also of course a more "traditional" RPC-type interface.

## Coding Conventions and Style

US English spelling is used for programming-related terms and symbol names (for example "Initialize" and "Finalize" as opposed to "Initialise", etc), including in inline documentation.

The Microsoft coding guidelines for Typescript are generally adhered to (https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines).
An incomplete set of these includes:

- Use PascalCase for type names.
- Do not use "I" as a prefix for interface names.
- Use PascalCase for enum values.
- Use camelCase for function names.
- Use camelCase for property names and local variables.
- Do not use "\_" as a prefix for private properties.
- Use whole words in names when possible.
- 1 file per logical component
- Use undefined. Do not use null - Douglas Crockford (a key developer of the Javascript language) explains why here - https://www.youtube.com/watch?v=PSGEjv3Tqo0&feature=youtu.be&t=9m21s)
- More than 2 related Boolean properties on a type should be turned into a flag
- Use double quotes for strings.
- Always surround loop and conditional bodies with curly braces. Statements on the same line are allowed to omit braces.

I continue to use an "\_" prefix for private properties, as I find it useful particularly for distinguishing between private member variables that are returned by more loosely scoped (example, public) property getters.

I was originally taught that a method/function should only have one exit point. The argument was that it improved code legibility, and of course it was closely tied to logic flow charts. In reality the practice can lead to quite illegible code full of nested conditions, so I long ago embraced the guard clause:
https://refactoring.com/catalog/replaceNestedConditionalWithGuardClauses.html

There still remains some difference of opinion on this matter, and so I adopt whatever convention is the standard within the environment in which I am working.

## Model-View-ViewModel Architecture

The application is implemented within a MVVM (Model-View-ViewModel) architecture, and integrates MobX (a Javascript state-management library/store based upon Transparent Functional Reactive Programming, or TFRP) to manage application state. This architecture, in conjunction with Angular Reactive Forms, results in Angular components with very little "code behind", with most of the UI logic being implemented within the view-model. Some UI logic still exists within the markup (mainly in the form of Angular structural directives for conditional rendering and pipes for formatting), and in some cases within the component code where necessary to support requirements and/or features specific to the view component library (in this case Material Design for Angular).

A MVVM architecture decouples the logic required to manage view state from the view itself. This serves to better support unit testing, but also eliminates dependencies upon any single view/component library. For example, although I have implemented the view using Material Design components, I could relatively easily substitute (say) Ionic 4 Web Components. In fact, using this principle, the application logic -- including view models -- could be packaged separately and imported into Material Design, Ionic 4, and NativeScript Angular projects for parallel development.

### The Model

The model is represented logically by a store, which in practice is comprised of a number of discrete classes that maintain application state.
The model in this context repesents only the state of the application as it is observed by any given instance of this client application -- the broader service application is:

- Theoretically multi-tenanted and accessed via a web service API; and
- Potentially implemented within a Service Oriented Architecture or Microservice architecture.

### The View

The view comprises Angular components (including markup and code). In terms of code, view component implementations are typically very simple, usually restricted to dispatching action messages to the view-model for processing, and sometimes to code supporting UI components.

### The View-Model

The view-model encapsulates the store/model, hiding its mutable properties from the view. The view-model exposes observable model state as read-only properties, and action-methods which can be invoked to mutate/change the state of the model. The view-model, therefore, ensures that the view does not directly mutate the model, the state of which can only be changed indirectly via the view-model.

## Why MobX and not NgRx Store?

In this instance I decided to implement the store using MobX as opposed to NgRx Store, although in principle the two libraries do much the same thing and either can be used effectively.

Some arguments supporting MobX can be found here: https://medium.com/@vivainio/mobx-with-angular-the-prelude-1c0dcfb43fe6

To summarise:

- MobX is lightweight AND fast
- MobX allows safe mutation of stored data (whereas NgRx recreates data with every change, creating memory/garbage collection/processing overhead)
- MobX allows state to be stored in any structure you like
- MobX is efficient. Computed values are not updated until they are required for a side-effect (I/O), and automatically garbage-collected if not being observed by any active views, therefore memory consumption is minimal.

## Internationalization

https://www.npmjs.com/package/@ngx-translate/core
The ngx-translate library is used for localizing language resources, as opposed to Angular application internationalization tools. This was a design decision made to enable real-time in-application locale-switching (the Angular tools precompile a distinct application version for each supported language).

Custom pipes are implemented for formatting localized values such as currency values, as opposed to using built-in Angular pipes such as CurrencyPipe. This was a design decision made to enable better integration with the application's dynamic locale system.

## Other Features

- The APIKeyLoader (see src/api-keys-README.md)
- Google Places address lookup (found in the Address section of the Profile view)
- The Abstraction of ToastController and DialogController implementations to decouple from specific UI component libraries (src/app/presenters)
- The application employs an optimistic concurrency strategy. Although there will be no concurrency issues in the in-memory mock, the strategy is implemented anyway simply to show that it has been considered.

## Issues

This app was developed and tested using the Chrome and Edge browsers.

### Material Design Issues

- The Material Design Select component <mat-select> causes an Angular "expression was changed after it was checked" error when an interpolation expression including the "translate" pipe causes inlined <mat-option> inner HTML to change as a result of the application language having been changed. Although this error will not be raised in production mode, it is not ideal and was worked-around by using an \*ngFor structural directive to build the <mat-option> groups from name-value collections exposed in the relevant view-models, and updating those collections when the application dynamic locale changes.

### Chrome Browser Issues

- Chrome Intl.NumberFormat for currencies - Chrome displays the currency code and symbol when the format is set to "symbol". This is a Chrome issue. This was worked-around in the InternationalizationService formatter.

### Edge Browser Issues

- Edge goes into a text-selection mode after navigating away from the account transactions view. This appears to be an issue with the Material Design table component. This was worked-around by invoking document.selection.empty() in the ngOnDestroy method of AccountTransactionsViewComponent.

## Other ReadMe Files

- [src/api-keys-README.md](src/api-keys-README.md)
- [src/app/facade/README.md](src/app/facade/README.md)
- [src/app/presenters/README.md](src/app/presenters/README.md)
