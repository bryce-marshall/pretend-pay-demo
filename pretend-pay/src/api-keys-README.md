# API Key Resolution
API keys (particularly those used during development which may be unrestricted) are sensitive information that should not be saved to a source control repository.

API Keys should be included in the /assets/config/api-keys.json file. Note that this file is included in the /assets/config/.gitignore file, and so will need to be added and configured whenever the repository is cloned.

## Example API JSON File
This application requires the following API definitions to be included in "/assets/config/api-keys.json:

{
    "google": {
        "maps": {
            "places": "api-key goes here"
        }
    }
}

# API Script Definitions
API script URLs must be formatted during application initialisation to include the keys.
Such URLs are included in the <head> element in an HTML comment section the leading text of which must be @api-scripts

Within this comment section:

* All lines beginning with the # character or containing only whitespace will be ignored
* All other non-whitepace lines are interpreted as URLs and will be formatted and added to the HEAD element as script elements
* Left and right curly bracket character pairs are interpreted as tokens containing a JSON path and will be replaced with a mapped API key
* Mapped API keys are resolved from the /assets/config/api-keys.json file

Example:

  <!-- @api-scripts DO NOT REMOVE THIS COMMENT SECTION!
  
    # The following URLs will formatted with API keys and added as script elements during application pre-initialization
    
    https://maps.googleapis.com/maps/api/js?key={google.maps.places}&libraries=places
  -->
