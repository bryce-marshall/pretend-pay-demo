import { Injectable } from "@angular/core";
import { FinancialAccount, AccountNumber } from "../lib/financial-account";
import { Facade } from "../facade";
import { DockedViewModel } from "./docked-view-model";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { StoreStateObserver } from "../store/extensions";
import { SyncState } from "../lib/types";

@Injectable()
export class AccountsViewModel extends DockedViewModel {
    private _accounts: FinancialAccount[];
    private _syncState: SyncState;

    constructor(
        facade: Facade,

    ) {
        super(facade, { loadingStates: facade.account.observable });
        this.facade.account.ensureAccounts();
    }

    get syncState() {
        return this._syncState;
    }

    get accounts(): FinancialAccount[] {
        return this._accounts;
    }

    focusAccount(accountNumber: AccountNumber) {
        this.facade.navigate.transactions(accountNumber);
    }

    @StoreStateObserver()
    protected accountsChanged() {
        const accounts = this.facade.account.observable.accounts;
        if (accounts) this._accounts = cloneEntity(accounts);
    }

    @StoreStateObserver()
    protected accountsSyncChanged() {
        this._syncState = this.facade.account.observable.syncState;
    }
}
