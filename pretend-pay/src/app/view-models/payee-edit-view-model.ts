import { Injectable } from "@angular/core";
import { Facade } from "../facade";
import { SavedPayee } from "../lib/saved-payee";
import { PayeeType } from "src/app/lib/types";
import { DockedViewModel } from "./docked-view-model";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { FormControlExclusiveControlDirector } from "src/app/lib/view";
import { FormState } from "../lib/view/form-state";
import { ActionInfo } from "../lib/action-info";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { AppValidators } from "../lib/view/app-validators";
import { NameValue } from "../lib/name-value";
import { InternationalizationService } from "../services/i18n/internationalization.service";

export interface PayeeEditViewFormControls {
    payeeName: FormControl;
    payeeType: FormControl;
    payeeAccount: FormControl;
    payeeEmail: FormControl;
    description: FormControl;
}

@Injectable()
export class PayeeEditViewModel extends DockedViewModel {
    private _groupDirector: FormControlExclusiveControlDirector<PayeeType>;
    private _payeeId: number;
    private _entityVersion: number;
    private _payeeTypeOptions: NameValue[];

    readonly form: FormGroup;
    readonly controls: PayeeEditViewFormControls;
    readonly formState: FormState;

    constructor(
        private translate: InternationalizationService,
        facade: Facade,
        route: ActivatedRoute
    ) {
        super(facade, { loadingStates: facade.payee.observable, manualInit: true });

        this.form = new FormGroup({
            payeeName: new FormControl("", [
                Validators.required
            ]),

            payeeType: new FormControl("", [
                Validators.required
            ]),

            payeeAccount: new FormControl("", [
                Validators.required,
                AppValidators.accountNumber
            ]),

            payeeEmail: new FormControl("", [
                Validators.required,
                Validators.email
            ]),

            description: new FormControl("", [
            ])
        });

        this.controls = <any>this.form.controls;
        this.formState = new FormState(this.form, this);
        this.createTypeOpts();

        this._groupDirector = new FormControlExclusiveControlDirector<PayeeType>(
            this.controls.payeeType,
            [
                {
                    type: "email",
                    controls: <FormControl>this.controls.payeeEmail
                },
                {
                    type: "account-number",
                    controls: <FormControl>this.controls.payeeAccount
                }
            ]);

        this.facade.payee.ensurePayees().then(() => {
            // We don't want to try and resolve the edited payee until the full set of saved payees is available.
            this.subscribeTo(route.paramMap.pipe(), (p: ParamMap) => {
                const id = parseInt(p.get("payeeId"), 10);
                if (!isNaN(id)) this.initForEdit(id);
            });
        });

        // It is necessary to update the direction-options collection so that the view can bind to it.
        // Binding directly from the view causes an Angular "expresson was changed after it was set" error when the locale changes
        // and the material-select options text changes.
        super.subscribeTo(this.translate.localeChange, () => this.createTypeOpts());

        this.initObservers();
    }

    get payeeTypeOptions(): NameValue[] {
        return this._payeeTypeOptions;
    }

    get isEditing(): boolean {
        return this._payeeId !== undefined;
    }

    cancel() {
        this.facade.navigate.payees();
    }

    savePayee() {
        if (!this.formState.evalSubmit()) return;

        const payee = this.constructPayee();
        this.isBusyInt = true;
        const promise = this.isEditing ? this.facade.payee.updatePayee(payee) : this.facade.payee.createPayee(payee);
        promise.then((r) => {
            this.facade.environment.enqueueActionInfoNext(r);
            this.facade.navigate.payees();
        })
            .catch((e) => {
                this.facade.environment.enqueueActionInfo(ActionInfo.convert(e));
            })
            .finally(() => {
                this.isBusyInt = false;
            });
    }

    private initForEdit(payeeId: number) {
        if (this._payeeId === payeeId) return;

        const edit = this.facade.payee.observable.payees.find((other) => {
            return other.id === payeeId;
        });

        if (!edit) {
            // TODO: Add error ActionInfo error Payee not found
            this.facade.navigate.payees();
        } else {
            this._payeeId = edit.id;
            this._entityVersion = edit.entityVersion;
            this.controls.payeeName.setValue(edit.name);
            this.controls.description.setValue(edit.description);
            this.controls.payeeType.setValue(edit.payeeType);

            switch (edit.payeeType) {
                case "email":
                    this.controls.payeeEmail.setValue(edit.paymentTarget);
                    break;

                case "account-number":
                    this.controls.payeeAccount.setValue(edit.paymentTarget);
                    break;
            }
        }
    }

    private constructPayee(): SavedPayee {
        const payee = {
            id: this._payeeId,
            entityVersion: this._entityVersion,
            name: this.controls.payeeName.value,
            payeeType: this.controls.payeeType.value,
            paymentTarget: undefined,
            description: this.controls.description.value
        };

        switch (payee.payeeType) {
            case "account-number":
                payee.paymentTarget = this.controls.payeeAccount.value;
                break;

            case "email":
                payee.paymentTarget = this.controls.payeeEmail.value;
                break;

            default:
                throw new Error(`Payee type ${payee.payeeType} not supported.`);
        }

        return payee;
    }

    private createTypeOpts() {
        this._payeeTypeOptions = [
            { value: "email", name: this.translate.getTextInstant("payee-type.email") },
            { value: "account-number", name: this.translate.getTextInstant("payee-type.account-number") }
        ];
    }
}
