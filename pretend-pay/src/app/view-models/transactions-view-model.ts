import { Injectable } from "@angular/core";
import { FinancialAccount } from "../lib/financial-account";
import { AccountTransaction } from "../lib/account-transaction";
import { Facade } from "../facade";
import { DockedViewModel } from "./docked-view-model";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AppValidators } from "../lib/view/app-validators";
import { Currency } from "../lib/utilities/currency";
import { TransactionQueryArgs } from "../lib/transaction-query-args";
import { Equals } from "../lib/utilities/equals";
import { Convert } from "../lib/utilities/convert";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { StoreStateObserver } from "../store/extensions";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { NameValue } from "../lib/name-value";

export interface TransactionsViewFormControls {
    text: FormControl;
    minDate: FormControl;
    maxDate: FormControl;
    minValue: FormControl;
    maxValue: FormControl;
    direction: FormControl;
}

@Injectable()
export class TransactionsViewModel extends DockedViewModel {
    private _transactions: AccountTransaction[] = [];
    private _hasFilter = false;
    private _isFilterApplied = true;
    private _args: TransactionQueryArgs;
    private _focusedAccount: FinancialAccount;
    readonly form: FormGroup;
    readonly controls: TransactionsViewFormControls;
    private _directionOptions: NameValue[];

    constructor(
        private translate: InternationalizationService,
        facade: Facade,
        route: ActivatedRoute
    ) {
        super(facade, { loadingStates: facade.account.observable, manualInit: true });

        this.form = new FormGroup(
            {
                text: new FormControl("", []),
                minDate: new FormControl("", []),
                maxDate: new FormControl("", []),
                minValue: new FormControl("", [
                    AppValidators.greaterThan(0)
                ]),
                maxValue: new FormControl("", [
                    AppValidators.greaterThan(0)
                ]),
                direction: new FormControl("both", [
                    Validators.required
                ]),
            }
        );

        this._args = TransactionQueryArgs.createDefault("");
        this.controls = <any>this.form.controls;
        this.createDirOpts();

        this.form.valueChanges.subscribe(() => {
            this.refreshFilterState();
        });

        this.facade.account.ensureAccounts().then(() => {
            const accounts = cloneEntity(this.facade.account.observable.accounts);

            // We don't want to try and resolve the focused account until the full set of accounts is available.
            this.subscribeTo(route.paramMap.pipe(), (p: ParamMap) => {
                const number = p.get("accountNumber");
                const account = accounts.find(cmp => cmp.number === number);
                this.initForAccount(account);
            });
        });
        // It is necessary to update the direction-options collection so that the view can bind to it.
        // Binding directly from the view causes an Angular "expresson was changed after it was set" error when the locale changes
        // and the material-select options text changes.
        super.subscribeTo(this.translate.localeChange, () => this.createDirOpts());
        this.initObservers();
    }

    get directionOptions(): NameValue[] {
        return this._directionOptions;
    }

    get focusedAccount(): FinancialAccount {
        return this._focusedAccount;
    }

    get transactions(): AccountTransaction[] {
        return this._transactions;
    }

    get hasFilter(): boolean {
        return this._hasFilter;
    }

    get isFilterApplied(): boolean {
        return this._isFilterApplied;
    }

    search() {
        if (!this.isFilterApplied) this.applyFilter();
        this._args.text = (<string>this.controls.text.value).trim();

        this._args.page = 0;
        this.executeQuery();
    }

    clearFilter() {
        this.controls.minDate.setValue(undefined);
        this.controls.maxDate.setValue(undefined);
        this.controls.minValue.setValue(undefined);
        this.controls.maxValue.setValue(undefined);
        this.controls.direction.setValue("both");
        this.applyFilter();
    }

    applyFilter() {
        this.refreshFilterState();
        if (this.isFilterApplied) return;
        const args = this._args;
        args.minDate = Convert.asDate(this.controls.minDate.value);
        args.maxDate = Convert.asDate(this.controls.maxDate.value);
        args.minValue = this.controls.minValue.value;
        args.maxValue = this.controls.maxValue.value;
        args.direction = this.controls.direction.value;
        this.refreshFilterState();
        this.search();
    }

    private initForAccount(account: FinancialAccount) {
        if (!account) {
            // TODO: Add error ActionInfo error Account not found
            this.facade.navigate.accounts();
        } else {
            this._focusedAccount = account;
            this._args.account = account.number;
            const exponent = Currency.exponent(account.currency);
            this.controls.minValue.setValidators([
                AppValidators.greaterThan(0),
                AppValidators.maxPlaces(exponent)
            ]);

            this.controls.maxValue.setValidators([
                AppValidators.greaterThan(0),
                AppValidators.maxPlaces(exponent)
            ]);

            this.executeQuery();
        }
    }

    private refreshFilterState() {
        const filter = {
            minDate: Convert.asDate(this.controls.minDate.value),
            maxDate: Convert.asDate(this.controls.maxDate.value),
            minValue: this.controls.minValue.value,
            maxValue: this.controls.maxValue.value,
            direction: this.controls.direction.value
        };

        this._hasFilter =
            filter.minDate ||
            filter.maxDate ||
            filter.minValue ||
            filter.maxValue ||
            filter.direction !== "both";

        const args = this._args;

        this._isFilterApplied =
            Equals.date(filter.minDate, args.minDate) &&
            Equals.date(filter.maxDate, args.maxDate) &&
            filter.minValue === args.minValue &&
            filter.maxValue === args.maxValue &&
            Equals.transactionDirection(filter.direction, args.direction);
    }

    private async executeQuery() {
        try {
            const r = await this.facade.account.queryTransactions(cloneEntity(cloneEntity(this._args)));
            this._transactions = r.result.transactions;
        } catch (e) {
            // TOOD: Handle in view
            console.log("*** ERROR executing query", e);
        }
    }

    @StoreStateObserver()
    protected accountsChanged() {
        // Always reference the MobX observable, otherwise the observer may not be registered!
        const accounts = this.facade.account.observable.accounts;
        // An instance of the account should have been loaded first; if not, intialisation hasn't completed.
        if (!accounts || !this._focusedAccount) return;
        const account = accounts.find(cmp => cmp.number === this._focusedAccount.number);
        if (account) this._focusedAccount = cloneEntity(account);
    }

    private createDirOpts() {
        this._directionOptions = [
            { value: "in", name: this.translate.getTextInstant("tx-direction.in") },
            { value: "out", name: this.translate.getTextInstant("tx-direction.out") },
            { value: "both", name: this.translate.getTextInstant("tx-direction.both") }
        ];
    }
}
