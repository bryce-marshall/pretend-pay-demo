import { Injectable } from "@angular/core";
import { Facade } from "../facade";
import { PageViewModel } from "./page-view-model";

@Injectable()
export class HomePageViewModel extends PageViewModel {
    constructor(facade: Facade) {
        super(facade);
    }
}
