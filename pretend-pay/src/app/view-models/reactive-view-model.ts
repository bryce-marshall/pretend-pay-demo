import { Injectable, OnDestroy } from "@angular/core";
import { LoadingStateProvider } from "src/app/lib/loading-state-provider";
import { Facade } from "../facade";
import { ArrayHelper } from "../lib/collections/array-helper";
import { LoadingState } from "../lib/types";
import { BusyStateProvider } from "../lib/view/busy-state-provider";
import { HostedObserverSet, StoreStateObserver } from "../store/extensions";
import { ComponentViewModel } from "./component-view-model";

export interface ReactiveViewModelArgs {
    observables?: any | any[];
    loadingStates?: LoadingStateProvider | LoadingStateProvider[];
    manualInit?: boolean;
}

/**
 * The base class for all ViewModel implementations which need to react to changes to store observables.
 */
@Injectable()
export abstract class ReactiveViewModel extends ComponentViewModel implements OnDestroy, BusyStateProvider {
    private _loadingState: LoadingState = "loaded";
    private _loadingStates: LoadingStateProvider[];
    private _observers: HostedObserverSet;
    protected isBusyInt = false;

    constructor(facade: Facade, args?: ReactiveViewModelArgs) {
        super(facade);
        const initGuard = this.initObservers;

        const initFn = () => {
            let observers = [this];

            if (args) {
                this._loadingStates = ArrayHelper.ensureArray(args.loadingStates);
                observers = observers.concat(ArrayHelper.ensureArray(args.observables));
            }

            this._observers = new HostedObserverSet(observers);
            this.initObservers = initGuard;
        };

        if (args && args.manualInit === true) {
            this.initObservers = initFn;
        } else {
            initFn();
        }
    }

    protected initObservers() {
        // tslint:disable-next-line: max-line-length
        throw new Error("MobX observers have already been initialized. Did you mean to set the manualInit option in the ReactiveViewModelArgs?");
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        HostedObserverSet.dispose(this._observers);
    }

    //#region BusyStateProvider
    get isBusy() {
        return this.isBusyInt;
    }

    get loadingState(): LoadingState {
        return this._loadingState;
    }

    get isLoaded(): boolean {
        return this._loadingState === "loaded";
    }

    get isLoading(): boolean {
        return this._loadingState === "loading";
    }

    //#endregion

    @StoreStateObserver()
    protected onLoadingStateChanged() {
        if (!this._loadingStates) return;

        let state: LoadingState = "loading";
        // Derive an aggregate view of all loading state providers. If one has failed, the view fails.
        // If one remains in a loading state, the view is in a loading state.
        // If all succeeds, the view succeeds.
        let success = 0;
        this._loadingStates.forEach((ls) => {
            if (ls) {
                switch (ls.loadingState) {
                    case "error":
                        state = ls.loadingState;
                        break;

                    case "loaded":
                        success++;
                        break;
                }
            } else {
                success++;
            }
        });

        this._loadingState = success === this._loadingStates.length ? "loaded" : state;
    }
}
