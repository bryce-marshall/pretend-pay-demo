import { Injectable } from "@angular/core";
import { ReactiveViewModel } from "./reactive-view-model";
import { Facade } from "../facade";
import { StoreStateObserver } from "../store/extensions";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { AppNotification } from "../lib/app-notification";

@Injectable()
export class NotificationIndicatorViewModel extends ReactiveViewModel {
    private _notificationCount = 0;
    private _notifications: AppNotification[];

    constructor(facade: Facade) {
        super(facade, { manualInit: true });
        this.initObservers();
    }

    get notificationCount(): number {
        return this._notificationCount;
    }

    get notifications(): AppNotification[] {
        return this._notifications;
    }

    dismissAll() {
        this.facade.profile.clearNotifications();
    }

    dismiss(notification: AppNotification) {
        this.facade.profile.dismissNotification(notification);
    }

    @StoreStateObserver()
    protected notificationCountChanged() {
        this._notificationCount = this.facade.profile.notificationsObservable.notificationCount;
    }

    @StoreStateObserver()
    protected notificationsChanged() {
        this._notifications = cloneEntity(this.facade.profile.notificationsObservable.notifications);
    }
}
