import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AppConfig } from "../config/app-config";
import { Facade } from "../facade";
import { ActionInfo } from "../lib/action-info";
import { GeoAddress } from "../lib/geo-address";
import { ManualLoadingStateProvider } from "../lib/manual-loading-state-provider";
import { UserProfile } from "../lib/user-profile";
import { Convert } from "../lib/utilities/convert";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { FormState } from "../lib/view/form-state";
import { DockedViewModel } from "./docked-view-model";

export interface ProfileViewFormControls {
    firstName: FormControl;
    lastName: FormControl;
    dob: FormControl;
    addressStreet: FormControl;
    addressSuburb: FormControl;
    addressLocality: FormControl;
    addressPostcode: FormControl;
    addressRegion: FormControl;
    addressCountry: FormControl;
    language: FormControl;
}

@Injectable()
export class ProfileViewModel extends DockedViewModel {
    private _version: number;
    readonly form: FormGroup;
    readonly formState: FormState;
    readonly controls: ProfileViewFormControls;
    readonly locales: string[];

    constructor(facade: Facade) {
        const loadingState = new ManualLoadingStateProvider();
        super(facade, { loadingStates: loadingState });

        this.form = new FormGroup(
            {
                firstName: new FormControl("", [
                    Validators.required
                ]),
                lastName: new FormControl("", [
                    Validators.required
                ]),
                dob: new FormControl("", [
                    Validators.required
                ]),
                addressStreet: new FormControl("", [
                    Validators.required
                ]),
                addressSuburb: new FormControl("", []),
                addressLocality: new FormControl("", [
                    Validators.required
                ]),
                addressPostcode: new FormControl("", [
                    Validators.required
                ]),
                addressRegion: new FormControl("", [
                    Validators.required
                ]),
                addressCountry: new FormControl("", [
                    Validators.required
                ]),
                language: new FormControl("", [
                    Validators.required
                ])
            }
        );

        this.controls = <any>this.form.controls;
        this.formState = new FormState(this.form, this);
        this.locales = cloneEntity(AppConfig.settings.locales);

        this.loadProfile(loadingState);
    }

    cancel() {
        this.facade.navigate.accounts();
    }

    async save() {
        if (!this.formState.evalSubmit()) return;
        try {
            this.isBusyInt = true;
            const c = this.controls;
            const p: UserProfile = {
                entityVersion: this._version,
                dateOfBirth: Convert.asDate(c.dob.value),
                firstName: c.firstName.value,
                lastName: c.lastName.value,
                locale: c.language.value,
                streetAddress: c.addressStreet.value,
                suburb: c.addressSuburb.value,
                town: c.addressLocality.value,
                postcode: c.addressPostcode.value,
                state: c.addressRegion.value,
                country: c.addressCountry.value
            };

            const r = await this.facade.profile.saveProfile(p);
            if (r.isOk) {
                this.applyProfile(r.result);
                this.facade.environment.enqueueActionInfoNext(ActionInfo.convert(r));
                this.facade.navigate.accounts();
            } else {
                this.facade.environment.enqueueActionInfo(ActionInfo.convert(r));
            }
        } catch (e) {
            this.facade.environment.enqueueActionInfo(ActionInfo.convert(e));
        }
        finally {
            this.isBusyInt = false;
        }
    }

    applyGeoAddress(address: GeoAddress) {
        const c = this.controls;
        c.addressStreet.setValue(address.street);
        c.addressSuburb.setValue(address.suburb);
        c.addressLocality.setValue(address.locality);
        c.addressPostcode.setValue(address.postcode);
        c.addressRegion.setValue(address.region);
        c.addressCountry.setValue(address.country);
    }

    private async loadProfile(loadingState: ManualLoadingStateProvider) {
        try {
            const r = await this.facade.profile.getProfile();
            this.applyProfile(r.result);
            loadingState.setState("loaded");
        } catch (e) {
            this.facade.environment.enqueueActionInfo(ActionInfo.convert(e));
            loadingState.setState("error");
        }
    }

    private applyProfile(p: UserProfile) {
        const c = this.controls;
        c.firstName.setValue(p.firstName);
        c.lastName.setValue(p.lastName);
        c.dob.setValue(p.dateOfBirth);
        c.addressStreet.setValue(p.streetAddress);
        c.addressSuburb.setValue(p.suburb);
        c.addressLocality.setValue(p.town);
        c.addressPostcode.setValue(p.postcode);
        c.addressRegion.setValue(p.state);
        c.addressCountry.setValue(p.country);
        c.language.setValue(p.locale);
        this._version = p.entityVersion;
    }
}
