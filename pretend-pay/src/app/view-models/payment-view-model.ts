import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Facade } from "../facade";
import { ActionInfo } from "../lib/action-info";
import { ExecutableTransaction } from "../lib/executable-transaction";
import { AccountNumber, FinancialAccount } from "../lib/financial-account";
import { SavedPayee } from "../lib/saved-payee";
import { UserPaymentType } from "../lib/types";
import { Currency } from "../lib/utilities/currency";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { FormControlExclusiveControlDirector, ThreeStageFormMode, Translatable } from "../lib/view";
import { AppValidators } from "../lib/view/app-validators";
import { FormState } from "../lib/view/form-state";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { StoreStateObserver } from "../store/extensions";
import { DockedViewModel } from "./docked-view-model";

export interface PayementViewFormControls {
    fromAccount: FormControl;
    paymentType: FormControl;
    toAccount: FormControl;
    payeeAccount: FormControl;
    payeeEmail: FormControl;
    payee: FormControl;
    amount: FormControl;
    memo: FormControl;
    payeeMemo: FormControl;
}

@Injectable()
export class PaymentViewModel extends DockedViewModel {
    private _groupDirector: FormControlExclusiveControlDirector<UserPaymentType>;
    private _toAccounts: FinancialAccount[];
    private _mode: ThreeStageFormMode = "input";
    private _confirmPrompt: Translatable;
    private _successInfo: ActionInfo;
    private _accounts: FinancialAccount[];
    private _payees: SavedPayee[];

    readonly form: FormGroup;
    readonly controls: PayementViewFormControls;
    readonly formState: FormState;

    constructor(
        facade: Facade,
        private i18n: InternationalizationService
    ) {
        super(facade, { loadingStates: [facade.account.observable, facade.payee.observable] });
        Promise.all(
            [this.facade.account.ensureAccounts(),
            this.facade.payee.ensurePayees()]).then(() => {
                this._accounts = cloneEntity(facade.account.observable.accounts);
                this._payees = cloneEntity(facade.payee.observable.payees);
            });

        this.form = new FormGroup({
            fromAccount: new FormControl("", [
                Validators.required
            ]),
            paymentType: new FormControl("transfer", [
                Validators.required
            ]),
            toAccount: new FormControl("", [
                Validators.required
            ]),
            payeeAccount: new FormControl("", [
                Validators.required,
                AppValidators.accountNumber
            ]),
            payeeEmail: new FormControl("", [
                Validators.required,
                Validators.email
            ]),
            payee: new FormControl("", [
                Validators.required
            ]),
            amount: new FormControl("", [
                Validators.required,
                AppValidators.greaterThan(0)
            ]),
            memo: new FormControl("", []),
            payeeMemo: new FormControl("", [])
        });

        this.controls = <any>this.form.controls;
        this.formState = new FormState(this.form, this);

        this.controls.fromAccount.valueChanges.subscribe((value) => {
            this.applyFromAccount(value ? value.number : undefined);
        });

        this._groupDirector = new FormControlExclusiveControlDirector<UserPaymentType>(
            this.controls.paymentType,
            [
                {
                    type: "transfer",
                    controls: this.controls.toAccount
                },
                {
                    type: "email",
                    controls: [this.controls.payeeEmail, this.controls.payeeMemo]
                },
                {
                    type: "account-number",
                    controls: [this.controls.payeeAccount, this.controls.payeeMemo]
                },
                {
                    type: "payee",
                    controls: [this.controls.payee, this.controls.payeeMemo]
                }
            ]);
    }

    get mode(): ThreeStageFormMode {
        return this._mode;
    }

    get fromAccounts(): FinancialAccount[] {
        return this._accounts;
    }

    get payees(): SavedPayee[] {
        return this._payees;
    }

    get fromAccount(): FinancialAccount {
        return this.controls.fromAccount.value;
    }

    get toAccount(): FinancialAccount {
        return this.controls.toAccount.value;
    }

    get toAccounts(): FinancialAccount[] {
        return this._toAccounts;
    }

    get confirmPrompt(): Translatable {
        return this._confirmPrompt;
    }

    get successInfo(): ActionInfo {
        return this._successInfo;
    }

    cancel() {
        this.facade.navigate.accounts();
    }

    close() {
        this.facade.navigate.accounts();
    }

    reset() {
        const paymentType = this.controls.paymentType.value;
        this.form.reset();
        this.formState.resetSubmit();
        this.controls.paymentType.setValue(paymentType);
        this._successInfo = undefined;
        this._confirmPrompt = undefined;
        this._mode = "input";
    }

    continue() {
        if (this.formState.evalSubmit()) {
            this._mode = "confirm";
            this._confirmPrompt = this.createTranslatable();
        }
    }

    back() {
        this.formState.resetSubmit();
        this._mode = "input";
    }

    async submit() {
        const controls = this.controls;
        const fromAccount: FinancialAccount = controls.fromAccount.value;
        const tx: ExecutableTransaction = {
            fromAccount: fromAccount.number,
            payeeType: undefined,
            payee: undefined,
            value: controls.amount.value,
            memo: controls.memo.value,
            payeeMemo: controls.payeeMemo.value
        };

        switch (controls.paymentType.value) {
            case "account-number":
                tx.payeeType = "account-number";
                tx.payee = controls.payeeAccount.value;
                break;

            case "email":
                tx.payeeType = "email";
                tx.payee = controls.payeeEmail.value;
                break;

            case "payee":
                const payee = <SavedPayee>controls.payee.value;
                tx.payeeType = payee.payeeType;
                tx.payee = payee.paymentTarget;
                break;

            case "transfer":
                tx.payeeType = "account-number";
                tx.payee = (<FinancialAccount>controls.toAccount.value).number;
                tx.payeeMemo = tx.memo;
                break;

            default:
                throw new Error(`Invalid or unsupported payment type "${controls.paymentType.value}".`);
        }

        this.isBusyInt = true;
        this.facade.environment.clearActionInfo();
        try {
            const r = await this.facade.account.executeTransaction(controls.paymentType.value, tx);
            this.applyTranslationArgs(r.info.args, r.result.account.available);
            this._successInfo = r.info;
            this._mode = "report";
        } catch (e) {
            this.facade.environment.enqueueActionInfo(ActionInfo.convert(e));
            this._mode = "input";
        }
        finally {
            this.isBusyInt = false;
        }
    }

    private applyFromAccount(accountNumber: AccountNumber) {
        this._toAccounts = undefined;
        let a: FinancialAccount;
        if (this.fromAccounts) {
            a = this.fromAccounts.find((match) => match.number === accountNumber);
            if (a) {
                const set = [];
                this.fromAccounts.forEach((a2) => {
                    if (a !== a2 && a.currency === a2.currency) set.push(a2);
                });

                if (set.length > 0) this._toAccounts = set;
            }
        }

        const validators = [Validators.required, AppValidators.greaterThan(0)];
        if (a) validators.push(AppValidators.maxPlaces(Currency.exponent(a.currency)));
        this.controls.amount.setValidators(validators);
        // Only apply the form validation check at this point if the control has been modified by the user.
        if (this.controls.amount.dirty) this.controls.amount.updateValueAndValidity();
    }

    private createTranslatable(): Translatable {
        const result = {
            key: undefined,
            args: {}
        };

        result.key = "payments-view.confirm-prompt." + this.applyTranslationArgs(result.args);
        return result;
    }

    private applyTranslationArgs(args: any, available?: number): string {
        let key: string;
        const fromAccount: FinancialAccount = this.controls.fromAccount.value;
        args.fromName = fromAccount.name;
        args.available = this.i18n.format.currency(available ? available : fromAccount.available, fromAccount.currency);
        args.amount = this.i18n.format.currency(this.controls.amount.value, fromAccount.currency);

        switch (this.controls.paymentType.value) {
            case "transfer":
                key = "transfer";
                args.toName = this.controls.toAccount.value.name;
                break;

            case "email":
                key = "email";
                args.toName = this.controls.payeeEmail.value;
                break;

            case "account-number":
                key = "account";
                args.toName = this.controls.payeeAccount.value;
                break;

            case "payee":
                key = "payee";
                args.toName = this.controls.payee.value.name;
                break;
        }

        return key;
    }

    @StoreStateObserver()
    protected onAccountsChanged() {
        // Always reference the MobX observable!
        const accounts = this.facade.account.observable.accounts;
        // Don't assign if we are stil initialising or there has been an initialisation error.
        if (!this._accounts) return;
        this._accounts.forEach((bound) => {
            const latest = accounts.find(cmp => cmp.number === bound.number);
            if (latest) {
                bound.available = latest.available;
                bound.balance = latest.balance;
            }
        });
    }
}
