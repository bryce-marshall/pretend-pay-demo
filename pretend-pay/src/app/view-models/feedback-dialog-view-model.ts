import { Injectable } from "@angular/core";
import { ApiResponse } from "../lib/api-response";
import { DialogRef } from "../presenters";
import { ReactiveViewModel } from "./reactive-view-model";
import { Facade } from "../facade";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { FormState } from "../lib/view/form-state";

export interface FeedbackDialogViewFormControls {
    feedbackType: FormControl;
    feedback: FormControl;
}

@Injectable()
export class FeedbackDialogViewModel extends ReactiveViewModel {
    readonly form: FormGroup;
    readonly controls: FeedbackDialogViewFormControls;
    readonly formState: FormState;

    constructor(
        facade: Facade,
        private dialogRef: DialogRef<any>
    ) {
        super(facade);
        this.form = new FormGroup({
            feedbackType: new FormControl("usage", [
                Validators.required
            ]),
            feedback: new FormControl("", [
                Validators.required
            ])
        });
        this.controls = <any>this.form.controls;
        this.formState = new FormState(this.form, this);
    }

    closeDialog() {
        this.dialogRef.dialog.close();
    }

    submitFeedback() {
        if (this.form.invalid) return;
        const feedbackType = this.controls.feedbackType.value;
        const feedbackText = this.controls.feedback.value;
        if (!feedbackType || !feedbackText) throw new Error("Feedback data is invalid.");
        this.isBusyInt = true;

        this.facade.profile.submitFeedback(
            {
                userId: "",
                feedbackType: feedbackType,
                feedback: feedbackText
            })
            .then((info) => {
                this.isBusyInt = false;
                this.closeDialog();
                this.facade.environment.enqueueActionToast(info);
            })
            .catch((e) => {
                this.isBusyInt = false;
                this.facade.environment.enqueueError({
                    errorCode: ApiResponse.resolveErrorCode(e)
                });
            });
    }
}
