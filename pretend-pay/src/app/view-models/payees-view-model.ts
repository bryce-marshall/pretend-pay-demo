import { Injectable } from "@angular/core";
import { Facade, PayeesObservable } from "../facade";
import { SavedPayee } from "../lib/saved-payee";
import { DockedViewModel } from "./docked-view-model";
import { ConfirmModalViewComponent } from "../views/confirm-modal-view/confirm-modal-view.component";
import { ConfirmModalData } from "../lib/view/confirm-modal-data";
import { ActionInfo } from "../lib/action-info";

@Injectable()
export class PayeesViewModel extends DockedViewModel {
    private _store: PayeesObservable;

    constructor(
        facade: Facade
    ) {
        super(facade, { loadingStates: facade.payee.observable });
        this.facade.payee.ensurePayees();
        this._store = this.facade.payee.observable;
    }

    get payees(): SavedPayee[] {
        return this._store.payees;
    }

    newPayee() {
        this.facade.navigate.createPayee();
    }

    editPayee(payee: SavedPayee) {
        this.facade.navigate.editPayee(payee.id);
    }

    confirmDeletePayee(payee: SavedPayee) {
        const data: ConfirmModalData = {
            titleResourceKey: "payees-view.confirm-delete.title",
            messageResourceKey: "payees-view.confirm-delete.prompt",
            confirmButtonType: "delete",
            formatArgs: payee
        };

        const currentDialog = this.facade.navigate.openDialog(ConfirmModalViewComponent, data);
        const subscription = currentDialog.afterClosed().subscribe(async () => {
            subscription.unsubscribe();
            if (!data.isConfirmed) return;
            try {
                this.isBusyInt = true;
                const r = await this.facade.payee.deletePayee(payee);
                this.facade.environment.enqueueActionInfo(r);
            } catch (e) {
                this.facade.environment.enqueueActionInfo(ActionInfo.convert(e));
            }
            finally {
                this.isBusyInt = false;
            }
        });
    }
}
