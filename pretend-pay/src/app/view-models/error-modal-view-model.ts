import { Injectable } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { ErrorInfo } from "../lib/view/error-info";
import { DialogRef } from "../presenters/";
import { ComponentViewModel } from "./component-view-model";
import { Facade } from "../facade";

@Injectable()
export class ErrorModalViewModel extends ComponentViewModel {
    title: string;
    context: string;
    message: string;

    constructor(
        facade: Facade,
        private i18n: InternationalizationService,
        private dialogRef: DialogRef<ErrorInfo>
    ) {
        super(facade);
        let errorInfo = dialogRef.data;
        if (!errorInfo) errorInfo = {};

        this.title = this.resolveText(errorInfo.title, "error-view.title");
        this.context = this.resolveText(errorInfo.context, "error-view.context");
        if (errorInfo.errorMessage) this.message = errorInfo.errorMessage;
        else this.message = this.i18n.response.getErrorMessageInstant(errorInfo.errorCode);
    }

    closeModal() {
        this.dialogRef.dialog.close();
    }

    private resolveText(value: string, defaultKey: string): string {
        return value != undefined ? value : this.i18n.getTextInstant(defaultKey);
    }
}
