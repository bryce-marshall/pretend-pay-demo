import { Injectable } from "@angular/core";
import { Facade } from "../facade";
import { ReactiveViewModel } from "./reactive-view-model";
import { ActionInfo } from "../lib/action-info";
import { StoreStateObserver } from "../store/extensions";
import { cloneEntity } from "../lib/utilities/entity-clone";

/**
 * A ViewModel that maintains a list of current ActionInfo instances.
 */
@Injectable()
export class ActionInfoViewModel extends ReactiveViewModel {
    private _actionInfo: ActionInfo[] = [];

    constructor(
        facade: Facade,

    ) {
        super(facade, { manualInit: true });
        this.initObservers();
    }

    get actionInfo(): ActionInfo[] {
        return this._actionInfo;
    }

    close(info: ActionInfo) {
        this.facade.environment.removeActionInfo(info);
    }

    @StoreStateObserver()
    actionInfoChanged() {
        // We must reference the observable property (actionInfoCount) to wire-up the Mobx autorun method.
        // If we do not, this method will not be invoked when the property changes.
        this._actionInfo = cloneEntity(this.facade.environment.viewObservable.actionInfo);
    }
}
