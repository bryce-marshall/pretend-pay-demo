import { ReactiveViewModel, ReactiveViewModelArgs } from "./reactive-view-model";
import { Facade } from "../facade";

export abstract class PageViewModel extends ReactiveViewModel {

    constructor(facade: Facade, args?: ReactiveViewModelArgs) {
        super(facade, args);
    }
}
