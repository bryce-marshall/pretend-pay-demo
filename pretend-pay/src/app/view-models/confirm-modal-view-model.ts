import { Injectable } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { DialogRef } from "../presenters/";
import { ConfirmModalData } from "../lib/view/confirm-modal-data";
import { ComponentViewModel } from "./component-view-model";
import { Facade } from "../facade";

@Injectable()
export class ConfirmModalViewModel extends ComponentViewModel {
    title: string;
    message: string;
    cancelText: string;
    confirmText: string;

    constructor(
        facade: Facade,
        i18n: InternationalizationService,
        private dialogRef: DialogRef<ConfirmModalData>
    ) {
        super(facade);
        const data = dialogRef.data;
        data.isConfirmed = undefined;
        this.title = i18n.getTextInstant(data.titleResourceKey, data.formatArgs);
        this.message = i18n.getTextInstant(data.messageResourceKey, data.formatArgs);

        this.cancelText = i18n.getTextInstant("button." + (data.cancelButtonType ? data.cancelButtonType : "cancel"), data.formatArgs);
        this.confirmText = i18n.getTextInstant("button." + data.confirmButtonType);
    }

    cancel() {
        this.dialogRef.data.isConfirmed = false;
        this.dialogRef.dialog.close();
    }

    confirm() {
        this.dialogRef.data.isConfirmed = true;
        this.dialogRef.dialog.close();
    }
}
