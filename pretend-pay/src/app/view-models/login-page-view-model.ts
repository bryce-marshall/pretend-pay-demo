import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Facade } from "../facade";
import { ApiResponse, ApiResponseCode, KnownApiResponseCode } from "../lib/api-response";
import { FormState } from "../lib/view/form-state";
import { DockedViewModel } from "./docked-view-model";

export interface LoginPageFormControls {
  email: FormControl;
  password: FormControl;
}

@Injectable()
export class LoginPageViewModel extends DockedViewModel {
  private static _loginCount = 0;

  private _errorCode: ApiResponseCode;
  readonly form: FormGroup;
  readonly formState: FormState;
  readonly controls: LoginPageFormControls;

  constructor(facade: Facade) {
    super(facade);
    this.form = new FormGroup(
      {
        email: new FormControl("", [
          Validators.required,
          Validators.email
        ]),
        password: new FormControl("", [
          Validators.required
        ])
      });

    this.controls = <any>this.form.controls;
    this.formState = new FormState(this.form, this);

    // Automatically login when the app first loads (for demo purposes)
    if (LoginPageViewModel._loginCount === 0) {
      this.controls.email.setValue("john@doe.com");
      this.controls.password.setValue("password");
      this.login();
    }
  }

  get hasApiError(): boolean {
    return this._errorCode != undefined && this._errorCode !== KnownApiResponseCode.Ok;
  }

  get errorCode(): string {
    return this._errorCode;
  }

  //#region Action Methods
  async login() {
    if (!this.formState.evalSubmit()) return;

    this._errorCode = undefined;
    try {
      this.isBusyInt = true;
      const r = await this.facade.auth.login(this.controls.email.value, this.controls.password.value);
      this._errorCode = r.code;
      LoginPageViewModel._loginCount++;
    } catch (e) {
      this._errorCode = ApiResponse.as(e).code;
      this.isBusyInt = false;
    }
    finally {
      this.controls.password.setValue("");
      this.form.markAsPristine();
      this.form.markAsUntouched();
    }
  }
  //#endregion
}
