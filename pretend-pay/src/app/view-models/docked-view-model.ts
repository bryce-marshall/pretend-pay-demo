import { ReactiveViewModel, ReactiveViewModelArgs } from "./reactive-view-model";
import { Facade } from "../facade";

// TODO: Necessary? Prefer to delegate display of errors to main page component and store.
/**
 * The base class for all ViewModel implementations on the primary route.
 */
export abstract class DockedViewModel extends ReactiveViewModel {
    constructor(facade: Facade, args?: ReactiveViewModelArgs) {
        super(facade, args);
    }
}
