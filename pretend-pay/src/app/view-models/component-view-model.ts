import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { AppEnvironmentObservable, AppViewObservable, Facade } from "../facade";
import { NavigationFacade } from "../facade/navigation-facade";
import { MobxManager } from "../lib/mobx-manager";
import { SubscriptionManager } from "../lib/subscription-manager";
import { AppStateType, LanguageDirection, ViewContext, ViewportType } from "../lib/types";

/**
 * The base class for all ViewModel implementations.
 */
@Injectable()
export class ComponentViewModel implements OnDestroy {
    private _subscriptions = new SubscriptionManager();
    private _mobx = new MobxManager();
    private _viewportType: ViewportType = "tablet";
    private _viewportSubject: Subject<ViewportType>;
    protected readonly viewRef: AppViewObservable;
    protected readonly environmentRef: AppEnvironmentObservable;
    readonly viewportTypeChanged: Observable<ViewportType>;

    constructor(protected readonly facade: Facade) {
        this.viewRef = facade.environment.viewObservable;
        this.environmentRef = facade.environment.environmentObservable;
        this._viewportSubject = new Subject();
        this.viewportTypeChanged = this._viewportSubject.asObservable();
        this._mobx.autorun(() => {
            this._viewportType = facade.environment.viewObservable.viewportType;
            this._viewportSubject.next(this._viewportType);
        });
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
        this._mobx.dispose();
    }

    get navigate(): NavigationFacade {
        return this.facade.navigate;
    }

    get viewportType(): ViewportType {
        return this._viewportType;
    }

    get dir(): LanguageDirection {
        return this.environmentRef.dir;
    }

    get isPhone() {
        return this._viewportType === "phone";
    }

    get isTablet() {
        return this._viewportType === "tablet";
    }

    /**
     * Returns true if the current viewport-type is "phone", otherwise undefined.
     * This property is intended to be assigned to an element's attr collection, resulting in an attribute being created if
     * the language is rtl, and removed if it is not.
     */
    get phoneAttr() {
        return this.isPhone ? true : undefined;
    }

    /**
     * Returns true if the current viewport-type is "tablet", otherwise undefined.
     * This property is intended to be assigned to an element's attr collection, resulting in an attribute being created if
     * the language is ltr, and removed if it is not.
     */
    get tabletAttr() {
        return this.isTablet ? true : undefined;
    }

    //#region Observable Properties

    get isRtl() {
        return this.environmentRef.dir === "rtl";
    }

    get isLtr() {
        return this.environmentRef.dir !== "rtl";
    }

    /**
     * Returns true if the current language is rtl, otherwise undefined.
     * This property is intended to be assigned to an element's attr collection, resulting in an attribute being created if
     * the language is rtl, and removed if it is not.
     */
    get rtlAttr() {
        return this.isRtl ? true : undefined;
    }

    /**
     * Returns true if the current language is ltr, otherwise undefined.
     * This property is intended to be assigned to an element's attr collection, resulting in an attribute being created if
     * the language is ltr, and removed if it is not.
     */
    get ltrAttr() {
        return this.isRtl ? undefined : true;
    }

    get state(): AppStateType {
        return this.environmentRef.state;
    }

    get locale(): string {
        return this.environmentRef.locale;
    }

    get viewContext(): ViewContext {
        return this.viewRef.viewContext;
    }

    //#endregion

    //#region Action Methods

    setLanguage(languageCode: string) {
        this.facade.environment.setLanguage(languageCode);
    }

    logout() {
        this.facade.auth.logout();
    }

    //#endregion


    /**
     * Subscribes to an observable, and automatically unsubscribes when the ViewModel is destroyed.
     */
    protected subscribeTo<T>(observable: Observable<T>, next?: (value: T) => void, error?: (error: any) => void, complete?: () => void) {
        this._subscriptions.subscribeTo(observable, next, error, complete);
    }
}
