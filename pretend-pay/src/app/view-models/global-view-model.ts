import { Injectable } from "@angular/core";
import { Facade } from "../facade";
import { ComponentViewModel } from "./component-view-model";

/**
 * A globally-scoped ViewModel exposing observable properties and action methods common to minor application components.
 */
@Injectable({
    providedIn: "root"
})
export class GlobalViewModel extends ComponentViewModel {

    constructor(
        facade: Facade,
    ) {
        super(facade);
    }
}
