export interface StoreStateObserverDescriptor {
    readonly proto: any;
    readonly methodNames: string[];
}

export function StoreStateObserver() {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        let obs: StoreStateObserverDescriptor = target.__store_observers__;

        if (!obs) {
            obs = {
                proto: target,
                methodNames: []
            };

            target.__store_observers__ = obs;
        } else if (obs.proto !== target) {
            obs = {
                proto: target,
                methodNames: obs.methodNames.slice()
            };

            target.__store_observers__ = obs;
        }

        obs.methodNames.push(propertyKey);
    };
}
