import { autorun, IReactionDisposer } from "mobx";
import { argumentNullMessage } from "src/app/lib/utilities/error-message";
import { StoreStateObserverDescriptor } from "./decorators";

export interface HostedObserverSetConfig {
    traceFunction?: (message: string) => void;
}

export class HostedObserverSet {
    private _disposers: IReactionDisposer[] = [];
    private _traceFn: (message: string) => void;

    constructor(host: any | any[], config?: HostedObserverSetConfig) {
        if (host == undefined) throw new Error(argumentNullMessage("host"));
        this._traceFn = (config && typeof config.traceFunction === "function") ? config.traceFunction : () => { };
        this.proxyFor(host);
    }

    static dispose(set: HostedObserverSet) {
        if (set) {
            set._disposers.forEach(disposer => disposer());
        }
    }

    proxyFor(host: any | any[]) {
        if (Array.isArray(host)) {
            host.forEach((h) => {
                this.aggregateObservers(h);
            });
        } else {
            this.aggregateObservers(host);
        }
    }

    private aggregateObservers(host: any) {
        const observers: StoreStateObserverDescriptor = host["__proto__"].__store_observers__;
        if (!observers || !Array.isArray(observers.methodNames)) return;
        observers.methodNames.forEach(methodName => {
            const name = host["constructor"].name + "." + methodName;
            this._traceFn(`Registered MobX observer "${name}"`);
            this._disposers.push(autorun(() => {
                try {
                    host[methodName]();
                } catch (e) {
                    console.log(`*** Exception invoking autorun method "${methodName}".`);
                    console.log("*** host: ", host);
                    console.log("*** host[o.methodName]: ", host[methodName]);
                    throw e;
                }
            }, {
                name: name
            }));
        });
    }
}
