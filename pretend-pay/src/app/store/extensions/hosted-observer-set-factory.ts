import { AppLogger } from "../../services/logger/app-logger";
import { HostedObserverSet } from "./hosted-observer-set";

/**
 * A convenience factory method to create a HostedObserverSet with a configured trace logger.
 * @param host The instance or instances hosting methods adorned with a StoreStateObserver descriptor.
 * @param logger The logger to write trace data to.
 */
export function hostedObserverSetFactory(host: any | any[], logger: AppLogger): HostedObserverSet {
    return new HostedObserverSet(host,
        {
            traceFunction: (msg) => { logger.trace(msg); }
        }
    );

}
