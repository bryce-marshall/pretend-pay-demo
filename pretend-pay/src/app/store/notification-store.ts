import { Injectable } from "@angular/core";
import { AppNotification } from "../lib/app-notification";
import { observable, computed, action } from "mobx";
import { NotificationsObservable } from "./observables/notifications-observable";

@Injectable({
    providedIn: "root"
}) export class NotificationStore implements NotificationsObservable {
    @observable private _notifications: AppNotification[] = [];

    @computed get notifications(): AppNotification[] {
        return this._notifications;
    }

    @computed get notificationCount(): number {
        return this._notifications.length;
    }

    @action push(...notification: AppNotification[]) {
        // It is expected that the API will have returned notifications in ascending event sequence.
        // This store maintains notifications in descending event sequence, so that most recent notifications appear first in
        // any rendered list. Consequently, we reverse the array contents here.
        // A temporary local variable is necessary because the Mobx proxy doesn't perform the reverse function "in place".
        // Invoking the "slice" method suppresses the mobx warning.
        const t = this._notifications.slice().reverse();
        notification.forEach((n) => {
            if (n) t.push(n);
        });
        this._notifications = t.reverse();
    }

    @action remove(notification: AppNotification | number) {
        const id = typeof notification === "number" ? notification : notification.id;
        const idx = this._notifications.findIndex(cmp => cmp.id === id);
        if (idx >= 0) this._notifications.splice(idx, 1);
    }

    @action clear() {
        this._notifications = [];
    }

    @action reset() {
        this._notifications = [];
    }
}
