import { Injectable } from "@angular/core";
import { action, computed, observable } from "mobx";
import { ActionInfo } from "../lib/action-info";
import { Queue } from "../lib/collections/queue";
import { IdRegister } from "../lib/id-register";
import { ViewContext, ViewportType } from "../lib/types";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { argumentNullMessage } from "../lib/utilities/error-message";
import { ErrorInfo } from "../lib/view/error-info";
import { ToastData } from "../presenters/toast/toast-data";
import { AppViewObservable } from "./observables/app-view-observable";

@Injectable({
    providedIn: "root"
})
export class AppViewStore implements AppViewObservable {
    private _ids: IdRegister = new IdRegister();
    @observable private _viewportType: ViewportType;
    @observable private _viewContext: ViewContext;
    @observable private _toastArray = [];
    @observable private _errorsArray = [];

    /**
     * Action info to be displayed by the current view.
     */
    @observable private _actionInfo: ActionInfo[] = [];
    /**
     * Acion info to be shifted to the current actionInfo queue the next time the primary route changes.
     */
    private _errors: Queue<ErrorInfo>;
    private _toast: Queue<ToastData>;
    private _actionInfoNext = new Queue<ActionInfo>();

    constructor() {
        this._toast = new Queue(this._toastArray);
        this._errors = new Queue(this._errorsArray);
    }

    @computed get viewportType(): ViewportType {
        return this._viewportType;
    }

    @computed get viewContext(): ViewContext {
        return this._viewContext;
    }

    @computed get errorCount(): number {
        return this._errors.count;
    }

    @computed get toastCount(): number {
        return this._toast.count;
    }

    get actionInfo(): ActionInfo[] {
        return this._actionInfo;
    }

    @action setViewport(viewportType: ViewportType) {
        this._viewportType = viewportType;
    }

    @action setContext(viewContext: ViewContext) {
        this._viewContext = viewContext;
    }

    @action enqueueInfo(actionInfo: ActionInfo) {
        if (actionInfo == undefined) throw new Error(argumentNullMessage("actionInfo"));
        actionInfo = cloneEntity(actionInfo);
        actionInfo.id = this._ids.nextId();
        this._actionInfo.unshift(actionInfo);
    }

    @action removeInfo(actionInfo: ActionInfo | number) {
        const id = typeof actionInfo === "number" ? actionInfo : actionInfo.id;
        if (id == undefined) return;
        const idx = this._actionInfo.findIndex(cmp => cmp.id === id);
        if (idx >= 0) this._actionInfo.splice(idx, 1);
    }

    @action clearInfo() {
        this._actionInfo.splice(0, this._actionInfo.length);
    }

    @action enqueueInfoNext(actionInfo: ActionInfo) {
        if (actionInfo == undefined) throw new Error(argumentNullMessage("actionInfo"));
        actionInfo = cloneEntity(actionInfo);
        actionInfo.id = this._ids.nextId();
        return this._actionInfoNext.enqueue(actionInfo);
    }

    @action shiftActionInfo() {
        this.clearInfo();
        if (this._actionInfoNext.count > 0) {
            this._actionInfo.push(...this._actionInfoNext.dequeueAll());
        }
    }

    @action enqueueError(errorInfo: ErrorInfo) {
        if (errorInfo == undefined) throw new Error(argumentNullMessage("errorInfo"));
        return this._errors.enqueue(errorInfo);
    }

    @action dequeueError(): ErrorInfo {
        if (this._errors.count === 0) return undefined;
        return this._errors.dequeue();
    }

    @action enqueueToast(item: ToastData | string) {
        if (typeof item === "string") {
            item = {
                text: item
            };
        }

        this._toast.enqueue(item);
    }

    @action dequeueToast(): ToastData {
        if (this._toast.count === 0) return undefined;
        return this._toast.dequeue();
    }

    @action reset() {
        this.clearInfo();
        this._actionInfoNext.clear();
    }
}
