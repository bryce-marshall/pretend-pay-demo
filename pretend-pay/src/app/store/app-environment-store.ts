import { observable, action, computed } from "mobx";
import { Injectable } from "@angular/core";
import { AppStateType, LanguageDirection } from "../lib/types";
import { AppEnvironmentObservable } from "./observables/app-environment-observable";

@Injectable({
    providedIn: "root"
})
export class AppEnvironmentStore implements AppEnvironmentObservable {
    @observable private _state: AppStateType = "initializing";
    @observable private _locale: string;
    @observable private _dir: LanguageDirection = "ltr";

    @computed get state(): AppStateType {
        return this._state;
    }

    @computed get locale(): string {
        return this._locale;
    }

    @computed get dir(): LanguageDirection {
        return this._dir;
    }

    @action setState(state: AppStateType) {
        this._state = state;
    }

    @action setLocale(locale: string) {
        this._locale = locale;
    }

    @action setDir(dir: LanguageDirection) {
        this._dir = dir;
    }

    @action reset() {
    }
}
