import { action, computed, observable } from "mobx";
import { Injectable } from "@angular/core";
import { UserIdentity } from "../lib/user-identity";
import { ApiResponseCode } from "../lib/api-response";
import { IdentityStateType } from "../lib/types";
import { IdentityObservable } from "./observables/identity-observable";


@Injectable({
    providedIn: "root"
})
export class ActiveIdentityStore implements IdentityObservable {
    @observable private _state: IdentityStateType = "not-authenticated";
    @observable private _errorCode: ApiResponseCode;
    @observable private _id: string;
    @observable private _username: string;
    @observable private _displayName: string;

    @computed get state(): IdentityStateType {
        return this._state;
    }

    @computed get errorCode(): ApiResponseCode {
        return this._errorCode;
    }

    @computed get id(): string {
        return this._id;
    }

    @computed get username(): string {
        return this._username;
    }

    @computed get displayName(): string {
        return this._displayName;
    }

    @computed get isAuthenticated() {
        return this._state === "authenticated";
    }

    @action beginAuthenticating() {
        this._state = "authenticating";
        this.clearIdentity();
    }

    @action authenticated(identity: UserIdentity) {
        this._state = "authenticated";
        this._id = identity.id;
        this._username = identity.username;
        this._displayName = typeof identity.displayName === "string" ? identity.displayName : identity.username;
        this._errorCode = undefined;
    }

    @action authenticationError(errorCode: string) {
        this._state = "error";
        this._displayName = undefined;
        this._errorCode = errorCode;
    }

    @action notAuthenticated() {
        this._state = "not-authenticated";
        this.clearIdentity();
    }

    @action reset() {
        this._errorCode = undefined;
        this.clearIdentity();
    }

    private clearIdentity() {
        this._id = undefined;
        this._username = undefined;
        this._displayName = undefined;
    }
}
