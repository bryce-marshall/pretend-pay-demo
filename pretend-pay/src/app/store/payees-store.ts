import { observable, computed, action } from "mobx";
import { Injectable } from "@angular/core";
import { LoadingState } from "../lib/types";
import { SavedPayee } from "../lib/saved-payee";
import { cloneEntity } from "../lib/utilities/entity-clone";
import { PayeesObservable } from "./observables/payees-observable";

@Injectable({
    providedIn: "root"
})
export class PayeesStore implements PayeesObservable {
    @observable private _loadingState: LoadingState;
    @observable private _payees: SavedPayee[];

    @computed get loadingState(): LoadingState {
        return this._loadingState;
    }

    @computed get payees(): SavedPayee[] {
        return this._payees;
    }

    @action setLoadingState(loadingState: LoadingState) {
        this._loadingState = loadingState;
    }

    @action loadPayees(payees: SavedPayee[]) {
        this._payees = cloneEntity(payees);
        this._loadingState = "loaded";
    }

    @action deletePayee(payeeId: number) {
        const idx = this._payees.findIndex((cmp) => {
            return cmp.id === payeeId;
        });

        if (idx >= 0) this._payees.splice(idx, 1);
    }

    @action setPayee(payee: SavedPayee) {
        payee = cloneEntity(payee);
        const idx = this.payees.findIndex((cmp) => {
            return cmp.id === payee.id;
        });
        if (idx >= 0) {
            this._payees[idx] = payee;
        } else {
            this._payees.push(payee);
        }
    }

    @action reset() {
        this._loadingState = undefined;
        this._payees = undefined;
    }
}
