import { observable, computed, action } from "mobx";
import { Injectable } from "@angular/core";
import { LoadingState, SyncState } from "../lib/types";
import { FinancialAccount } from "../lib/financial-account";
import { AccountsObservable } from "./observables/accounts-observable";
import { cloneEntity } from "../lib/utilities/entity-clone";

@Injectable({
    providedIn: "root"
})
export class AccountsStore implements AccountsObservable {
    @observable private _loadingState: LoadingState;
    @observable private _syncState: SyncState = "out-of-sync";
    @observable private _accounts: FinancialAccount[];
    @observable private _outgoingTxCount = 0;

    @computed get loadingState(): LoadingState {
        return this._loadingState;
    }

    @computed get syncState(): SyncState {
        return this._syncState;
    }

    @computed get outgoingTxCount(): number {
        return this._outgoingTxCount;
    }

    @computed get accounts(): FinancialAccount[] {
        return this._accounts;
    }

    @action incOutgoingTxCount() {
        this._outgoingTxCount++;
    }

    @action setLoadingState(loadingState: LoadingState) {
        this._loadingState = loadingState;
    }

    @action setSyncState(syncState: SyncState) {
        this._syncState = (syncState && <any>syncState !== "") ? syncState : "out-of-sync";
    }

    @action loadAccounts(accounts: FinancialAccount[]) {
        this._accounts = cloneEntity(accounts);
        this._loadingState = "loaded";
    }

    @action clearAccounts() {
        this._loadingState = undefined;
        this._accounts = [];
    }

    @action reset() {
        this._loadingState = undefined;
        this._syncState = "out-of-sync";
        this._accounts = undefined;
        this._outgoingTxCount = 0;
    }
}
