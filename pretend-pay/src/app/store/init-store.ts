import { configure } from "mobx";

export function initStore() {
    configure({
        enforceActions: "observed"
    });
}
