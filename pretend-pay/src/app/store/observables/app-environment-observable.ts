import { AppStateType, LanguageDirection } from "src/app/lib/types";

export interface AppEnvironmentObservable {
    state: AppStateType;
    locale: string;
    dir: LanguageDirection;
}
