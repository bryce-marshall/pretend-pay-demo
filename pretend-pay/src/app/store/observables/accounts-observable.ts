import { FinancialAccount } from "src/app/lib/financial-account";
import { LoadingStateProvider } from "src/app/lib/loading-state-provider";
import { SyncState } from "src/app/lib/types";

export interface AccountsObservable extends LoadingStateProvider {
    readonly accounts: FinancialAccount[];
    readonly syncState: SyncState;
}
