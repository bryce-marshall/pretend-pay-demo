import { SavedPayee } from "src/app/lib/saved-payee";
import { LoadingStateProvider } from "src/app/lib/loading-state-provider";

export interface PayeesObservable extends LoadingStateProvider {
    payees: SavedPayee[];
}
