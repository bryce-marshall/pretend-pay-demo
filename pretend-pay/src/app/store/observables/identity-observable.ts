import { IdentityStateType } from "src/app/lib/types";
import { ApiResponseCode } from "src/app/lib/api-response";

export interface IdentityObservable {
    state: IdentityStateType;
    errorCode: ApiResponseCode;
    id: string;
    username: string;
    displayName: string;
    isAuthenticated: boolean;
}
