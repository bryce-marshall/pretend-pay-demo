import { ViewContext, ViewportType } from "src/app/lib/types";
import { ActionInfo } from "src/app/lib/action-info";

export interface AppViewObservable {
    viewportType: ViewportType;
    viewContext: ViewContext;
    errorCount: number;
    toastCount: number;
    actionInfo: ActionInfo[];
}
