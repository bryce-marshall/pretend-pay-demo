import { AppNotification } from "src/app/lib/app-notification";

export interface NotificationsObservable {
    readonly notifications: AppNotification[];
    readonly notificationCount: number;
}
