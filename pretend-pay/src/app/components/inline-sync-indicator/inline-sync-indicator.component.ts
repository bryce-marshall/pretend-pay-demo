import { Component, OnInit, Input } from "@angular/core";
import { SyncState } from "src/app/lib/types";

@Component({
  selector: "app-inline-sync-indicator",
  templateUrl: "./inline-sync-indicator.component.html",
  styleUrls: ["./inline-sync-indicator.component.scss"]
})
export class InlineSyncIndicatorComponent implements OnInit {
  @Input()
  syncState: SyncState;

  constructor() { }

  ngOnInit() {
  }

  get icon(): string {
    switch (this.syncState) {
      case "in-sync":
        return "check_circle_outline";
      case "syncing":
        return "sync";
      default:
        return "sync_problem";
    }
  }

  get inSyncAttr(): any {
    return this.syncState === "in-sync" ? "" : undefined;
  }

  get outOfSyncAttr(): any {
    return this.syncState === "out-of-sync" ? "" : undefined;
  }

  get syncingAttr(): any {
    return this.syncState === "syncing" ? "" : undefined;
  }
}
