import { Component } from "@angular/core";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { StoreStateObserver} from "src/app/store/extensions";
import { GlobalViewModel } from "src/app/view-models";
import { AppConfig } from "src/app/config/app-config";

@Component({
  selector: "app-language-selector",
  templateUrl: "./language-selector.component.html",
  styleUrls: ["./language-selector.component.scss"]
})
export class LanguageSelectorComponent extends ReactiveComponentBase {

  locales: string[] = AppConfig.settings.locales;
  currentLanguage: string;

  constructor(
    private viewModel: GlobalViewModel) {
    super();
  }

  isCurrent(code: string): boolean {
    return code === this.currentLanguage;
  }

  @StoreStateObserver()
  onLocaleChanged() {
    this.currentLanguage = this.viewModel.locale;
  }

  languageButtonClick(languageCode: string) {
    this.viewModel.setLanguage(languageCode);
  }
}
