import { Component, OnInit, Input } from "@angular/core";
import { BusyStateProvider } from "src/app/lib/view/busy-state-provider";

@Component({
  selector: "app-busystate-wrapper",
  templateUrl: "./busystate-wrapper.component.html",
  styleUrls: ["./busystate-wrapper.component.scss"]
})
export class BusyStateWrapperComponent implements OnInit {
  @Input() state: BusyStateProvider;
  @Input() suppressLoading: boolean;
  @Input() suppressBusy: boolean;

  constructor() { }

  ngOnInit() {
  }

  get isLoading(): boolean {
    return !this.suppressLoading && this.state && this.state.loadingState === "loading";
  }

  get isBusy(): boolean {
    return !this.suppressBusy && this.state && this.state.isBusy;
  }
}
