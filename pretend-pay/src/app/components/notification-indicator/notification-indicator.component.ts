import { Component } from "@angular/core";
import { NotificationIndicatorViewModel } from "src/app/view-models/notification-indicator.view-model";
import { AppNotification } from "src/app/lib/app-notification";

@Component({
  selector: "app-notification-indicator",
  templateUrl: "./notification-indicator.component.html",
  styleUrls: ["./notification-indicator.component.scss"],
  providers: [NotificationIndicatorViewModel]
})
export class NotificationIndicatorComponent {

  constructor(public viewModel: NotificationIndicatorViewModel) { }

  get badge(): number {
    return this.viewModel.notificationCount > 0 ? this.viewModel.notificationCount : undefined;
  }

  dismissAllClick() {
    this.viewModel.dismissAll();
  }

  dismissClick(event: Event, notification: AppNotification) {
    if (this.viewModel.notificationCount > 1) event.stopPropagation(); // Prevent the Material menu component from closing
    this.viewModel.dismiss(notification);
  }
}
