import { Component, Input } from "@angular/core";
import { FinancialAccount } from "src/app/lib/financial-account";
import { GlobalViewModel } from "src/app/view-models";

@Component({
  selector: "app-account-header",
  templateUrl: "./account-header.component.html",
  styleUrls: ["./account-header.component.scss"]
})
export class AccountHeaderComponent {
  @Input() account: FinancialAccount;
  @Input() isRtl: boolean;

  constructor(public viewModel: GlobalViewModel) {
  }

  get isPhone(): boolean {
    return this.viewModel.viewportType === "tablet";
  }
}
