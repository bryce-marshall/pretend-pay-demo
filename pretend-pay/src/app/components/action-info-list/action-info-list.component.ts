import { Component } from "@angular/core";
import { ActionInfoViewModel } from "src/app/view-models/action-info-view-model";
import { ActionInfo } from "src/app/lib/action-info";

@Component({
  selector: "app-action-info-list",
  templateUrl: "./action-info-list.component.html",
  styleUrls: ["./action-info-list.component.scss"],
  providers: [ActionInfoViewModel]
})
export class ActionInfoListComponent {

  constructor(protected viewModel: ActionInfoViewModel) {
  }

  get actionInfo(): ActionInfo[] {
    return this.viewModel.actionInfo;
  }

  closeClick(info: ActionInfo) {
    this.viewModel.close(info);
  }
}
