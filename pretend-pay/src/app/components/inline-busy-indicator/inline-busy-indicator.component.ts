import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-inline-busy-indicator",
  templateUrl: "./inline-busy-indicator.component.html",
  styleUrls: ["./inline-busy-indicator.component.scss"]
})
export class InlineBusyIndicatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
