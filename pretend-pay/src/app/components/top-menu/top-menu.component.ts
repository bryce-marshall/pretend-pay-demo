import { Component } from "@angular/core";
import { GlobalViewModel } from "src/app/view-models";
import { FeedbackDialogViewComponent } from "src/app/views/feedback-dialog-view/feedback-dialog-view.component";

@Component({
  selector: "app-top-menu",
  templateUrl: "./top-menu.component.html",
  styleUrls: ["./top-menu.component.scss"]
})
export class TopMenuComponent {

  constructor(public viewModel: GlobalViewModel) {
  }

  feedbackClick() {
    this.viewModel.navigate.openDialog(FeedbackDialogViewComponent);
  }

  profileClick() {
    this.viewModel.navigate.profile();
  }

  logoutClick() {
    this.viewModel.logout();
  }
}
