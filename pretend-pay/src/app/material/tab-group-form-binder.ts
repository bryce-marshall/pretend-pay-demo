import { FormControl } from "@angular/forms";
import { MatTabGroup } from "@angular/material/tabs";
import { Subscription } from "rxjs";
import { argumentNullMessage } from "../lib/utilities/error-message";
import { LifecycleEventSource } from "../lib/view";

export class TabGroupFormBinder<TValue> {
    private _context: MatTabGroup;
    private _subscription: Subscription;
    private _assignedIndex: number;

    constructor(
        host: LifecycleEventSource,
        private controlAccessor: () => MatTabGroup,
        private formControl: FormControl,
        private valueMap: TValue[]
    ) {
        if (!controlAccessor) throw new Error(argumentNullMessage("tabGroup"));
        if (!formControl) throw new Error(argumentNullMessage("formControl"));
        if (!valueMap) throw new Error(argumentNullMessage("valueMap"));

        this.formControl.valueChanges.subscribe((value) => {
            this.setActiveTab(value);
        });

        this.setActiveTab(this.formControl.value);
        // If setActiveTab throws then this event handler will never be attached.
        host.doCheckEvent.subscribe(() => {
            this.refresh();
        });
    }

    get selected(): TValue {
        return this.formControl.value;
    }

    private setActiveTab(value: TValue) {
        const idx = this.valueMap.findIndex((cmp) => {
            return value === cmp;
        });

        // If the form control is simply unassigned, we default to the first item.
        // tslint:disable-next-line: max-line-length
        if (idx < 0 && value && <any>value !== "") throw new Error(`The TabGroupFormBinder form control was set to an unmapped value "${value}".`);
        this._assignedIndex = idx > 0 ? idx : 0;
    }

    private refresh() {
        const tabGroup = this.controlAccessor();
        if (tabGroup && this._context === tabGroup) return;
        if (this._subscription) this._subscription.unsubscribe();
        this._context = tabGroup;
        if (tabGroup == undefined) return;
        // tslint:disable-next-line: max-line-length
        if (this.valueMap.length !== tabGroup._tabs.length) throw new Error("There must be one map item for each tab in the group.");

        tabGroup.selectedIndex = this._assignedIndex;

        this._subscription = tabGroup.selectedIndexChange.subscribe(() => {
            this._assignedIndex = tabGroup.selectedIndex;
            this.formControl.setValue(this.valueMap[tabGroup.selectedIndex]);
        });
    }
}
