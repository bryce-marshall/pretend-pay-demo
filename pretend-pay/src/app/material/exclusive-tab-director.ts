import { AutoExclusiveControlDirector, ExclusiveControlGroup, OrdinalGroupSelectorAccessor } from "src/app/lib/view";
import { LifecycleEventSource } from "../lib/view";
import { MatTabGroup } from "@angular/material/tabs";
import { Subscription } from "rxjs";

class TabGroupAccessor<TValue = string> extends OrdinalGroupSelectorAccessor<TValue> {
    constructor(private controlAccessor: () => MatTabGroup) {
        super();
    }

    getControlContext(): any {
        return this.controlAccessor();
    }

    subscribeToChangeEvent(handler: () => void): Subscription {
        return this.controlAccessor().selectedIndexChange.subscribe(handler);
    }

    getSelectedIndex(): number {
        return this.controlAccessor().selectedIndex;
    }

    setSelected(ordinal: number) {
        this.controlAccessor().selectedIndex = ordinal;
    }
}

export class ExclusiveTabDirector<TValue = string> extends AutoExclusiveControlDirector<TValue> {
    constructor(
        host: LifecycleEventSource,
        controlAccessor: () => MatTabGroup,
        sequence: ExclusiveControlGroup<TValue>[],
        defaultGroup?: TValue) {

        if (!defaultGroup && sequence && sequence.length > 0) defaultGroup = sequence[0].type;
        super(host, new TabGroupAccessor(controlAccessor), defaultGroup, sequence);
    }
}
