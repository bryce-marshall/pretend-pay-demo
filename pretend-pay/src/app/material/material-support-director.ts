import { Injectable, OnDestroy } from "@angular/core";
import { DateAdapter } from "@angular/material/core";
import { argumentNullMessage } from "../lib/utilities/error-message";
import { AppLogger } from "../services/logger/app-logger";
import { AppEnvironmentStore } from "../store";
import { HostedObserverSet, hostedObserverSetFactory, StoreStateObserver } from "../store/extensions";

/**
 * Syncronizes the MaterialDesign DateAdapter with the application locale.
 * Ideally, this would be a global service provided in the root injector, however there is a known issue with the DateAdapter that causes
 * lazy-loaded modules to receive a new  instance of it.
 *
 * https://github.com/angular/material2/issues/12891
 *
 * The workaround in this instance is to require (root) components of lazy-loaded app modules to inject and host MaterialSupportDirector
 */
@Injectable()
export class MaterialSupportDirector implements OnDestroy {
    private _observers: HostedObserverSet;

    constructor(
        private logger: AppLogger,
        private env: AppEnvironmentStore,
        private dateAdapter: DateAdapter<any>
    ) {
        this.logger = logger.contextFor("MaterialSupportDirector");
        if (!this.dateAdapter) this.logger.error("No Material DateAdapter was able to be resolved");
    }

    /**
     * The init method must be invoked to wire-up observables.
     * This could be done in the constructor, however, this would result in an unreferenced injected MaterialSupportDirector constructor
     * parameter in the host component, which could result in an uninformed developer removing the parameter and thus breaking the
     * localization feature. This approach requiress the developer to invoke init() from the host component"s constructor.
     */
    init(component: any) {
        if (!component) throw new Error(argumentNullMessage("component"));
        if (this._observers) throw new Error("init has already been called.");
        this.logger.trace(`Initialized by an instance of ${component["constructor"].name}`);
        this._observers = hostedObserverSetFactory(this, this.logger);
    }

    ngOnDestroy() {
        HostedObserverSet.dispose(this._observers);
    }

    @StoreStateObserver()
    localeChanged() {
        // IMPORTANT: this.env.locale MUST be referenced before this method returns, otherwise the MobX Observer will not be initialized.
        let locale: string = this.env.locale;
        if (!this.dateAdapter) return;
        if (!locale) locale = "";
        this.dateAdapter.setLocale(locale);
        this.logger.trace(`Applied the "${locale}" locale to the Material Design date adapter`);
    }
}
