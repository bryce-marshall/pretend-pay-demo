import { ReactiveComponentBase } from "../lib/view/reactive-component-base";
import { MaterialSupportDirector } from "./material-support-director";
import { Directive } from "@angular/core";

@Directive()
export abstract class MaterialPage  extends ReactiveComponentBase {

    constructor(materialSupport: MaterialSupportDirector) {
        super();
        materialSupport.init(this);
    }
}
