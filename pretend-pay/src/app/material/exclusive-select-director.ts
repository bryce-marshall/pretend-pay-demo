import { AutoExclusiveControlDirector, ExclusiveControlGroup, ValueGroupSelectorAccessor } from "src/app/lib/view";
import { LifecycleEventSource } from "../lib/view";
import { MatSelect } from "@angular/material/select";
import { Subscription } from "rxjs";
import { FormControl } from "@angular/forms";

class SelectAccessor<TValue = string> extends ValueGroupSelectorAccessor<TValue> {
    constructor(
        private controlAccessor: () => MatSelect,
        private accessorFormControl: FormControl
    ) {
        super();
    }

    getControlContext(): any {
        return this.controlAccessor();
    }

    subscribeToChangeEvent(handler: () => void): Subscription {
        return this.controlAccessor().selectionChange.subscribe(handler);
    }

    getSelectedValue(): TValue {
        return this.accessorFormControl.value;
    }

    setSelected(_ordinal: number, value: TValue) {
        this.accessorFormControl.setValue(value);
    }
}

export class ExclusiveSelectDirector<TValue = string> extends AutoExclusiveControlDirector<TValue> {
    constructor(
        host: LifecycleEventSource,
        controlAccessor: () => MatSelect,
        accessorFormControl: FormControl,
        sequence: ExclusiveControlGroup<TValue>[],
        defaultGroup?: TValue) {

        super(host, new SelectAccessor(controlAccessor, accessorFormControl), defaultGroup, sequence);
    }
}
