import { NgModule } from "@angular/core";
import { MatMomentDateModule, MomentDateAdapter } from "@angular/material-moment-adapter";
import { DateAdapter } from "@angular/material/core";

const modules = [
    MatMomentDateModule,
];

@NgModule({
    imports: modules,
    exports: modules,
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter },
    ]
})
export class MaterialSupportModule {
}
