import { ComponentType } from "@angular/cdk/portal";
import { Injectable, TemplateRef } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { Observable } from "rxjs";
import { DialogConfig } from "src/app/presenters";
import { DialogControllerHandle, DialogImp, DialogInstanceHandle, DialogObservableType } from "src/app/presenters/implementors";

@Injectable()
export class MaterialDialog extends DialogImp {
    constructor(private dialog: MatDialog) {
        super();
    }

    getControllerHandle(): DialogControllerHandle {
        return this.dialog;
    }

    releaseController(handle: any) {
        // No action required
    }

    createInstance(_controller: DialogControllerHandle, componentOrTemplateRef: ComponentType<any> | TemplateRef<any>,
        config?: DialogConfig<any>): DialogInstanceHandle {

        return EnclosedDialogWrapper(this.dialog.open(componentOrTemplateRef, {
            data: config.data,
            width: config.width,
            height: config.height,
            minWidth: config.minWidth,
            minHeight: config.minHeight,
            maxWidth: config.maxWidth,
            maxHeight: config.maxHeight
        }));
    }

    closeInstance(handle: DialogInstanceHandle, result?: any) {
        (<DialogWrapper>handle).close(result);
    }

    isInstanceClosed(handle: DialogInstanceHandle): boolean {
        return (<DialogWrapper>handle).isClosed;
    }

    getInstanceObservable(handle: DialogInstanceHandle, type: DialogObservableType): Observable<any> {
        switch (type) {
            case "AfterClosed":
                return (<DialogWrapper>handle).getAfterClosed();

            default:
                throw new Error(`The observable type ${type} is not supported by this implementation.`);
        }
    }

    getComponentInstance(handle: DialogInstanceHandle): any {
        return (<DialogWrapper>handle).getAfterClosed();
    }
}

interface DialogWrapper {
    readonly isClosed: boolean;
    close(result?: any);
    getAfterClosed(): Observable<any>;
    getComponent(): any;
}

function EnclosedDialogWrapper(itemRef: MatDialogRef<any>): DialogWrapper {
    let closed = false;
    const subscription = itemRef.afterClosed().subscribe(() => {
        closed = true;
        subscription.unsubscribe();
    });
    return {
        get isClosed(): boolean {
            return closed;
        },

        close: (result?: any) => {
            itemRef.close(result);
            closed = true;
        },

        getAfterClosed(): Observable<any> {
            return itemRef.afterClosed();
        },

        getComponent(): any {
            return itemRef.componentInstance;
        }
    };
}
