import { Injectable } from "@angular/core";
import { MatSnackBar, MatSnackBarRef } from "@angular/material/snack-bar";
import { Observable } from "rxjs";
import { ToastData } from "src/app/presenters";
import { ToastControllerHandle, ToastImp, ToastInstanceHandle, ToastObservableType } from "src/app/presenters/implementors";

@Injectable()
export class MaterialToast extends ToastImp {
    constructor(private snackBar: MatSnackBar) {
        super();
    }

    getControllerHandle(): ToastControllerHandle {
        return this.snackBar;
    }

    releaseController(handle: any) {
        // No action required
    }

    createInstance(controller: ToastControllerHandle, data: ToastData): ToastInstanceHandle {
        return EnclosedSnackBarWrapper(controller, this.snackBar.open(data.text, undefined, {
            panelClass: "default-snack-bar",
            verticalPosition: "bottom",
            duration: data.duration
        }));
    }

    closeInstance(handle: ToastInstanceHandle) {
        (<SnackBarWrapper>handle).close();
    }

    isInstanceClosed(handle: ToastInstanceHandle): boolean {
        return (<SnackBarWrapper>handle).isClosed;
    }

    getInstanceObservable(handle: ToastInstanceHandle, type: ToastObservableType): Observable<any> {
        switch (type) {
            case "AfterDismissed":
                return (<SnackBarWrapper>handle).getAfterDismissed();
            default:
                throw new Error(`The observable type ${type} is not supported by this implementation.`);
        }
    }
}

interface SnackBarWrapper {
    readonly isClosed: boolean;
    close();
    getAfterDismissed(): Observable<any>;
}

function EnclosedSnackBarWrapper(snackBar: MatSnackBar, itemRef: MatSnackBarRef<any>): SnackBarWrapper {
    let closed = false;
    const subscription = itemRef.afterDismissed().subscribe(() => {
        closed = true;
        subscription.unsubscribe();
    });
    return {
        get isClosed(): boolean {
            return closed;
        },

        close: () => {
            // Only dismiss the currently visible snackbar if it is this instance
            if (itemRef && snackBar._openedSnackBarRef === itemRef) snackBar.dismiss();
            closed = true;
        },

        getAfterDismissed(): Observable<any> {
            return itemRef.afterDismissed();
        }
    };
}
