import { Pipe, PipeTransform } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { TransactionData } from "../lib/transaction-data";

/**
 * Formats dates in short format for transaction entries according to the active user's locale.
 * Note that this pipe is used instead of the Angular DatePipe so as to allow better integration with the application's
 * dynamic locale system. This pipe is impure, so that its output is re-rendered whenever the application locale changes.
 */
@Pipe({
    name: "txData",
    pure: false
})
export class TransactionDataPipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }
    
    transform(value: TransactionData): any {
        return this.i18n.getTextInstant("tx-data-format." + value.type, value);
    }
}
