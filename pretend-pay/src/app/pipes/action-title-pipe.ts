import { Pipe, PipeTransform } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { ActionInfo } from "../lib/action-info";

/**
 * Transforms an ActionInfo instance into a localized action context message.
 */
@Pipe({
    name: "actionTitle",
    pure: false
})
export class ActionTitlePipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }
    
    transform(value: ActionInfo): any {
        return value.titleResource ? this.i18n.response.getActionInfoTextInstant(value.titleResource, value.args) : "";
    }
}
