import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "appletTitle"
})
export class AppletTitlePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return "view-title." + value;
  }
}
