import { Pipe, PipeTransform } from "@angular/core";
import { argumentMessage, argumentNullMessage } from "../lib/utilities/error-message";
import { InternationalizationService } from "../services/i18n/internationalization.service";

/**
 * Formats numeric values as currency values according to the active user's locale.
 * Note that this pipe is used instead of the Angular CurrencyPipe so as to allow better integration with the application's
 * dynamic locale system. This pipe is impure, so that its output is re-rendered whenever the application locale changes.
 */
@Pipe({
  name: "formatCurrency",
  pure: false
})
export class FormatCurrencyPipe implements PipeTransform {
  constructor(private i18n: InternationalizationService) {
  }

  transform(value: number, currencyCode: any, sign?: string, operator?: string): any {
    if (currencyCode == undefined) throw new Error(argumentNullMessage("currencyCode"));
    if (typeof currencyCode !== "string" || currencyCode.length === 0) throw new Error(argumentMessage("currencyCode"));

    if (sign === "pos" && value < 0 || sign === "neg" && value >= 0) return "";

    if (operator === "abs") value = Math.abs(value);
    
    return this.i18n.format.currency(value, currencyCode);
  }
}
