import { Pipe, PipeTransform } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { Translatable } from "../lib/view";

/**
 * Formats text using the Translatable key to resolve a localized template and the Translatable args as format input.
 * This pipe is impure, so that its output is re-rendered whenever the application locale changes.
 */
@Pipe({
    name: "translatable",
    pure: false
})
export class TranslatablePipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }
    
    transform(value: Translatable): any {
        return this.i18n.getTextInstant(value.key, value.args);
    }
}
