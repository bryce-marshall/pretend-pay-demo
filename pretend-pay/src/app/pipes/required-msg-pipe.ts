import { Pipe, PipeTransform } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";

/**
 * Formats a generic required validation error message using the specified resource key to resolve the field name.
 * This pipe is impure, so that its output is re-rendered whenever the application locale changes.
 */
@Pipe({
    name: "requiredMsg",
    pure: false
})
export class RequiredMessagePipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }

    transform(key: string): any {
        return this.i18n.getTextInstant("validation.generic-required", { name: this.i18n.getTextInstant(key) });
    }
}
