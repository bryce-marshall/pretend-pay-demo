import { Pipe, PipeTransform } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";

/**
 * Formats text using a localized template resolved by extending a base key with the LocalizedQuantityDescriptor
 * applicable to the specified value. This pipe is impure, so that its output is re-rendered whenever the application locale changes.
 */
@Pipe({
    name: "quantityDescriptor",
    pure: false
})
export class QuantityDescriptorPipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }

    transform(baseKey: string, quantity: number, args?: any): any {
        baseKey = baseKey + "." + this.i18n.getQuantityDescriptor(quantity);
        
        return this.i18n.getTextInstant(baseKey, args);
    }
}
