import { Pipe, PipeTransform } from "@angular/core";
import { FinancialAccount } from "../lib/financial-account";
import { InternationalizationService } from "../services/i18n/internationalization.service";

/**
 * Formats an available balance message for a specified account.
 * This pipe is impure, so that its output is re-rendered whenever the application locale changes.
 */
@Pipe({
    name: "accountAvailable",
    pure: false
})
export class AccountAvailablePipe implements PipeTransform {
    constructor(private readonly i18n: InternationalizationService) {
    }

    transform(account: FinancialAccount): any {
        const available = this.i18n.format.currency(account.available > 0 ? account.available : 0, account.currency);

        return this.i18n.getTextInstant("format.account-available", { available });
    }
}
