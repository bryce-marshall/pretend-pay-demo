import { Pipe, PipeTransform } from "@angular/core";
import { ActionInfo } from "../lib/action-info";
import { InternationalizationService } from "../services/i18n/internationalization.service";

/**
 * Transforms an ActionInfo instance into a localized action outcome message.
 */
@Pipe({
    name: "actionOutcome",
    pure: false
})
export class ActionOutcomePipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }
    transform(value: ActionInfo): any {
        return this.i18n.response.getActionInfoTextInstant(value.outcomeResource, value.args);
    }
}
