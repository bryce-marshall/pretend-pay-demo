import { Pipe, PipeTransform } from "@angular/core";
import { InternationalizationService } from "../services/i18n/internationalization.service";

@Pipe({
  name: "apiErrorMessage",
  pure: false
})
export class ApiErrorMessagePipe implements PipeTransform {
    constructor(private i18n: InternationalizationService) {
    }

  transform(value: any, args?: any): any {
    return this.i18n.response.getErrorMessageInstant(value);
  }
}
