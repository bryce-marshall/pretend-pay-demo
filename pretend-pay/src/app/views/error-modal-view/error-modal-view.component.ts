import { Component } from "@angular/core";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { DialogRef } from "src/app/presenters";
import { ErrorModalViewModel } from "src/app/view-models/error-modal-view-model";

@Component({
  selector: "app-error-modal",
  templateUrl: "./error-modal-view.component.html",
  styleUrls: ["./error-modal-view.component.scss"],
  providers: [DialogRef, ErrorModalViewModel]
})
export class ErrorModalViewComponent extends ReactiveComponentBase {
  constructor(
    public viewModel: ErrorModalViewModel
  ) {
    super();
  }

  closeClick() {
    this.viewModel.closeModal();
  }
}
