import { animate, state, style, transition, trigger } from "@angular/animations";
import { Component, OnDestroy } from "@angular/core";
import { AccountTransaction } from "src/app/lib/account-transaction";
import { FinancialAccount } from "src/app/lib/financial-account";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { AppErrorStateMatcher } from "src/app/material";
import { TransactionsViewFormControls, TransactionsViewModel } from "src/app/view-models/transactions-view-model";

@Component({
  selector: "app-account-transactions-view",
  templateUrl: "./account-transactions-view.component.html",
  styleUrls: ["./account-transactions-view.component.scss"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0", display: "none" })),
      state("expanded", style({ height: "*" })),
      transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")),
    ]),
  ],
  providers: [TransactionsViewModel]
})
export class AccountTransactionsViewComponent extends ReactiveComponentBase implements OnDestroy {
  matcher = new AppErrorStateMatcher();
  controls: TransactionsViewFormControls;
  tabletColumns: string[] = ["date", "memo", "out", "in", "balance"];
  phoneColumns: string[] = ["date", "out", "in", "balance"];
  readonly fromStartDate: Date;
  readonly toStartDate: Date;
  filterVisible = false;

  expandedTx: AccountTransaction;

  constructor(
    public viewModel: TransactionsViewModel
  ) {
    super();
    const now = new Date(Date.now());
    this.fromStartDate = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate());
    this.toStartDate = new Date(Date.now());
    this.controls = viewModel.controls;
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    // Works-around an issue with Material Design Table component on the Microsoft Edge browser, which results in
    // the browser entering a text-selection mode wherein mouse movement causes page content to be selected.
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    } else if ((<any>document).selection) {
      (<any>document).selection.empty();
    }
  }

  get account(): FinancialAccount {
    return this.viewModel.focusedAccount;
  }

  get transactions(): AccountTransaction[] {
    return this.viewModel.transactions;
  }

  toggleExpandedTx(tx: AccountTransaction) {
    this.expandedTx = this.expandedTx === tx ? undefined : tx;
  }

  clearSearchClick() {
    this.viewModel.controls.text.setValue("");
  }

  searchClick() {
    this.viewModel.search();
  }

  toggleFilterClick() {
    this.filterVisible = !this.filterVisible;
  }

  clearFilter() {
    this.viewModel.clearFilter();
  }

  applyFilter() {
    this.viewModel.applyFilter();
  }

  backClick() {
    this.viewModel.navigate.accounts();
  }
}
