import { Component } from "@angular/core";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { DialogRef } from "src/app/presenters";
import { ConfirmModalViewModel } from "src/app/view-models/confirm-modal-view-model";

@Component({
  selector: "app-confirm-modal",
  templateUrl: "./confirm-modal-view.component.html",
  styleUrls: ["./confirm-modal-view.component.scss"],
  providers: [DialogRef, ConfirmModalViewModel]
})
export class ConfirmModalViewComponent extends ReactiveComponentBase {
  constructor(
    public viewModel: ConfirmModalViewModel
  ) {
    super();
  }

  cancelClick() {
    this.viewModel.cancel();
  }

  confirmClick() {
    this.viewModel.confirm();
  }
}
