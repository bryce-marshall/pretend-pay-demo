import { Component } from "@angular/core";
import { SavedPayee } from "src/app/lib/saved-payee";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { PayeesViewModel } from "src/app/view-models";

@Component({
  selector: "app-payees-view",
  templateUrl: "./payees-view.component.html",
  styleUrls: ["./payees-view.component.scss"],
  providers: [PayeesViewModel]
})
export class PayeesViewComponent extends ReactiveComponentBase {
  constructor(
    public viewModel: PayeesViewModel
  ) {
    super();
  }

  createClick() {
    this.viewModel.newPayee();
  }

  editClick(payee: SavedPayee) {
    this.viewModel.editPayee(payee);
  }

  deleteClick(payee: SavedPayee) {
    this.viewModel.confirmDeletePayee(payee);
  }
}
