import { Component, ViewChild } from "@angular/core";
import { MatTabGroup } from "@angular/material/tabs";
import { UserPaymentType } from "src/app/lib/types";
import { FormState } from "src/app/lib/view/form-state";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { AppErrorStateMatcher, TabGroupFormBinder } from "src/app/material";
import { PayementViewFormControls, PaymentViewModel } from "src/app/view-models/payment-view-model";

@Component({
  selector: "app-payments-view",
  templateUrl: "./payments-view.component.html",
  styleUrls: ["./payments-view.component.scss"],
  providers: [PaymentViewModel]
})
export class PaymentsViewComponent extends ReactiveComponentBase {
  private _tabBinder: TabGroupFormBinder<UserPaymentType>;
  matcher = new AppErrorStateMatcher();
  formControls: PayementViewFormControls;
  state: FormState;

  @ViewChild("paymentTypeTabGroup") paymentTypeTabGroup: MatTabGroup;

  constructor(
    public viewModel: PaymentViewModel
  ) {
    super();
    this.formControls = viewModel.controls;
    this.state = viewModel.formState;

    this._tabBinder = new TabGroupFormBinder<UserPaymentType>(
      this,
      () => this.paymentTypeTabGroup,
      viewModel.controls.paymentType, ["transfer", "email", "account-number", "payee"]);
  }

  get fromAccountsLoading() {
    return this.viewModel.fromAccounts == undefined;
  }

  get payeesLoading() {
    return this.viewModel.payees == undefined;
  }

  cancelClick() {
    this.viewModel.cancel();
  }

  continueClick() {
    this.viewModel.continue();
  }

  backClick() {
    this.viewModel.back();
  }

  submitClick() {
    this.viewModel.submit();
  }

  closeClick() {
    this.viewModel.close();
  }

  resetClick() {
    this.viewModel.reset();
  }
}
