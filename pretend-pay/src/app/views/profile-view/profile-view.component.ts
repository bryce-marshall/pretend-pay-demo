import { ChangeDetectorRef, Component } from "@angular/core";
import { FormControl } from "@angular/forms";
import { GeoAddress } from "src/app/lib/geo-address";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { AppErrorStateMatcher } from "src/app/material";
import { ProfileViewModel } from "src/app/view-models";
import { ProfileViewFormControls } from "src/app/view-models/profile-view-model";

@Component({
  selector: "app-profile-view",
  templateUrl: "./profile-view.component.html",
  styleUrls: ["./profile-view.component.scss"],
  providers: [ProfileViewModel]
})
export class ProfileViewComponent extends ReactiveComponentBase {
  private _step = 0;
  readonly controls: ProfileViewFormControls;
  matcher = new AppErrorStateMatcher();
  searchAddressFormControl = new FormControl("");
  dobStartDate: any;


  constructor(
    public viewModel: ProfileViewModel,
    private changeRef: ChangeDetectorRef
  ) {
    super();
    this.controls = viewModel.controls;
    this.dobStartDate = new Date(new Date(Date.now()).getFullYear() - 30, 1, 1);
  }

  onAddressChanged(address: GeoAddress) {
    if (address) {
      this.viewModel.applyGeoAddress(address);
      // In development (Win10/Chrome 71.0.3578.98) the affected form controls do not refresh properly without forcing change detection
      this.changeRef.detectChanges();
    }
  }

  cancelClick() {
    this.viewModel.cancel();
  }

  saveClick() {
    this.viewModel.save().then(() => {
      this.setStep(-1); // Collapse the accordion in the case of an error action info being displayed.
    });
  }

  setStep(index: number) {
    this._step = index;
  }

  nextStep() {
    this._step++;
  }

  prevStep() {
    this._step--;
  }

  isExpanded(index: number): boolean {
    return this._step === index;
  }
}
