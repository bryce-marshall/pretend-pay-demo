import { Component } from "@angular/core";
import { PayeeType } from "src/app/lib/types";
import { FormState } from "src/app/lib/view/form-state";
import { AppErrorStateMatcher } from "src/app/material";
import { PayeeEditViewModel } from "src/app/view-models";
import { PayeeEditViewFormControls } from "src/app/view-models/payee-edit-view-model";

@Component({
  selector: "app-payee-edit-view",
  templateUrl: "./payee-edit-view.component.html",
  styleUrls: ["./payee-edit-view.component.scss"],
  providers: [PayeeEditViewModel]
})
export class PayeeEditViewComponent {
  matcher = new AppErrorStateMatcher();
  state: FormState;
  formControls: PayeeEditViewFormControls;

  constructor(
    public viewModel: PayeeEditViewModel
  ) {
    this.state = viewModel.formState;
    this.formControls = viewModel.controls;
  }

  get payeeType(): PayeeType {
    return this.formControls.payeeType.value;
  }

  cancelClick() {
    this.viewModel.cancel();
  }

  saveClick() {
    this.viewModel.savePayee();
  }
}
