import { Component } from "@angular/core";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { AccountsViewModel } from "src/app/view-models";
import { FinancialAccount } from "src/app/lib/financial-account";

@Component({
  selector: "app-accounts-view",
  templateUrl: "./accounts-view.component.html",
  styleUrls: ["./accounts-view.component.scss"],
  providers: [AccountsViewModel]
})
export class AccountsViewComponent extends ReactiveComponentBase {
  constructor(
    public viewModel: AccountsViewModel
  ) {
    super();
  }

  accountClick(account: FinancialAccount) {
    this.viewModel.focusAccount(account.number);
  }
}
