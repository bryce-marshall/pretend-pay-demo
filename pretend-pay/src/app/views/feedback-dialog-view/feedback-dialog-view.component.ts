import { Component } from "@angular/core";
import { FormState } from "src/app/lib/view/form-state";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { AppErrorStateMatcher } from "src/app/material";
import { DialogRef } from "src/app/presenters/dialog/dialog-controller";
import { FeedbackDialogViewFormControls, FeedbackDialogViewModel } from "src/app/view-models/feedback-dialog-view-model";

@Component({
  selector: "app-feedback-dialog",
  templateUrl: "./feedback-dialog-view.component.html",
  styleUrls: ["./feedback-dialog-view.component.scss"],
  providers: [DialogRef, FeedbackDialogViewModel]
})
export class FeedbackDialogViewComponent extends ReactiveComponentBase {
  matcher = new AppErrorStateMatcher();

  constructor(
    public viewModel: FeedbackDialogViewModel
  ) {
    super();
  }

  get state(): FormState {
    return this.viewModel.formState;
  }

  get formControls(): FeedbackDialogViewFormControls {
    return this.viewModel.controls;
  }

  cancelClick() {
    this.viewModel.closeDialog();
  }

  continueClick() {
    this.viewModel.submitFeedback();
  }
}

