import { HttpClient } from "@angular/common/http";
import { MapFormatter } from "../lib/utilities/map-formatter";

export class ApiKeyLoader {

    constructor(private http: HttpClient) {
    }

    async load(): Promise<void> {
        const keys = await loadkeys(this.http);
        const nodes = extractNodes();
        applyKeys(nodes, keys);
        replaceNodes(nodes);

        return Promise.resolve();
    }
}

interface MappedNode {
    node: Node;
    urls: string[];
}

async function loadkeys(http: HttpClient): Promise<any> {
    const path = "assets/config/api-keys.json";
    try {
        return http.get(path).toPromise();
    } catch (e) {
        return Promise.reject(`Failed to load configuration file "${path}": ${JSON.stringify(e)}`);
    }
}

function extractNodes(): MappedNode[] {
    const result = [];
    const head = document.head;
    head.childNodes.forEach((node) => {
        if (node.nodeType !== 8) return; // comments only
        const text = node.textContent.trim();
        if (!text.startsWith("@api-scripts")) return;

        const urls: string[] = [];
        text.split(/\r?\n/).forEach((s) => {
            s = s.trim();
            // Ignore lines that are comments and whitespace
            if (!(s.length === 0 || s.startsWith("#") || s.startsWith("@"))) {
                urls.push(s);
            }
        });

        if (urls.length > 0) result.push({ node: node, urls: urls });
    });

    return result;
}

function applyKeys(nodes: MappedNode[], keys: any) {
    const f = MapFormatter.create(keys);

    let idx = 0;
    nodes.forEach((node) => {
        node.urls.forEach((url) => {
            node.urls[idx++] = f.format(url);
        });
    });
}

function replaceNodes(nodes: MappedNode[]) {
    nodes.forEach((n) => {
        n.node.parentNode.removeChild(n.node);
        n.urls.forEach((url) => {
            const el = document.createElement("script");
            el.type = "text/javascript"; // For non-HTML5 browsers
            el.src = url;
            document.head.appendChild(el);
        });
    });
}
