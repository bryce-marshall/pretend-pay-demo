import { HttpClient } from "@angular/common/http";

export abstract class ConfigBase<T> {
    constructor(private http: HttpClient, private jsonFilePath: string, private onLoaded: (result: T) => void) {
    }

    load(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.get(this.jsonFilePath).toPromise()
                .then((response) => {
                    this.onLoaded(<T>response);
                    delete this.http;
                    delete this.jsonFilePath;
                    delete this.onLoaded;
                    this.load = () => Promise.resolve();
                    resolve();
                })
                .catch((response: any) => {
                    reject(`Failed to load configuration file "${this.jsonFilePath}": ${JSON.stringify(response)}`);
                });
        });
    }
}
