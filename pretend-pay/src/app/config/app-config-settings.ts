
export interface AppConfigSettings {
    defaultLocale: string;
    locales: string[];
}
