import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigBase } from "./config-base";
import { environment } from "src/environments/environment";
import { AppConfigSettings } from "./app-config-settings";

@Injectable()
export class AppConfig extends ConfigBase<AppConfigSettings> {
    static settings: AppConfigSettings;

    constructor(http: HttpClient) {
        const path = `assets/config/config.${environment.production ? "production" : "dev"}.json`;

        super(http, path, (result: AppConfigSettings) => {
            AppConfig.settings = result;
        });
    }
}
