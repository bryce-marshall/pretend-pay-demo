/// <reference types="@types/googlemaps" />

import { GeoAddress } from "../lib/geo-address";

export class GooglePlaceAddressResolver {
    private _used: string[] = [];
    private _result: GeoAddress;

    private constructor(private placeResult: google.maps.places.PlaceResult) {
        const target: any = {};
        target.street = this.extractStreetAddress();
        target.locality = this.getAddressComponentShortName("administrative_area_level_2", "postal_town", "locality");
        target.suburb = this.getAddressComponentName("sublocality", "neighborhood", "locality");
        target.postcode = this.getAddressComponentName("postal_code");
        target.region = this.getAddressComponentName("administrative_area_level_1");
        target.country = this.getAddressComponentName("country");

        // if (!target.locality) target.locality = target.suburb;

        this._result = target;
    }

    static resolve(placeResult: google.maps.places.PlaceResult): GeoAddress {
        return new GooglePlaceAddressResolver(placeResult)._result;
    }

    /**
     * Returns the name of the specified address component if it exists on a PlaceResult, or otherwise an empty string.
     * @param placeResult A google maps place.
     * @param types The list of candidate address types, in order of precedence.
     */
    private getAddressComponentName(...types: string[]): string {
        const comp = this.getAddressComponent(...types);

        return comp ? comp.long_name : "";
    }

    private getAddressComponentShortName(...types: string[]): string {
        const comp = this.getAddressComponent(...types);

        return comp ? comp.short_name : "";
    }

    /**
     * Returns the first address component in a PlaceResult containing one of the specified address types.
     * @param placeResult A google maps place.
     * @param types The list of candidate address types, in order of precedence.
     */
    private getAddressComponent(...types: string[]): google.maps.GeocoderAddressComponent {
        // Retrieve the first address component with any of the specified types
        return this.placeResult.address_components.find((c) => {
            const type = types.find((t) => {
                return c.types.includes(t);
            });

            if (type == undefined || this._used.includes(type)) return false;
            this._used.push(type);
            return true;
        });
    }

    private extractStreetAddress(): string {
        if (typeof this.placeResult.adr_address !== "string") return "";

        const matches = new RegExp(/<span class="[^"]*?street-address[^"]*?">(.*?)<\/span>/g).exec(this.placeResult.adr_address);
        if (matches.length < 2) return "";

        return matches[1];
    }
}
