export class AppConstants {
    static AppName = "Pretend-Pay";
    static TransactionPageSize = 25;
    /**
     * The period after which the user-interface will timeout when awaiting state updates from the remote server.
     */
    static LoadingTimeout = 15000;
}
