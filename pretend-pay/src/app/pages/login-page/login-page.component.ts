import { Component } from "@angular/core";
import { ReactiveComponentBase } from "src/app/lib/view/reactive-component-base";
import { AppErrorStateMatcher } from "src/app/material";
import { LoginPageViewModel } from "src/app/view-models";
import { LoginPageFormControls as LoginViewFormControls } from "src/app/view-models/login-page-view-model";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.scss"],
  providers: [LoginPageViewModel]
})
export class LoginPageComponent extends ReactiveComponentBase {
  readonly controls: LoginViewFormControls;
  matcher = new AppErrorStateMatcher();

  constructor(
    public viewModel: LoginPageViewModel
  ) {
    super();
    this.controls = viewModel.controls;
  }

  loginClick() {
    this.viewModel.login();
  }
}
