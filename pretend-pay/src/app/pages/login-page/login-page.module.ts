import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LoginPageComponent } from "./login-page.component";
import { AppCommonModule } from "src/app/app-common.module";
import { AppFormsModule } from "src/app/app-forms.module";

@NgModule({
  imports: [
    AppCommonModule,
    AppFormsModule,
    RouterModule.forChild([{ path: "", component: LoginPageComponent }])
  ],
  declarations: [
    LoginPageComponent,
  ],
  entryComponents: [LoginPageComponent]
})
export class LoginPageModule { }
