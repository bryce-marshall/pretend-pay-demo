import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomePageComponent } from "./home-page.component";
import { AccountsViewComponent } from "src/app/views/accounts-view/accounts-view.component";
import { PaymentsViewComponent } from "src/app/views/payments-view/payments-view.component";
import { PayeesViewComponent } from "src/app/views/payees-view/payees-view.component";
import { AccountTransactionsViewComponent } from "src/app/views/account-transactions-view/account-transactions-view.component";
import { ProfileViewComponent } from "src/app/views/profile-view/profile-view.component";
import { PayeeEditViewComponent } from "src/app/views/payee-edit-view/payee-edit-view.component";

const routes: Routes = [
  {
    path: "", component: HomePageComponent,
    children:
      [
        { path: "accounts", component: AccountsViewComponent, data: { viewContext: "accounts" } },
        { path: "transactions/:accountNumber", component: AccountTransactionsViewComponent, data: { viewContext: "accounts" } },
        { path: "pay-transfer", component: PaymentsViewComponent, data: { viewContext: "pay-transfer" } },
        { path: "payees", component: PayeesViewComponent, data: { viewContext: "payees" } },
        { path: "create-payee", component: PayeeEditViewComponent, data: { viewContext: "payees" } },
        { path: "edit-payee/:payeeId", component: PayeeEditViewComponent, data: { viewContext: "payees" } },
        { path: "profile", component: ProfileViewComponent, data: { viewContext: "profile" } }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule { }
