import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { Router, RouterEvent, RoutesRecognized } from "@angular/router";
import { ViewportType } from "src/app/lib/types";
import { MaterialSupportDirector } from "src/app/material";
import { MaterialPage } from "src/app/material/material-page";
import { StoreStateObserver } from "src/app/store/extensions";
import { HomePageViewModel } from "src/app/view-models/home-page-view-model";
import { FeedbackDialogViewComponent } from "src/app/views/feedback-dialog-view/feedback-dialog-view.component";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
  providers: [HomePageViewModel, MaterialSupportDirector]
})
export class HomePageComponent extends MaterialPage implements OnInit {
  @ViewChild("scrollTarget", { static: true }) scrollTarget: ElementRef;
  @ViewChild(MatSidenav, { static: true }) sideNav: MatSidenav;
  viewContext: string;
  isPhone: boolean;

  constructor(
    public viewModel: HomePageViewModel,
    material: MaterialSupportDirector,
    private router: Router
  ) {
    super(material);
    this.isPhone = false;
  }

  ngOnInit() {
    super.ngOnInit();
    this.applyViewport(this.viewModel.viewportType);
    this.subscribeTo(this.viewModel.viewportTypeChanged, (value: ViewportType) => {
      this.applyViewport(value);
    });

    this.subscribeTo(this.router.events, (e: RouterEvent) => {
      if (e instanceof RoutesRecognized) {
        // Ensure that content container has its scroll position reset when a new embedded component (sub-page)
        // is injected into the router-routlet.
        if (this.scrollTarget.nativeElement.scrollIntoView) this.scrollTarget.nativeElement.scrollIntoView();
      }
    });
  }

  toggleSidenavClick() {
    this.sideNav.opened = !this.sideNav.opened;
  }

  accountsClick() {
    this.viewModel.navigate.accounts();
    this.afterMenuClick();
  }

  paymentClick() {
    this.viewModel.navigate.payment();
    this.afterMenuClick();
  }

  payeesClick() {
    this.viewModel.navigate.payees();
    this.afterMenuClick();
  }

  profileClick() {
    this.viewModel.navigate.profile();
    this.afterMenuClick();
  }

  feedbackClick() {
    this.viewModel.navigate.openDialog(FeedbackDialogViewComponent);
    this.afterMenuClick();
  }

  logoutClick() {
    this.viewModel.logout();
    this.afterMenuClick();
  }

  @StoreStateObserver()
  protected onViewContextChanged() {
    this.viewContext = this.viewModel.viewContext;
  }

  private applyViewport(value: ViewportType) {
    this.isPhone = value === "phone";
    this.sideNav.opened = !this.isPhone;
    this.sideNav.mode = this.isPhone ? "over" : "side";
  }

  private afterMenuClick() {
    if (this.isPhone) this.sideNav.opened = false;
  }
}
