import { NgModule } from "@angular/core";
import { HomePageComponent } from "./home-page.component";
import { AppCommonModule } from "src/app/app-common.module";
import { AppFormsModule } from "src/app/app-forms.module";
import { AccountsViewComponent } from "src/app/views/accounts-view/accounts-view.component";
import { PaymentsViewComponent } from "src/app/views/payments-view/payments-view.component";
import { PayeesViewComponent } from "src/app/views/payees-view/payees-view.component";
import { HomePageRoutingModule } from "./home-page-routing.module";
import { AccountTransactionsViewComponent } from "src/app/views/account-transactions-view/account-transactions-view.component";
import { ProfileViewComponent } from "src/app/views/profile-view/profile-view.component";
import { PayeeEditViewComponent } from "src/app/views/payee-edit-view/payee-edit-view.component";
import { ActionInfoListComponent } from "src/app/components/action-info-list/action-info-list.component";

const components = [
  HomePageComponent,
  ActionInfoListComponent,
  AccountsViewComponent,
  AccountTransactionsViewComponent,
  PaymentsViewComponent,
  PayeesViewComponent,
  PayeeEditViewComponent,
  ProfileViewComponent,

  // For AOT Production
  // ... commonComponents
];

@NgModule({
  imports: [
    AppCommonModule,
    // ...AppCommonModule.globalImports(),
    AppFormsModule,
    HomePageRoutingModule
  ],
  declarations: components,
  entryComponents: [...components]
})
export class HomePageModule { }
