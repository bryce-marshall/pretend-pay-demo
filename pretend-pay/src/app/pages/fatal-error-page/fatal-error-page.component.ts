import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-fatal-error-page",
  templateUrl: "./fatal-error-page.component.html",
  styleUrls: ["./fatal-error-page.component.scss"]
})
export class FatalErrorPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
