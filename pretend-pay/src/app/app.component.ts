import { HttpClient } from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { AppConstants } from "./app-constants";
import { ApiKeyLoader } from "./config/api-key-loader";
import { AppConfig } from "./config/app-config";
import { Facade } from "./facade";
import { AccountsApi, AuthApi } from "./lib/api";
import { LanguageDirection } from "./lib/types";
import { AccountRefreshTask, AppTaskManager, NotificationPollerTask } from "./services/app-task-manager";
import { ApplicationStateDirector } from "./services/directors/application-state-director";
import { ViewDirector } from "./services/directors/view-director";
import { InternationalizationService } from "./services/i18n/internationalization.service";
import { AppLogger } from "./services/logger/app-logger";
import { ServiceLifecycleManager } from "./services/service-lifecycle-manager";
import { AppEnvironmentStore, AppViewStore } from "./store";
import { initStore } from "./store/init-store";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  providers: [
    NotificationPollerTask,
    AccountRefreshTask
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  private _serviceManager: ServiceLifecycleManager;
  protected reloading = false;

  constructor(
    http: HttpClient,
    private logger: AppLogger,
    stateDirector: ApplicationStateDirector,
    viewDirector: ViewDirector,
    i18n: InternationalizationService,
    authApi: AuthApi,
    accountsApi: AccountsApi,
    private environment: AppEnvironmentStore,
    private view: AppViewStore,
    private facade: Facade,
    taskManager: AppTaskManager,
    task1: NotificationPollerTask,
    task2: AccountRefreshTask
  ) {
    this.facade.navigate.init();
    this.logger = logger.contextFor("AppComponent");
    this.logger.trace(`Starting the ${AppConstants.AppName} application`);
    initStore();
    taskManager.addTask(task1, task2);

    this._serviceManager = new ServiceLifecycleManager(
      [logger],
      [ServiceLifecycleManager.autoProxy("ApiKeyLoader", new ApiKeyLoader(http), (s) => s.load())],
      [ServiceLifecycleManager.autoProxy("ConfigLoader", new AppConfig(http), (s) => s.load())],
      [
        authApi,
        accountsApi,
      ],
      [i18n],
      [stateDirector],
      [viewDirector],
      [taskManager]
    );

    handleServiceManagerLogging(this._serviceManager, logger);
  }

  ngOnInit() {
    this.logger.trace("The ServiceLifecycleManager is preparing to initialize the application services.");
    this._serviceManager.onServiceInit()
      .then(() => {
        this.logger.trace(`The ${AppConstants.AppName} application was started successfully`);
        this.setInitialized();
      }).catch((e) => {
        this.logger.errorDetail(e, `Failed to start the {AppConstants.AppName} application`);
        this.handleFatalError();
      });
  }

  ngOnDestroy() {
    this.logger.trace("The ServiceLifecycleManager is preparing to finalize the application services.");
    this._serviceManager.onServiceFinalize();
  }

  get dir(): LanguageDirection {
    return this.environment.dir;
  }

  get isRtl(): boolean {
    return this.environment.dir === "rtl";
  }

  get isPhone(): boolean {
    return this.view.viewportType === "phone";
  }

  private setInitialized() {
    this.environment.setState("initialized");
    this.environment.setLocale(AppConfig.settings.defaultLocale);
  }

  handleFatalError() {
    this.facade.navigate.fatalError();
  }
}

function handleServiceManagerLogging(serviceManager: ServiceLifecycleManager, logger: AppLogger) {
  logger = logger.contextFor("ServiceLifecycleManager");
  serviceManager.serviceEvent.subscribe(args => {
    if (args.isError) {
      logger.errorDetail(args.error, args.message);
    } else {
      logger.trace(args.message);
    }
  });
}
