import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FatalErrorPageComponent } from "./pages/fatal-error-page/fatal-error-page.component";
import { InitPageComponent } from "./pages/init-page/init-page.component";

const routes: Routes = [
  { path: "", redirectTo: "/init", pathMatch: "full" },
  { path: "home", loadChildren: () => import('./pages/home-page/home-page.module').then(m => m.HomePageModule), data: { viewcontext: "other" } },
  { path: "init", component: InitPageComponent },
  { path: "login", loadChildren: () => import('./pages/login-page/login-page.module').then(m => m.LoginPageModule) },
  { path: "fatal-error", component: FatalErrorPageComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
