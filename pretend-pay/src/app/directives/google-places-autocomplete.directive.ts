/// <reference types="@types/googlemaps" />
import { Directive, ElementRef, OnInit, Output, EventEmitter } from "@angular/core";
import { GeoAddress } from "../lib/geo-address";
import { GooglePlaceAddressResolver } from "../google/google-place-address-resolver";

// https://developers.google.com/maps/documentation/javascript/places-autocomplete

@Directive({
  selector: "[google-places-autocomplete]",
  exportAs: "google-places-autocomplete"
})
export class GooglePlacesAutocompleteDirective implements OnInit {
  private _element: HTMLInputElement;
  private _autocomplete: google.maps.places.Autocomplete;

  @Output() placeChanged = new EventEmitter<google.maps.places.PlaceResult>();
  @Output() addressChanged = new EventEmitter<GeoAddress>();

  constructor(elRef: ElementRef) {
    this._element = elRef.nativeElement;
  }

  ngOnInit() {
    const options = {
      types: ["address"],
      strictBounds: true, // With no location bias, the service should automatically bias based upon the user"s IP address,
      // componentRestrictions: { country: "nz" }
    };
    this._autocomplete = new google.maps.places.Autocomplete(this._element, options);

    this._autocomplete.addListener("place_changed", () => {
      let place: google.maps.places.PlaceResult;
      if (this.addressChanged.observers.length > 0) {
        place = this._autocomplete.getPlace();
        this.addressChanged.emit(GooglePlaceAddressResolver.resolve(place));
      }

      if (this.placeChanged.observers.length > 0) {
        place = place || this._autocomplete.getPlace();
        this.placeChanged.emit(this._autocomplete.getPlace());
      }
    });
  }
}
