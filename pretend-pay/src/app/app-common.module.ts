import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { AccountHeaderComponent } from "./components/account-header/account-header.component";
import { BusyStateWrapperComponent } from "./components/busystate-wrapper/busystate-wrapper.component";
import { InlineBusyIndicatorComponent } from "./components/inline-busy-indicator/inline-busy-indicator.component";
import { InlineSyncIndicatorComponent } from "./components/inline-sync-indicator/inline-sync-indicator.component";
import { LanguageSelectorComponent } from "./components/language-selector/language-selector.component";
import { NotificationIndicatorComponent } from "./components/notification-indicator/notification-indicator.component";
import { TopMenuComponent } from "./components/top-menu/top-menu.component";
import { GooglePlacesAutocompleteDirective } from "./directives/google-places-autocomplete.directive";
import { MaterialModule } from "./material.module";
import { MaterialSupportModule } from "./material/material-support.module";
import { AccountAvailablePipe } from "./pipes/account-available-pipe";
import { AccountNamePipe } from "./pipes/account-name-pipe";
import { ActionOutcomePipe } from "./pipes/action-outcome-pipe";
import { ActionTitlePipe } from "./pipes/action-title-pipe";
import { ApiErrorMessagePipe } from "./pipes/api-error-message-pipe";
import { AppletTitlePipe } from "./pipes/applet-title-pipe";
import { FormatCurrencyPipe } from "./pipes/format-currency-pipe";
import { InvalidMessagePipe } from "./pipes/invalid-msg-pipe";
import { QuantityDescriptorPipe } from "./pipes/quantity-descriptor-pipe";
import { RequiredMessagePipe } from "./pipes/required-msg-pipe";
import { ShortDatePipe } from "./pipes/short-date-pipe";
import { TransactionDataPipe } from "./pipes/transaction-data-pipe";
import { TranslatablePipe } from "./pipes/translatable-pipe";
import { PresentersModule } from "./presenters/presenters.module";
import { ConfirmModalViewComponent } from "./views/confirm-modal-view/confirm-modal-view.component";
import { ErrorModalViewComponent } from "./views/error-modal-view/error-modal-view.component";
import { FeedbackDialogViewComponent } from "./views/feedback-dialog-view/feedback-dialog-view.component";

const _modules: any[] = [
    CommonModule,
    PresentersModule,
    TranslateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialSupportModule,
];

const _entryComponents: any[] = [
    FeedbackDialogViewComponent,
    ConfirmModalViewComponent,
    ErrorModalViewComponent
];

const _components: any[] = [
    // Components
    InvalidMessagePipe,
    RequiredMessagePipe,
    TranslatablePipe,
    FormatCurrencyPipe,
    AccountNamePipe,
    AccountAvailablePipe,
    QuantityDescriptorPipe,
    ShortDatePipe,
    ApiErrorMessagePipe,
    AppletTitlePipe,
    ActionOutcomePipe,
    ActionTitlePipe,
    TransactionDataPipe,
    LanguageSelectorComponent,
    TopMenuComponent,
    NotificationIndicatorComponent,
    BusyStateWrapperComponent,
    InlineBusyIndicatorComponent,
    InlineSyncIndicatorComponent,
    AccountHeaderComponent,
    GooglePlacesAutocompleteDirective,

    // Entry Components
    FeedbackDialogViewComponent,
    ConfirmModalViewComponent,
    ErrorModalViewComponent,
];

const _exports: any[] = [
    // Modules
    CommonModule,
    PresentersModule,
    TranslateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialSupportModule,
    // Components
    InvalidMessagePipe,
    RequiredMessagePipe,
    TranslatablePipe,
    FormatCurrencyPipe,
    AccountNamePipe,
    AccountAvailablePipe,
    QuantityDescriptorPipe,
    ShortDatePipe,
    ApiErrorMessagePipe,
    AppletTitlePipe,
    ActionOutcomePipe,
    ActionTitlePipe,
    TransactionDataPipe,
    LanguageSelectorComponent,
    TopMenuComponent,
    NotificationIndicatorComponent,
    BusyStateWrapperComponent,
    InlineBusyIndicatorComponent,
    InlineSyncIndicatorComponent,
    AccountHeaderComponent,
    GooglePlacesAutocompleteDirective,
];

@NgModule({
    declarations: _components,
    imports: _modules,
    exports: _exports,
    entryComponents: _entryComponents,
})
export class AppCommonModule {
    static forRoot(): ModuleWithProviders<AppCommonModule> {
        return {
            ngModule: AppCommonModule
        };
    }
}
