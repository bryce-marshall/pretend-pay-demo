export type ApiResponseCode = string;

/**
 * The set of API response codes.
 */
export enum KnownApiResponseCode {
    Ok = "ok",
    Timeout = "timeout",
    ApplicationError = "application-error",
    InvalidOperation = "invalid-operation",
    ConcurrencyError = "concurrency-error",
    InvalidUserCredentials = "invalid-credentials",
    AccessDenied = "access-denied",
    InvalidAccount = "account-invalid",
    CurrencyMismatch = "currency-mismatch",
    InsufficientFunds = "insufficient-funds",
    InvalidTransaction = "invalid-transaction",
    PayeeExists = "payee-exists",
    PayeeNotExists = "payee-not-exists",
}

export interface ApiResponseData<T> {
    readonly code: ApiResponseCode;
    readonly result: T;
}

/**
 * The structure returned by API methods.
 */
export class ApiResponse<T> implements ApiResponseData<T> {
    readonly code: ApiResponseCode;
    readonly result: T;

    private constructor(response: string | ApiResponseData<T>, result?: T) {
        if (typeof (response) === "string") {
            this.code = response;
            this.result = result;
        } else if (response != undefined) {
            this.code = response.code;
            this.result = response.result;
        }
    }

    get isOk(): boolean {
        return this.code === KnownApiResponseCode.Ok;
    }

    /**
     * Converts a candidate object to an ApiResponse.
     * @param o The object to convert.
     */
    static as<T>(o: ApiResponse<T> | any): ApiResponse<T> {
        if (!o) return ApiResponse.error(KnownApiResponseCode.ApplicationError);
        if (typeof (o) === "string" || o.hasOwnProperty("code") && o.hasOwnProperty("result")) return new ApiResponse(o);
        return ApiResponse.error(KnownApiResponseCode.ApplicationError);
    }

    static resolveErrorCode(apiResponse: any): ApiResponseCode {
        if (!apiResponse || typeof apiResponse.code !== "string") return KnownApiResponseCode.ApplicationError;

        return apiResponse.code;
    }

    static ok<T>(result?: T): ApiResponse<T> {
        return new ApiResponse(KnownApiResponseCode.Ok, result);
    }

    static resolve<T>(result?: T): Promise<ApiResponse<T>> {
        return Promise.resolve(new ApiResponse(KnownApiResponseCode.Ok, result));
    }

    static error(code: KnownApiResponseCode | string): ApiResponse<any> {
        return new ApiResponse(code);
    }

    static reject(response: KnownApiResponseCode | string | ApiResponse<any> | any): Promise<ApiResponse<any>> {
        return Promise.reject(ApiResponse.as(response));
    }

    static rejectVoid(response: KnownApiResponseCode | string | ApiResponse<any> | any): Promise<void> {
        return Promise.reject(ApiResponse.as(response));
    }
}
