import { LoadingState } from "./types";

export interface LoadingStateProvider {
    loadingState: LoadingState;
}
