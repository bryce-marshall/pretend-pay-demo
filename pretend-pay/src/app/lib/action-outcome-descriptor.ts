export interface ActionOutcomeDescriptor {
    title?: string;
    outcome: string;
}
