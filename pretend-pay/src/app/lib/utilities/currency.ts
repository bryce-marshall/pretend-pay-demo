import { argumentMessage, argumentNullMessage } from "./error-message";

/**
 * A helper class for currencies.\
 * 
 * NOTE: In a production application these features would be built from services  
 */
export class Currency {
    /**
     * Returns the result of the addition of two currency values, truncated to the correct number of decimal places for the currency.
     * @param currencyCode The currency code of the currency in which the values are expressed.
     * @param x The first value to add.
     * @param y The econd value to add.
     */
    static add(currencyCode: string, x: number, y: number) {
        if (x == undefined || x === NaN) x = 0;
        if (y == undefined || y === NaN) y = 0;

        return Currency.trunc(currencyCode, x + y);
    }
    /**
     * Truncates a numeric value (without rounding) the number of decimal places of a floating point number so that it does not exceed
     * the exponent of the specified currency.
     * @param currencyCode The currency code from which to derive the exponent.
     * @param value The value to be truncated.
     */
    static trunc(currencyCode: string, value: number): number {
        const s = value.toString().replace(",", ".");
        const idx = s.indexOf(".");
        if (idx < 0) return value;
        return parseFloat(s.substr(0, Math.min(s.length, 1 + idx + Currency.exponent(currencyCode))));
    }

    /**
     * Returns the exponent (number of decimal places) of the specified currency. If the currency is unknown,
     * the default exponent 2 is returned.
     * 
     * @param currencyCode The currency code.
     */
    static exponent(currencyCode: string): number {
        if (currencyCode == undefined) throw new Error(argumentNullMessage("currencyCode"));
        if (typeof currencyCode !== "string") throw new Error(argumentMessage("currencyCode"));

        switch (currencyCode.toUpperCase()) {
            case "BIF":
            case "BYR":
            case "CLF":
            case "CLP":
            case "CVE":
            case "DJF":
            case "GNF":
            case "IDR":
            case "IQD":
            case "IRR":
            case "ISK":
            case "JPY":
            case "KMF":
            case "KPW":
            case "KRW":
            case "LAK":
            case "LBR":
            case "MMK":
            case "PYG":
            case "RWF":
            case "SLL":
            case "STD":
            case "UYI":
            case "VND":
            case "VUV":
            case "XPF":
                return 0;

            case "MOP":
                return 1;

            case "BHD":
            case "JOD":
            case "KWD":
            case "LYD":
            case "OMR":
            case "TND":
                return 3;

            default:
                return 2;
        }
    }
}
