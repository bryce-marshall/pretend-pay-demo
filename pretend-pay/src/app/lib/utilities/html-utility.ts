export class HtmlUtility {
    static stripHtml(value: string): string {
        if (value == undefined || value === "") return "";
        const div = document.createElement("div");
        div.innerHTML = value;
        return div.textContent || div.innerText || "";
    }
}
