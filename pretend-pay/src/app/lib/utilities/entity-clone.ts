
export type PropertyFilter = (obj: any, propertyName: string) => boolean;

export function cloneEntity<T>(src: any, propertyFilter?: PropertyFilter) {
    // If src is undefined or one of the immmutable value-types, return the input value
    if (src == undefined || typeof src !== "object") return src;
    if (src instanceof Date) return new Date(src.getTime());
    if (src instanceof Array) return cloneArray(src, propertyFilter);

    const result = {};
    for (const prop in src) {
        if (src.hasOwnProperty(prop) && (propertyFilter == undefined || propertyFilter(src, prop))) {
            result[prop] = cloneEntity(src[prop], propertyFilter);
        }
    }

    return result;
}

function cloneArray(src: any[], propertyFilter: PropertyFilter): any[] {
    const result = [];
    src.forEach((el) => {
        result.push(cloneEntity(el, propertyFilter));
    });

    return result;
}
