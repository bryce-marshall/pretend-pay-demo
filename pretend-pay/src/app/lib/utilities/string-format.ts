const stringFormatRegex = /\{([0-9a-zA-Z]+)\}/g;

/**
   * Replaces tokens in the specified format string with the specified arguments.
   *
   * Usage:
   *
   * `FormatHelper.format("Index-0 = {0}, Index-1 = {1}", ["One", "Two"])`
   *
   * or
   *
   * `FormatHelper.format("Lookup-Name = {name}, Lookup-Value = {value}", { name: 'One', value: '1' })`;
   * @param format The format string
   * @param args The format arguments
   */
export function stringFormat(format: string, ...args: any[]): string {
    let data: any;

    if (args.length === 1 && typeof args[0] === "object") {
        data = args[0];
    } else {
        data = args;
    }

    return format.replace(
        stringFormatRegex,
        (match: string, token: string, index: number) => {
            let result: string;

            if (format[index - 1] === "{" && format[index + match.length] === "}") {
                result = token;
            } else {
                result = data[token];
            }

            return result != undefined ? result : "";
        }
    );
}
