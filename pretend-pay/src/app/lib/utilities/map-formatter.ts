import { LinkedList } from "../collections/linked-list";

/**
 * The interval, in milliseconds, between cache flush operations.
 */
const FLUSH_INTERVAL = 1000;
const EXPIRE_INTERVAL = 500;

class MapFormatCache {
    private items = new LinkedList<CachedFormatter>();
    private flushQueued = false;

    format(fmt: string, map: any): string {
        let f = this.items.find((item) => {
            return item.map === map;
        });

        // if (f) console.log("*** DEBUG MatFormatter Cache Hit!!!");
        if (!f) f = this.items.append(cachedFormatter(map));
        f.item.accessed = Date.now();
        // console.log(`*** DEBUG MatFormatter Cache item count = ${this.items.count}`);
        if (!this.flushQueued) {
            this.flushQueued = true;
            setTimeout(() => {
                this.flush();
            }, FLUSH_INTERVAL);
        }
        const r = f.item.format(fmt);
        // The current formatter is already at the top of the list.
        if (!f.prev) return r;
        // The most recently used map is most likely to be used again, so reposition it at the top of the list.
        this.items.moveFirst(f);

        return r;
    }

    private flush() {
        // console.log("*** DEBUG MatFormatter Cache Flush");
        if (this.items.count === 0) return;
        let f = this.items.first;
        const now = Date.now();

        while (f) {
            const next = f.next;
            if (now - f.item.accessed >= EXPIRE_INTERVAL) {
                // Remove the item from the cache
                this.items.remove(f);
                // console.log("*** DEBUG flushed MapFormatter ", f);
            }

            f = next;
        }

        // console.log(`*** DEBUG cache count after flush = ${this.items.count}`);
        this.flushQueued = this.items.count > 0;
        if (this.flushQueued) {
            setTimeout(() => {
                this.flush();
            }, FLUSH_INTERVAL);
        }
    }
}

function mapKeys(src: any): any {
    const target = {};
    const flatten = (path: string, node: any) => {
        for (let key in node) {
            if (key == undefined) continue;
            const v = node[key];
            if (v == undefined) continue;
            key = path + "." + key;
            if (typeof (v) === "string") target["{" + key + "}"] = v; else flatten(key, v);
        }
    };

    for (const key in src) {
        if (key == undefined) continue;
        flatten(key, src[key]);
    }

    return target;
}

function formatString(s, keys: any): string {
    // Strip whitespace from inside brackets
    s = s.replace(/\s+(?=[^{}]*})/g, "");
    const matches = s.match(/\{([^}]+)\}/g);
    if (!matches) return s;
    matches.forEach((m) => {
        const v = keys[m];
        s = s.replace(m, v != undefined ? v : "");
    });

    return s;
}

const cache = new MapFormatCache();

export interface MappedFormatter {
    format(fmt: string): string;
}

export class MapFormatter {
    static create(map: any): MappedFormatter {
        return mapFormatter(map);
    }

    static format(fmt: string, map: any): string {
        // console.log("*** DEBUG - MapFormatter.format");
        return cache.format(fmt, map);
    }

    static formatOnce(fmt: string, map: any): string {
        return mapFormatter(map).format(fmt);
    }
}

function mapFormatter(map: any): MappedFormatter {
    map = map != undefined ? mapKeys(map) : {};
    return {
        format: (fmt: string) => {
            return formatString(fmt, map);
        }
    };
}

function cachedFormatter(map: any): CachedFormatter {
    const r = <CachedFormatter>mapFormatter(map);
    r.map = map;
    r.accessed = undefined;

    return r;
}

interface CachedFormatter extends MappedFormatter {
    map: any;
    accessed: number;
}
