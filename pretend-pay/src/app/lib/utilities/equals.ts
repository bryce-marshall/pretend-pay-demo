import { TransactionDirection } from "../transaction-query-args";

export class Equals {
    static date(x: Date, y: Date): boolean {
        if (x == undefined) return y == undefined;
        if (y == undefined) return false;
        return x.getTime() === y.getTime();
    }

    static transactionDirection(x: TransactionDirection, y: TransactionDirection): boolean {
        if (x == undefined || <any>x === "" || x === "both") return y == undefined || <any>y === "" || y === "both";
        if (y == undefined || <any>y === "" || y === "both") return false;
        return x === y;
    }
}
