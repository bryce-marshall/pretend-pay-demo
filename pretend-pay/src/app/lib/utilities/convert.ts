export class Convert {
    static asDate(value: any) {
        if (value == undefined || <any>value === "") return undefined;
        if (typeof value.toDate === "function") return value.toDate(); // This is a Moment instance
        return value;
    }
}
