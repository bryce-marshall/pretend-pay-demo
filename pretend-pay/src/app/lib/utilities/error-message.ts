export function argumentMessage(argumentName: string, message?: string): string {
    let r = `The argument ${argumentName}" is invalid.`;
    if (message) {
        r += (" " + message);
    }
    return r;
}

export function argumentNullMessage(argumentName?: string): string {
    return argumentName ? `The argument ${argumentName}" cannot be null.` : "Argument null.";
}

export function invalidOperationMessage(message?: string): string {
    return message ? message : "Operation is not valid due to the current state of the object.";
}
