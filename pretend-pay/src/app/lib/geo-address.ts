
export interface GeoAddress {
    street: string;
    suburb: string;
    locality: string;
    postcode: string;
    region: string;
    country: string;
}
