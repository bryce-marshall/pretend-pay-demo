import { ExclusiveControlDirector, GroupSelectorAccessor, ExclusiveControlGroup } from "./exclusive-control-director";
import { Subject, Subscription, Observable } from "rxjs";
import { LifecycleEventSource } from "./lifecycle-event-source";

/**
 * Binds to a selector to automatically coordinate the enabled state of conditional form controls contained within exclusive
 * groups so that controls contained within inactive groups do not raise validation errors.
 */
export abstract class AutoExclusiveControlDirector<TValue = string> extends ExclusiveControlDirector<TValue> {
    private _context: any;
    private _assignedGroup: TValue;
    private _subscription: Subscription;
    private _selectedChanged: Subject<TValue>;
    readonly selectedChanged: Observable<TValue>;

    constructor(
        host: LifecycleEventSource,
        private accessor: GroupSelectorAccessor<TValue>,
        initialGroup: TValue,
        sequence: ExclusiveControlGroup<TValue>[]
    ) {
        super(initialGroup, sequence);
        this._selectedChanged = new Subject<TValue>();
        this.selectedChanged = this._selectedChanged.asObservable();

        host.doCheckEvent.subscribe(() => {
            if (!this.checkContext()) return;
            if (this._assignedGroup === this.selected) return;
            this.refresh();
        });
    }

    /**
     * Refreshes the enabled state of the managed form controls and updates the selected value of the selector.
     */
    refresh() {
        this.refreshControlState((idx, value) => {
            this.accessor.setSelected(idx, value);
        });

        this._assignedGroup = this.selected;
    }

    private checkContext(): boolean {
        const context = this.accessor.getControlContext();
        if (context && this._context === context) return true;
        if (this._subscription) this._subscription.unsubscribe();
        this._context = context;
        this._assignedGroup = undefined;
        if (context == undefined) return false;

        this._subscription = this.accessor.subscribeToChangeEvent(() => {
            const selected = this.accessor.resolveSelected(this.sequence);
            this.selected = selected ? selected.type : undefined;
            this._selectedChanged.next(this.selected);
        });

        return true;
    }
}
