import { OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy, Directive } from "@angular/core";
import { HostedObserverSet } from "src/app/store/extensions/hosted-observer-set";
import { Observable, Subject } from "rxjs";
import { LifecycleEventSource } from "src/app/lib/view";
import { SubscriptionManager } from "../subscription-manager";

/**
 * The base class for Angular components that directly observe application stores.
 */
@Directive()
export class ReactiveComponentBase implements
    OnChanges,
    OnInit, DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy,
    LifecycleEventSource {

    private _subscriptions = new SubscriptionManager();
    private _preInit = true;
    private _observerHosts: any[];
    private _observers: HostedObserverSet;
    private _changesEvent: Subject<any> = new Subject();
    private _initEvent: Subject<any> = new Subject();
    private _doCheckEvent: Subject<any> = new Subject();
    private _afterContentInitEvent: Subject<any> = new Subject();
    private _afterContentCheckedEvent: Subject<any> = new Subject();
    private _afterViewInitEvent: Subject<any> = new Subject();
    private _afterViewCheckedEvent: Subject<any> = new Subject();
    private _destroyEvent: Subject<any> = new Subject();

    readonly changesEvent: Observable<any>;
    readonly initEvent: Observable<any>;
    readonly doCheckEvent: Observable<any>;
    readonly afterContentInitEvent: Observable<any>;
    readonly afterContentCheckedEvent: Observable<any>;
    readonly afterViewInitEvent: Observable<any>;
    readonly afterViewCheckedEvent: Observable<any>;
    readonly destroyEvent: Observable<any>;

    constructor() {
        this._observerHosts = [this];
        this.changesEvent = this._changesEvent.asObservable();
        this.initEvent = this._initEvent.asObservable();
        this.doCheckEvent = this._doCheckEvent.asObservable();
        this.afterContentInitEvent = this._afterContentInitEvent.asObservable();
        this.afterContentCheckedEvent = this._afterContentCheckedEvent.asObservable();
        this.afterViewInitEvent = this._afterViewInitEvent.asObservable();
        this.afterViewCheckedEvent = this._afterViewCheckedEvent.asObservable();
        this.destroyEvent = this._destroyEvent.asObservable();
    }

    ngOnChanges() {
        this._changesEvent.next(this);
    }

    ngOnInit(): void {
        const hosts = this._observerHosts;
        delete this._preInit;
        delete this._observerHosts;
        this._observers = new HostedObserverSet(hosts);
        this._initEvent.next(this);
    }

    ngDoCheck() {
        this._doCheckEvent.next(this);
    }

    ngAfterContentInit() {
        this._afterContentInitEvent.next(this);
    }

    ngAfterContentChecked() {
        this._afterContentCheckedEvent.next(this);
    }

    ngAfterViewInit(): void {
        this._afterViewInitEvent.next(this);
    }

    ngAfterViewChecked(): void {
        this._afterViewCheckedEvent.next(this);
    }

    ngOnDestroy(): void {
        HostedObserverSet.dispose(this._observers);
        this._subscriptions.unsubscribe();
        this._destroyEvent.next(this);
    }

    protected addStoreStateObservers(...hosts: any[]) {
        if (!this._preInit) throw new Error("Invalid invocation of addStoreStateObservers after ngOnInit.");
        if (!hosts || hosts.length === 0) return;

        hosts.forEach((h) => {
            if (h) this._observerHosts.push(h);
        });
    }

    /**
     * Subscribes to an observable, and automatically unsubscribes when the ViewModel is destroyed.
     */
    protected subscribeTo<T>(observable: Observable<T>, next?: (value: T) => void, error?: (error: any) => void, complete?: () => void) {
        this._subscriptions.subscribeTo(observable, next, error, complete);
    }
}
