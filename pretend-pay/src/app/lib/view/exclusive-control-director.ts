import { FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { argumentMessage, argumentNullMessage } from "../utilities/error-message";

// Consider shifting logic to directives applied to selector and group controls
// example <mat-select exclusiveGroupSelector="payee-type" formControl="payeeTypeFormControl" >
// <input exclusiveGroup="payee-type" >
// https://stackoverflow.com/questions/41171686/angular2-v-2-3-have-a-directive-access-a-formcontrol-created-through-formcontr


export interface ExclusiveControlGroup<TValue = string> {
    readonly type: TValue;
    readonly controls: FormControl | FormControl[];
}

/**
 * GroupSelectorAccessor provides the abstraction necessary to implement a bridge between ExclusiveControlDirector and specific underlying
 * selector component types.
 */
export abstract class GroupSelectorAccessor<TValue = string> {
    /**
     * When implemented in a derived class, returns an object that can be used by the director to determine whether or not it has a
     * stale reference to the underlying selector component.
     * Typically, the value returned by this method will be the current selector component itself.
     * This is necessary because component instances can potentially be created and destroyed multiple times within the
     * host component's lifecycle due to the use of *ngIf structural directives.
     */
    abstract getControlContext(): any;
    /**
     * Subscribes to the underlying component's change event handler.
     * @param handler The event handler
     */
    abstract subscribeToChangeEvent(handler: () => void): Subscription;
    /**
     * Resolves the active control group based upon the underlying component's selected value.
     * @param sequence The set of control groups in ordinal sequence.
     */
    abstract resolveSelected(sequence: ExclusiveControlGroup<TValue>[]): ExclusiveControlGroup<TValue>;
    /**
     * Sets the underlying component's selected value
     * @param ordinal The ordinal value of the active group.
     * @param type The type value of the selected group.
     */
    abstract setSelected(ordinal: number, type: TValue): void;
}

/**
 * The base class for GroupSelectorAccessor implementations whose components implement an ordinal index.
 */
export abstract class OrdinalGroupSelectorAccessor<TValue = string> extends GroupSelectorAccessor<TValue> {
    abstract getSelectedIndex(): number;

    resolveSelected(sequence: ExclusiveControlGroup<TValue>[]): ExclusiveControlGroup<TValue> {
        const idx: number = this.getSelectedIndex();
        if (idx >= sequence.length) return;
        return sequence[idx];
    }
}

/**
 * The base class for GroupSelectorAccessor implementations whose components implement a key-based index.
 */
export abstract class ValueGroupSelectorAccessor<TValue = string> extends GroupSelectorAccessor<TValue> {
    abstract getSelectedValue(): TValue;

    resolveSelected(sequence: ExclusiveControlGroup<TValue>[]): ExclusiveControlGroup<TValue> {
        const value: TValue = this.getSelectedValue();
        return sequence.find((g) => {
            return g.type === value;
        });
    }
}

/**
 * Coordinates the enabled state of conditional form controls contained within exclusive groups so that controls contained
 * within inactive groups do not raise validation errors.
 * ExclusiveControlDirector ensures that the specified controls are enabled when their associated group is selected, and
 * disabled when it is not.
 */
export class ExclusiveControlDirector<TValue = string> {
    constructor(
        initialGroup: TValue,
        protected sequence: ExclusiveControlGroup<TValue>[]
    ) {
        if (sequence == undefined) throw new Error(argumentNullMessage("sequence"));
        if (sequence.length === 0) throw new Error(argumentMessage("sequence"));
        this.selected = initialGroup;
        this.refresh();
    }

    /**
     * Gets or sets a value representing the currently selected group.
     */
    selected: TValue;

    /**
     * Sets selected to the specified value and invokes the refresh method.
     * @param value The selected value to apply
     */
    update(value: TValue) {
        this.selected = value;
        this.refresh();
    }

    /**
     * Refreshes the enabled state of the managed form controls.
     */
    refresh() {
        this.refreshControlState();
    }

    protected refreshControlState(onSetSelected?: (idx: number, value: TValue) => void) {
        let idx = 0;
        let enabledSequence: ExclusiveControlGroup<TValue>;
        let enabledIdx: number;
        this.sequence.forEach((ts) => {
            if (ts.type === this.selected) {
                enabledSequence = ts;
                enabledIdx = idx;
            } else {
                setControlState(false, ts.controls);
            }
            idx++;
        });

        // Because sets may contain duplicate controls, set the state of the enabled set last so that all member controls are
        // guaranteed to be enabled.
        if (enabledSequence) {
            setControlState(true, enabledSequence.controls);
            if (onSetSelected) onSetSelected(enabledIdx, this.selected);
        }
    }
}

function setControlState(enabled: boolean, controls: FormControl | FormControl[]) {
    if (controls == undefined) return;

    if (Array.isArray(controls)) {
        if (enabled) {
            controls.forEach((c) => { c.enable(); });
        } else {
            controls.forEach((c) => { c.disable(); });
        }
    } else {
        if (enabled) controls.enable(); else controls.disable();
    }
}
