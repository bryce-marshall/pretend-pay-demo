import { LoadingState } from "../types";

// Use LoadingState, which indicates

/**
 * An interface implemented by objects that expose information about the state of asynchronous background operations
 * which affect the state of a view.
 */
export interface BusyStateProvider {
    /**
     * Represents the state of one or more data retrieval operations that a view is dependent upon.
     * Typically, the view will not fully construct its interface until the data that it depends upon has loaded.
     */
    readonly loadingState: LoadingState;
    /**
     * Indicates whether or not a background operation that inhibits a view's functionality is occurring.
     * Typically, a user's interaction with the view will be restricted in some way for the duration of such an operation.
     */
    readonly isBusy: boolean;
}
