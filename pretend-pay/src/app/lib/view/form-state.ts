import { FormGroup } from "@angular/forms";
import { LoadingState } from "../types";
import { BusyStateProvider } from "./busy-state-provider";

export class FormState {
    private _attempted = false;

    constructor(private form: FormGroup, private provider?: BusyStateProvider) {
        if (!provider) {
            this.provider = {
                isBusy: false,
                loadingState: "loaded"
            };
        }
    }

    get loadingState(): LoadingState {
        return this.provider.loadingState;
    }

    get isLoading(): boolean {
        return this.provider.loadingState === "loading";
    }

    get isLoaded(): boolean {
        return this.provider.loadingState === "loaded";
    }

    get isBusy(): boolean {
        return this.provider.isBusy;
    }

    get submitAttempted(): boolean {
        return this._attempted;
    }

    get disableSubmit(): boolean {
        return (this.isBusy || this.isLoading) || this.showError;
    }

    get showError(): boolean {
        return this._attempted && this.form.invalid;
    }

    evalSubmit(): boolean {
        setTimeout(() => {
            // Allow any form control validation error messages to be displayed before the submit button is potentially disabled
            // (disabling the button in the immediate execution context will potentially prevent the messages being displayed)
            this._attempted = true;
        });

        return this.form ? this.form.valid : true;
    }

    resetSubmit() {
        this._attempted = false;
    }
}
