import { Observable } from "rxjs";

export interface LifecycleEventSource<T = any> {
    changesEvent: Observable<T>;
    initEvent: Observable<T>;
    doCheckEvent: Observable<T>;
    afterContentInitEvent: Observable<T>;
    afterContentCheckedEvent: Observable<T>;
    afterViewInitEvent: Observable<T>;
    afterViewCheckedEvent: Observable<T>;
    destroyEvent: Observable<T>;
}
