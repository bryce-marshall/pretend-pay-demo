export interface Translatable {
    readonly key: string;
    readonly args: any;
}
