import { ButtonResourceType } from "../types";

export interface ConfirmModalData {
    titleResourceKey: string;
    messageResourceKey: string;
    confirmButtonType: ButtonResourceType;
    cancelButtonType?: ButtonResourceType;
    formatArgs: any;
    isConfirmed?: boolean;
}
