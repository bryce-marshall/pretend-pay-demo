import { ValidatorFn, AbstractControl } from "@angular/forms";
import { AccountNumber } from "../financial-account";

export class AppValidators {
    static greaterThan(bound: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const value = asNumber(control.value);
            return (value == undefined || value > bound) ? undefined : { "greaterThan": { value: value } };
        };
    }

    static maxPlaces(places: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const value = asNumber(control.value);
            if (value == undefined) return undefined;
            // This function presents two challenges. First, it does not lend itself easily to a mathmatical solution because
            // of the inherent innaccuracy of floating point arithmetic (for example in Javascript 1.1 - 1 = 0.10000000000000009).
            // Secondly, different locales may accept input using different decimal separator characters (i.e. either '.' or ',').
            // The simple solution is to convert the input value to a string and validate against the number of characters
            // to the right of the decimal separator.
            // This solution is not an issue for right-to-left languages, as strings still start at index 0.
            const svalue = value.toString(10);
            const idx = Math.max(svalue.indexOf("."), svalue.indexOf(","));
            return idx < 0 || svalue.length - idx - 1 <= places ? undefined : { "maxPlaces": { value: value } };
        };
    }

    static accountNumber(control: AbstractControl): { [key: string]: any } | undefined {
        let result;
        const accountNumber = control.value as AccountNumber;
        if (accountNumber && accountNumber !== "" && (typeof accountNumber !== "string"
        || accountNumber.match(/^\d{4}-\d{4}-\d{4}-\d{4}$/g) == undefined)) {
            result = { "accountNumber": { value: accountNumber } };
        }

        return result;
    }
}

function asNumber(input: any): number {
    if (input == undefined || input === "") return undefined;
    if (typeof input === "number") return input;
    if (typeof input !== "string") return undefined;
    input = Number.parseFloat(input);
    return input === NaN ? undefined : input;
}
