import { FormControl } from "@angular/forms";
import { argumentNullMessage } from "../utilities/error-message";
import { ExclusiveControlDirector, ExclusiveControlGroup } from "./exclusive-control-director";

/**
 * Binds to a controlling FormControl to automatically coordinate the enabled state of conditional form controls contained
 * within exclusive groups so that controls contained within inactive groups do not raise validation errors.
 * Binding to the controller form control is one-way; it is assumed that an underlying component manages the form control state.
 */
export class FormControlExclusiveControlDirector<TValue> extends ExclusiveControlDirector<TValue> {
    constructor(
        controller: FormControl,
        sequence: ExclusiveControlGroup<TValue>[]
    ) {
        if (!controller) throw new Error(argumentNullMessage("controller"));
        super(controller.value, sequence);
        controller.valueChanges.subscribe((value) => {
            this.update(value);
        });
    }
}
