/**
 * Represents an authentication token that can be used by the application to access secure API resources.
 */
export interface AuthToken {
    token: string;
    expires: Date;
}
