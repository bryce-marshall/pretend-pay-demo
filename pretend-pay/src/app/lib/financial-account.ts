export type AccountNumber = string;

export class FinancialAccount {
    number: AccountNumber;
    email: string;
    name: string;
    currency: string;
    balance: number;
    available: number;
}
