
export interface UserProfile {
    entityVersion: number;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    streetAddress: string;
    suburb: string;
    town: string;
    postcode: string;
    state: string;
    country: string;
    locale: string;
}
