import { VersionedEntity } from "./versioned-entity";
import { PayeeType } from "./types";

export interface SavedPayee extends VersionedEntity<number> {
    id: number;
    name: string;
    description?: string;
    payeeType: PayeeType;
    paymentTarget: string;
}
