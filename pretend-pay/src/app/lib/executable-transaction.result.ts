import { AccountTransaction } from "./account-transaction";
import { FinancialAccount } from "./financial-account";

export interface ExecutableTransactionResult {
    account: FinancialAccount;
    transaction: AccountTransaction;
}
