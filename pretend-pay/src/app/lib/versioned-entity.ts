export interface VersionedEntity<T> {
    id: T;
    entityVersion: number;
}
