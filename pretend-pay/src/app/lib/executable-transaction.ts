import { AccountNumber } from "./financial-account";
import { PayeeType } from "./types";

export interface ExecutableTransaction {
    fromAccount: AccountNumber;
    payeeType: PayeeType;
    payee: AccountNumber | string;
    memo: string;
    payeeMemo: string;
    value: number;
}
