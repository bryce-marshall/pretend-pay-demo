export type FeedbackType = "usage" | "products" | "experience";

export class FeedbackData {
    userId: string;
    feedbackType: FeedbackType;
    feedback: string;
}
