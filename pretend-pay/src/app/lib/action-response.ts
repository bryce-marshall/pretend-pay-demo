import { ActionInfo } from "./action-info";
import { ApiResponseCode, ApiResponseData, KnownApiResponseCode } from "./api-response";
import { argumentNullMessage } from "./utilities/error-message";

export class ActionResponse<T> implements ApiResponseData<T> {
    readonly result: T;

    constructor(readonly info: ActionInfo, result?: T) {
        if (!info) throw new Error(argumentNullMessage("info"));
        this.result = result;
    }

    get isOk(): boolean {
        return this.code === KnownApiResponseCode.Ok;
    }

    get code(): ApiResponseCode {
        return this.info.responseCode;
    }

    asPromise(): Promise<ActionResponse<T>> {
        return this.isOk ? Promise.resolve(this) : Promise.reject(this);
    }
}
