import { Observable, Subject, Subscription } from "rxjs";
import { SubscriptionManager } from "../subscription-manager";
import { argumentNullMessage } from "../utilities/error-message";

export abstract class BackgroundTask {
    private _enabled = true;
    private _enabledEvals: (() => boolean)[];
    private _enabledStateChanged: Subject<BackgroundTask>;
    private _subscriptions: SubscriptionManager;
    enabledStateChanged: Observable<BackgroundTask>;

    constructor(public readonly name: string) {
        this._enabledStateChanged = new Subject<BackgroundTask>();
        this.enabledStateChanged = this._enabledStateChanged.asObservable();
    }

    /**
     * Invoked once when the task is added to the task manager.
     */
    init() {
        this.refreshEnabledState();
    }

    abstract nextInterval(): number;
    abstract execute(): Promise<any>;

    destroy() {
        if (this._subscriptions) this._subscriptions.unsubscribe();
    }

    get enabled(): boolean {
        return this._enabled;
    }

    protected registerSubscription<T = Subscription | Subject<any>>(subscription: T): T {
        if (!subscription) throw new Error(argumentNullMessage("subscription"));
        if (!this._subscriptions) this._subscriptions = new SubscriptionManager();
        this._subscriptions.subscribeTo(<any>subscription);
        return subscription;
    }

    protected registerEnabledStateEvaluator(stateEvaluator: () => boolean) {
        if (!stateEvaluator) throw new Error(argumentNullMessage("stateEvaluator"));
        if (!this._enabledEvals) this._enabledEvals = [];
        this._enabledEvals.push(stateEvaluator);
    }

    protected refreshEnabledState() {
        if (!this._enabledEvals) return;

        let value = true;
        for (let i = 0; value && i < this._enabledEvals.length; i++) {
            value = value && (this._enabledEvals[i]() === true);
        }

        if (value === this._enabled) return;
        this._enabled = value;
        this._enabledStateChanged.next(this);
    }
}
