export interface BackgroundTaskErrorEventArgs {
    readonly taskName: string;
    readonly error: any;
}
