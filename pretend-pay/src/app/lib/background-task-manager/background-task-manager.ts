import { Observable, Subject, Subscription } from "rxjs";
import { BackgroundTask } from "./background-task";
import { BackgroundTaskErrorEventArgs } from "./background-task-error-event-args";

const MAX_INTERVAL = 5000;

/**
 * A wrapper for background tasks, which provides basic task event and lifecycle management.
 */
class HostedTask {
    private _subscription: Subscription;

    constructor(enabledChanged: (task: BackgroundTask) => void, private task: BackgroundTask) {
        this.eventTime = Date.now() + task.nextInterval();
        this._subscription = task.enabledStateChanged.subscribe(enabledChanged);
    }

    initialized = false;
    active = false;
    eventTime = 0;

    get name(): string {
        return this.task.name;
    }

    get enabled(): boolean {
        return this.task.enabled;
    }

    initOnce() {
        if (this.initialized) return;
        this.task.init();
        this.initialized = true;
    }

    execute(): Promise<void> {
        this.active = true;
        return new Promise<void>((resolve, reject) => {
            this.task.execute().then(() => {
                resolve();
            })
                .catch((e) => {
                    reject(e);
                })
                .finally(() => {
                    this.active = false;
                    this.eventTime = Date.now() + Math.max(this.task.nextInterval(), 0);
                });
        });
    }

    destroy() {
        this._subscription.unsubscribe();
        this.task.destroy();
    }
}

export class BackgroundTaskManager {
    private _enabled = false;
    private _scheduled = false;
    private _enabledCount = 0;
    private _tasks: HostedTask[] = [];
    private _taskErrorSubject: Subject<BackgroundTaskErrorEventArgs>;
    onTaskError: Observable<BackgroundTaskErrorEventArgs>;

    constructor() {
        this._taskErrorSubject = new Subject<BackgroundTaskErrorEventArgs>();
        this.onTaskError = this._taskErrorSubject.asObservable();
    }

    get enabled() {
        return this._enabled;
    }

    set enabled(value: boolean) {
        if (this._enabled === value) return;
        this._enabled = value === true;
        if (this._enabled) this._tasks.forEach(t => { t.initOnce(); });
        this.tickIf();
    }

    addTask(...tasks: BackgroundTask[]) {
        tasks.forEach(t => {
            if (t) {
                const ht = new HostedTask((task) => {
                    if (!ht.initialized) return;
                    this._enabledCount = this._enabledCount + (task.enabled ? 1 : -1);
                    this.tickIf();
                }, t);
                if (this._enabled) ht.initOnce();
                this._tasks.push(ht);
            }
        });

        this.sort();
        this.tickIf();
    }

    destroy() {
        this._enabled = false;
        this._enabledCount = 0;
        this._tasks.forEach(task => {
            task.destroy();
        });
        this._tasks = [];
    }

    private tickIf() {
        // tslint:disable-next-line: max-line-length
        // console.log(`***! BackgroundTaskManager.tickIf() taskCount=${this._tasks.length}, enabled=${this.enabled}, enabledCount=${this._enabledCount}, scheduled=${this._scheduled}`);
        if (
            // Only tick if both this instance and one or more tasks are enabled.
            this._enabled && this._enabledCount > 0
            // Don't tick if a tick has already been scheduled (also safeguards against a tick being triggered by an executing task).
            && !this._scheduled) {
            this.tick();
        }
    }

    private tick() {
        // Note that a more sophisticated scheduling mechanism might be implemented in the event that
        // a large number of tasks were being managed.
        this._scheduled = this._enabled === true && this._enabledCount > 0;

        if (!this._scheduled) return;

        const now = Date.now();
        let idx = 0;
        while (idx < this._tasks.length) {
            const task = this._tasks[idx];
            // As tasks are sorted by next scheduled event time, if the event time of the next task  has not been reached
            // then no subsequent tasks will be in scope either.
            if (task.eventTime <= now) {
                if (task.enabled && !task.active) {
                    // Multiple asynchronous tasks may be active concurrently
                    task.execute().catch((e) => {
                        this._taskErrorSubject.next({
                            taskName: task.name,
                            error: e
                        });
                    });
                }
                idx++;
            } else {
                idx = this._tasks.length;
            }
        }

        this.sort();
        // Resolve the next event time. Use the event time of either the first enabled task, or MAX_INTERVAL,
        // with a maximum interval of MAX_INTERVAL.
        // The MAX_INTERVAL constant is to ensure that the addition, enabling and disabling of tasks does not
        // result in long delays between event resumption,
        // a scenario which could otherwise occur if a task with a near event time was added or enabled after a tick had been
        // scheduled for an event with a distant event time.
        const nextTask = this._tasks.find((t) => t.enabled);

        setTimeout(() => {
            this.tick();
        }, nextTask ? Math.min(Math.max(nextTask.eventTime - Date.now(), 0), MAX_INTERVAL) : MAX_INTERVAL);
    }

    private sort() {
        // Theoretically, sorting the task array after each execution cycle will result in a performance hit
        // (it would be faster to simply iterate the array and execute each task whose event time had been reached).
        // Sorting does, however, ensure that order of task execution is always prioritised by task event time.
        this._tasks.sort((a, b) => a.eventTime - b.eventTime);
    }
}
