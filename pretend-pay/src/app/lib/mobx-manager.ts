import { autorun, IReactionDisposer, IReactionPublic, IAutorunOptions } from "mobx";

export class MobxManager {
    private _disposers: IReactionDisposer[];

    autorun(view: (r: IReactionPublic) => any, opts?: IAutorunOptions) {
        if (!this._disposers) this._disposers = [];
        this._disposers.push(autorun(view, opts));
    }

    dispose() {
        this._disposers.forEach(disposer => disposer());
        this._disposers = undefined;
    }
}
