import { TransactionData } from "./transaction-data";

export class AccountTransaction {
    id: number;
    time: Date;
    memo: string;
    value: number;
    balance: number;
    data?: TransactionData[];
}
