import { AccountNumber } from "./financial-account";
import { AppConstants } from "../app-constants";

export type TransactionDirection = "in" | "out" | "both";

export interface TransactionQueryArgs {
    account: AccountNumber;
    pageSize: number;
    page: number;
    sortAscending?: boolean;
    minDate?: Date;
    maxDate?: Date;
    minValue?: number;
    maxValue?: number;
    direction?: TransactionDirection;
    text?: string;
}

export class TransactionQueryArgs implements TransactionQueryArgs {
    constructor(public account: AccountNumber, public page: number, public pageSize: number) {
    }

    static createDefault(account: AccountNumber): TransactionQueryArgs {
        return new TransactionQueryArgs(account, 0, AppConstants.TransactionPageSize);
    }
}
