export type NotificationType = "account-receipt";

export interface AppNotification {
    id: number;
    notificationType: NotificationType;
}
