import { LoadingStateProvider } from "./loading-state-provider";
import { LoadingState } from "./types";
import { action, observable, computed } from "mobx";

export class ManualLoadingStateProvider implements LoadingStateProvider {
    @observable private _loadingState: LoadingState = "loading";

    @computed get loadingState(): LoadingState {
        return this._loadingState;
    }

    @action setState(loadingState: LoadingState) {
        this._loadingState = loadingState;
    }
}
