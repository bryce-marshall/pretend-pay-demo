import { AppNotification } from "./app-notification";
import { AccountNumber } from "./financial-account";

export interface AccountReceiptNotification extends AppNotification {
    accountNumber: AccountNumber;
    accountName: string;
    currency: string;
    transactionCount: number;
    total: number;
}
