export enum TransactionDataType {
    TransferFromAccount = "tf",
    TransferToAccount = "tt",
    PayerAccount = "af",
    PayeeAccount = "at",
    PayeeEmail = "et"
}

export interface TransactionData {
    type: TransactionDataType;
    data: any;
}
