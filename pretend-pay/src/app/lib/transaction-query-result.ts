import { AccountTransaction } from "./account-transaction";

export interface TransactionQueryResult {
    currentPage: number;
    totalPages: number;
    totalTransactions: number;
    transactions: AccountTransaction[];
}
