import { ActionResponse } from "./action-response";
import { ApiResponse, ApiResponseCode, KnownApiResponseCode } from "./api-response";
import { argumentNullMessage } from "./utilities/error-message";

/**
 * Exposes information about the state of a response to an application action, including resource keys which can be used to
 * lookup localized messages.
 */
export interface ActionInfo {
    /**
     * The unique identifier of the ActionInfo instance within the AppViewStore. Assigned internally by the store.
     */
    id?: number;
    /**
     * The response code returned as a resuly of the action.
     */
    readonly responseCode: ApiResponseCode;
    /**
     * An optional resource-key which may be used to lookup a message describing the action context.
     */
    readonly titleResource?: string;
    /**
     * A resource-key which may be used to lookup a message describing the action outcome.
     */
    readonly outcomeResource: string;
    /**
     * An object containing properties which can be used as substitution arguments by message formatters.
     */
    readonly args: any;
}

export class ActionInfo {
    constructor(readonly responseCode: ApiResponseCode, readonly outcomeResource: string, readonly args: any,
        readonly titleResource?: string) {
    }

    static convert(info: ActionInfo | ActionResponse<any> | ApiResponse<any> | ApiResponseCode | any) {
        if (info == undefined) throw new Error(argumentNullMessage("info"));
        // Handle ActionInfo
        if (info.hasOwnProperty("responseCode") && info.hasOwnProperty("outcomeResource")) return <ActionInfo>info;
        // Handle ActionResponse
        if (info.hasOwnProperty("info")) return <ActionInfo>(<any>info).info;
        // Handle ApiResponse
        if (info.hasOwnProperty("code")) return actionInfoFromResponseCode(info.code);
        // Handle pure api response code
        if (typeof info === "string") return actionInfoFromResponseCode(<string>info);

        return actionInfoFromResponseCode(KnownApiResponseCode.ApplicationError);
    }
}

function actionInfoFromResponseCode(code: string): ActionInfo {
    const keyPrefix = "actionInfo-generic.";
    return {
        responseCode: code,
        outcomeResource: keyPrefix + code === KnownApiResponseCode.Ok ? "ok" : "failed",
        args: undefined
    };
}
