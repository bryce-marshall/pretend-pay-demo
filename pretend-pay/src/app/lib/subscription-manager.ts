import { SubscriptionLike, Observable } from "rxjs";

export class SubscriptionManager {
    private _subscriptions: SubscriptionLike[];

    /**
     * Subscribes to an observable.
     */
    subscribeTo<T>(observable: Observable<T>, next?: (value: T) => void, error?: (error: any) => void, complete?: () => void) {
        if (!this._subscriptions) this._subscriptions = [];
        this._subscriptions.push(observable.subscribe(next, error, complete));
    }

    /**
     * Unsubscribes from all subcriptions managed by this instance.
     */
    unsubscribe() {
        if (!this._subscriptions) return;
        this._subscriptions.forEach((s) => {
            s.unsubscribe();
        });
        this._subscriptions = undefined;
    }
}
