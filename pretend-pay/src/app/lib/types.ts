export type LoadingState = "error" | "loading" | "loaded" | undefined;

export type AppStateType = "error" | "initializing" | "initialized";

export type IdentityStateType = "authenticated" | "authenticating" | "not-authenticated" | "expired" | "error";

export type ViewContext = "accounts" | "pay-transfer" | "payees" | "profile" | "other";

export type SyncState = "in-sync" | "out-of-sync" | "syncing";

export type PayeeType = "account-number" | "email";

export type UserPaymentType = "transfer" | "email" | "account-number" | "payee";

export type ButtonResourceType = "cancel" | "ok" | "delete";

export type LanguageDirection = "ltr" | "rtl";

export type ViewportType = "phone" | "tablet";
