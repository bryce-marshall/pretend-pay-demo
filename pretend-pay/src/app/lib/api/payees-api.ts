import { ApiResponse } from "src/app/lib/api-response";
import { SavedPayee } from "../saved-payee";
import { VersionedEntity } from "../versioned-entity";

/**
 * The base class for Payees API implementations.
 */
export abstract class PayeesApi {
    abstract getSavedPayees(): Promise<ApiResponse<SavedPayee[]>>;
    abstract createPayee(payee: SavedPayee): Promise<ApiResponse<SavedPayee>>;
    abstract updatePayee(payee: SavedPayee): Promise<ApiResponse<SavedPayee>>;
    abstract deletePayee(payee: VersionedEntity<number>): Promise<ApiResponse<SavedPayee>>;
}
