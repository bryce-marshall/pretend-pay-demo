import { ApiResponse } from "src/app/lib/api-response";
import { FinancialAccount } from "src/app/lib/financial-account";
import { TransactionQueryArgs } from "src/app/lib/transaction-query-args";
import { ExecutableTransaction } from "../executable-transaction";
import { TransactionQueryResult } from "../transaction-query-result";
import { ExecutableTransactionResult } from "../executable-transaction.result";

/**
 * The base class for Acccounts API implementations.
 */
export abstract class AccountsApi {
    abstract getAccounts(): Promise<ApiResponse<FinancialAccount[]>>;
    abstract queryTransactions(args: TransactionQueryArgs): Promise<ApiResponse<TransactionQueryResult>>;
    abstract executeTransaction(transaction: ExecutableTransaction): Promise<ApiResponse<ExecutableTransactionResult>>;
}
