import { ApiResponse } from "src/app/lib/api-response";
import { AuthToken } from "src/app/lib/auth-token";
import { UserIdentity } from "src/app/lib/user-identity";

/**
 * The structure returned by the AuthApiService login method.
 */
export interface LoginData {
    authToken: AuthToken;
    identity: UserIdentity;
}

/**
 * The base class for application authentication and authorization API implementations.
 */
export abstract class AuthApi {
    abstract login(username: string, password: string): Promise<ApiResponse<LoginData>>;
}
