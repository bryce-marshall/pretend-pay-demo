import { ApiResponse } from "src/app/lib/api-response";
import { FeedbackData } from "src/app/lib/feedback-data";
import { AppNotification } from "../app-notification";
import { UserProfile } from "../user-profile";

/**
 * The base class for application subscriber  API implementations.
 */
export abstract class SubscriberApi {
    abstract getProfile(): Promise<ApiResponse<UserProfile>>;
    abstract saveProfile(profile: UserProfile): Promise<ApiResponse<UserProfile>>;
    abstract submitFeedback(feedbackData: FeedbackData): Promise<ApiResponse<any>>;
    /**
     * Gets the set of outstanding notifications for the currently authenticated user from the remote service.
     * @param boundingNotificationId The notificationId of the last notification the client previously retrieved from the service.
     */
    abstract getNotifications(boundingNotificationId: number): Promise<ApiResponse<AppNotification[]>>;
}
