/**
 * Describes the identity of an application user.
 */
export interface UserIdentity {
    readonly id: string;
    readonly username: string;
    readonly displayName: string;
}
