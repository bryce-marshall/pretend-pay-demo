/**
 * An interface facilitating iteration over a collection.
 */
export interface Enumerator<T> {
    readonly current: T;
    moveNext(): boolean;
    reset(): void;
}

export class ArrayEnumerator<T> implements Enumerator<T> {
    private idx = -1;
    private currentEl: T;

    constructor(private array: T[]) {
    }

    get current(): T {
        // tslint:disable-next-line: max-line-length
        if (this.idx < 0 || this.idx >= this.array.length) throw new Error("The Enumerator cursor is positioned outside of the collection");
        return this.currentEl;
    }

    moveNext(): boolean {
        if (this.idx === this.array.length) return false;

        this.idx++;
        const result = this.idx < this.array.length;
        this.currentEl = result ? this.array[this.idx] : undefined;
        return result;
    }

    reset() {
        this.idx = -1;
        this.currentEl = undefined;
    }
}

export async function asynchIterate<T>(enumerator: Enumerator<T>, handler: (value: T) => boolean, batchSize: number = 500): Promise<void> {
    const result = new Promise<void>((resolve) => {
        const fn = () => {
            let cancelled = false;
            let step = 0;
            while (step < batchSize && enumerator.moveNext()) {
                step++;
                cancelled = handler(enumerator.current);
            }

            if (!cancelled && step < batchSize) {
                resolve();
            } else {
                setTimeout(fn);
            }
        };

        fn();
    });

    return result;
}
