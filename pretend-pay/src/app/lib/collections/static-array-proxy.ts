import { LinkedList } from "./linked-list";

/**
 * Wraps an internal reference array and allows elements to be removed from the wrapper without changing the reference array.
 * If the reference array grows in length, the proxy array will grow also, but elements previously removed from the proxy
 * will remain removed. The reference array is a list that may grow but must not shrink or be reordered.
 */
export class StaticArrayProxy<T> {
    private _list: LinkedList<Proxy<T>> = new LinkedList();
    private _refIdx = 0;

    constructor(ref?: T[]) {
        this.append(ref);
    }

    remove(compareFn: (item: T) => boolean): boolean {
        const item = this._list.find((proxy) => {
            return compareFn(proxy.item);
        });

        if (!item) return false;
        this._list.remove(item);
        return true;
    }

    append(src: T[]) {
        if (!src || src.length === 0) return;
        src.forEach((item) => {
            this._list.append({ item: item, index: this._refIdx++ });
        });
    }

    extend(src: T[]) {
        if (!src || !src.length) return;

        while (this._refIdx < src.length) {
            this._list.append({ item: src[this._refIdx], index: this._refIdx++ });
        }
    }

    merge(src: T[]) {
        if (!src || src.length === 0) return;

        if (this._list.count > 0) {
            let listItem = this._list.first;
            let proxy = this._list.first.item;
            while (proxy != undefined && proxy.index < src.length) {
                proxy.item = src[proxy.index];
                listItem = listItem.next;
                proxy = listItem.next ? listItem.next.item : undefined;
            }
        }

        while (this._refIdx < src.length) {
            this._list.append({ item: src[this._refIdx], index: this._refIdx++ });
        }
    }

    clear() {
        this._list.clear();
        this._refIdx = 0;
    }

    toArray(): T[] {
        const result: T[] = [];
        let proxy = this._list.first;

        while (proxy != undefined) {
            result.push(proxy.item.item);
            proxy = proxy.next;
        }

        return result;
    }
}

interface Proxy<T> {
    item: T;
    index: number;
}
