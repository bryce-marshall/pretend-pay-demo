/**
 * Implements a simple Queue by encapsulating an Array.
 */
export class Queue<T> {
    private _array: T[];

    constructor(array?: T[]) {
        this._array = array ? array : [];
    }

    /**
     * Removes all items from the queue.
     */
    clear() {
        this._array.splice(0, this._array.length);
    }

    get count(): number {
        return this._array.length;
    }

    /**
     * Adds an item to the queue.
     * @param item The item to add
     */
    enqueue(item: T) {
        this._array.unshift(item);
    }

    /**
     * Removes the item at the front of the queue.
     */
    dequeue(): T {
        if (this.count === 0) throw new Error("Invalid attempt to dequeue from an empty Queue.");
        return this._array.shift();
    }

    enqueueAll(items: T[]) {
        if (items == undefined || items.length === 0) return;

        items.forEach((item) => {
            this._array.unshift(item);
        });
    }

    dequeueAll() {
        if (this.count === 0) throw new Error("Invalid attempt to dequeue from an empty Queue.");
        return this._array.splice(0, this._array.length);
    }

    /**
     * Returns the object at the beginning of the queue without removing it.
     */
    peek(): T {
        if (this.count === 0) throw new Error("Invalid attempt to peek an empty Queue.");
        return this._array[0];
    }

    /**
     * Removes from the queue all items for which the comparer function evaluates true.
     * @param comparer The comparer function to use to evaluate candidates for removal.
     */
    remove(comparer: (x: T) => boolean) {
        const idx = this._array.findIndex(comparer);
        if (idx < 0) return undefined;
        const r = this._array[idx];
        this._array.splice(idx, 1);

        return r;
    }

    /**
     * Returns the contents of the queue as an array.
     */
    toArray(): T[] {
        return this._array.slice(0);
    }
}
