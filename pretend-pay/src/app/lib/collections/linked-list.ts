import { argumentNullMessage } from "../utilities/error-message";

export interface ListItem<TItem> {
    readonly item: TItem;
    readonly prev: ListItem<TItem>;
    readonly next: ListItem<TItem>;
}

/**
 * A doubly-linked list.
 */
export class LinkedList<TItem> {
    private _count = 0;
    private _first: ListItemInt<TItem>;
    private _last: ListItemInt<TItem>;

    get count(): number {
        return this._count;
    }

    get first(): ListItem<TItem> {
        return this._first;
    }

    get last(): ListItem<TItem> {
        return this._last;
    }

    /**
     * Removes all items from the queue.
     */
    clear() {
        this._first = undefined;
        this._last = undefined;
        this._count = 0;
    }

    /**
     * Adds an item to the end of list.
     * @param item The item to add
     */
    append(item: TItem): ListItem<TItem> {
        const lItem = listItemInt(this, item);
        lItem.prev = this._last;
        if (this._count > 0) {
            this._last.next = lItem;
        } else {
            this._first = lItem;
        }

        this._last = lItem;
        this._count++;

        return lItem;
    }

    /**
     * Adds an item to the start of list.
     * @param item The item to add
     */
    prepend(item: TItem): ListItem<TItem> {
        const lItem = listItemInt(this, item);
        lItem.next = this._first;
        if (this._count > 0) {
            this._first.prev = lItem;
        } else {
            this._last = lItem;
        }

        this._first = lItem;
        this._count++;

        return lItem;
    }

    moveFirst(item: ListItem<TItem>) {
        if (!item) throw new Error(argumentNullMessage("item"));
        if (item === this.first) return;
        const itemInt: ListItemInt<TItem> = <any>item;
        validate(this, itemInt, "item");
        detach(<any>this, itemInt);
        // The list has at least one other member
        this._first.prev = itemInt;
        itemInt.next = this._first;
        this._first = itemInt;
    }

    moveLast(item: ListItem<TItem>) {
        if (!item) throw new Error(argumentNullMessage("item"));
        if (item === this.last) return;
        const itemInt: ListItemInt<TItem> = <any>item;
        validate(this, itemInt, "item");
        detach(<any>this, itemInt);
        // The list has at least one other member
        this._last.next = itemInt;
        itemInt.prev = this._last;
        this._last = itemInt;
    }

    moveBefore(item: TItem, other: ListItem<TItem>) {
        if (!other) throw new Error(argumentNullMessage("other"));
        if (!item) throw new Error(argumentNullMessage("item"));
        const itemInt: ListItemInt<TItem> = <any>item;
        const otherInt: ListItemInt<TItem> = <any>other;
        validate(this, itemInt, "item");
        validate(this, otherInt, "other");
        detach(<any>this, itemInt);
        before(<any>this, itemInt, otherInt);
    }


    moveAfter(item: TItem, other: ListItem<TItem>) {
        if (!other) throw new Error(argumentNullMessage("other"));
        if (!item) throw new Error(argumentNullMessage("item"));
        const itemInt: ListItemInt<TItem> = <any>item;
        const otherInt: ListItemInt<TItem> = <any>other;
        validate(this, itemInt, "item");
        validate(this, otherInt, "other");
        detach(<any>this, itemInt);
        after(<any>this, itemInt, otherInt);
    }

    insertBefore(item: TItem, other: ListItem<TItem>): ListItem<TItem> {
        if (!other) throw new Error(argumentNullMessage("other"));
        if (!item) throw new Error(argumentNullMessage("item"));
        const otherInt: ListItemInt<TItem> = <any>other;
        validate(this, otherInt, "other");
        const itemInt = listItemInt(this, item);
        before(<any>this, itemInt, otherInt);

        return itemInt;
    }

    insertAfter(item: TItem, other: ListItem<TItem>): ListItem<TItem> {
        if (!other) throw new Error(argumentNullMessage("other"));
        if (!item) throw new Error(argumentNullMessage("item"));
        const otherInt: ListItemInt<TItem> = <any>other;
        validate(this, otherInt, "other");
        const itemInt = listItemInt(this, item);
        after(<any>this, itemInt, otherInt);

        return itemInt;
    }

    /**
     * Removes an item from the list.
     * @param item the item to remove.
     */
    remove(item: ListItem<TItem>) {
        if (!item) throw new Error(argumentNullMessage("item"));
        const itemInt: ListItemInt<TItem> = <any>item;
        validate(this, itemInt, "item");
        detach(<any>this, <any>item);
        itemInt.detach(); // Remove the association with this list
        // if (itemInt.prev) {
        //     itemInt.prev.next = itemInt.next;
        // }
        // else {
        //     this.firstInt = itemInt.next;
        // }

        // if (itemInt.next) {
        //     itemInt.next.prev = itemInt.prev;
        // }
        // else {
        //     this.lastInt = itemInt.prev;
        // }

        // itemInt.prev = undefined;
        // itemInt.next = undefined;
        this._count--;
    }

    find(compareFn: (item: TItem) => boolean): ListItem<TItem> {
        let item = this.first;
        while (item) {
            if (compareFn(item.item)) return item;
            item = item.next;
        }

        return undefined;
    }

    [Symbol.iterator] = () => {
        let current = this.first;
        return {
            next() {
                if (current == undefined) return { done: true };
                const r = { done: false, value: current };
                current = current.next;
                return r;
            }
        };
    }
}

interface LinkedListInt<TItem> {
    firstInt: ListItemInt<TItem>;
    lastInt: ListItemInt<TItem>;
}

interface ListItemInt<TItem> {
    item: TItem;
    prev: ListItemInt<TItem>;
    next: ListItemInt<TItem>;
    validate(list: LinkedList<TItem>): boolean;
    detach();
}

function listItemInt<TItem>(owner: LinkedList<TItem>, item: TItem): ListItemInt<TItem> {
    return {
        item: item,
        prev: undefined,
        next: undefined,
        validate: (list: LinkedList<TItem>) => {
            return list === owner;
        },
        detach: () => {
            owner = undefined;
        }
    };
}

function validate(owner: LinkedList<any>, item: ListItemInt<any>, paramName: string) {
    // tslint:disable-next-line: max-line-length
    if (!item.validate || !item.validate(owner)) throw new Error(`The ListItem "${paramName}" is not associated with the LinkedList instance.`);
}

function detach<TItem>(owner: LinkedListInt<TItem>, itemInt: ListItemInt<TItem>) {
    if (itemInt === owner.firstInt) {
        owner.firstInt = itemInt.next;
    } else {
        itemInt.prev.next = itemInt.next;
    }

    if (itemInt === owner.lastInt) {
        owner.lastInt = itemInt.prev;
    } else {
        itemInt.next.prev = itemInt.prev;
    }
    // if (itemInt.prev) {
    //     itemInt.prev.next = itemInt.next;
    // }
    // else {
    //     owner.firstInt = itemInt.next;
    // }

    // if (itemInt.next) {
    //     itemInt.next.prev = itemInt.prev;
    // }
    // else {
    //     owner.lastInt = itemInt.prev;
    // }

    itemInt.prev = undefined;
    itemInt.next = undefined;
}

function before<TItem>(owner: LinkedListInt<TItem>, itemInt: ListItemInt<TItem>, otherInt: ListItemInt<TItem>) {
    itemInt.prev = otherInt.prev;
    itemInt.next = otherInt;
    otherInt.prev = itemInt;
    if (otherInt === owner.firstInt) owner.firstInt = itemInt;
    if (otherInt === owner.lastInt) owner.lastInt = itemInt;
}
function after<TItem>(owner: LinkedListInt<TItem>, itemInt: ListItemInt<TItem>, otherInt: ListItemInt<TItem>) {
    itemInt.prev = otherInt;
    itemInt.next = otherInt.next;
    otherInt.next = itemInt;
    if (otherInt === owner.firstInt) owner.firstInt = itemInt;
    if (otherInt === owner.lastInt) owner.lastInt = itemInt;
}
