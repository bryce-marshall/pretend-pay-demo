/**
 * Implements a simple Stack by encapsulating an Array.
 */
export class Stack<T> {
    private _array: T[] = [];

    constructor(array?: T[]) {
        this._array = array ? array : [];
    }
    /**
     * Removes all items from the stack.
     */
    clear() {
        this._array.splice(0, this._array.length);
    }

    /**
     * The number of items in the stack.
     */
    get count(): number {
        return this._array.length;
    }

    /**
     * Pushes an item onto the stack.
     * @param item The item to push.
     */
    push(item: T) {
        this._array.push(item);
    }

    /**
     * Pops an item from the stack.
     */
    pop(): T {
        if (this.count === 0) throw new Error("Invalid attempt to pop from an empty Stack.");
        return this._array.pop();
    }

    /**
     * Returns the object at the top of the stack without removing it.
     */
    peek(): T {
        if (this.count === 0) throw new Error("Invalid attempt to peek an empty Stack.");
        return this._array[this._array.length - 1];
    }

    /**
     * Returns the first item for which the evaluates true, or undefined if there is no match.
     * @param comparer The comparer function to use to evaluate candidates for retrieval.
     */
    find(comparer: (x: T) => boolean): T {
        return this._array.find(comparer);
    }

    /**
     * Removes from the stack all items for which the comparer function evaluates true.
     * @param comparer The comparer function to use to evaluate candidates for removal.
     */
    remove(comparer: (x: T) => boolean): T {
        const idx = this._array.findIndex(comparer);
        if (idx < 0) return undefined;
        const r = this._array[idx];
        this._array.splice(idx, 1);

        return r;
    }

    /**
     * Returns the contents of the stack as an array.
     */
    toArray(): T[] {
        return this._array.slice(0);
    }
}
