export class ArrayHelper {
    static ensureArray<T>(target: T | T[]): T[] {
        if (target == undefined) return [];
        if (Array.isArray(target)) return target;
        return [target];
    }
}
