export class IdRegister {
    constructor(private seed?: number) {
        if (!seed) this.seed = 1;
    }

    peek(): number {
        return this.seed;
    }

    nextId(): number {
        return this.seed++;
    }
}
