import { Injectable, OnDestroy } from "@angular/core";
import { AccountReceiptNotification } from "src/app/lib/account-receipt-notification";
import { ApiResponse, KnownApiResponseCode } from "src/app/lib/api-response";
import { SubscriberApi } from "src/app/lib/api/subscriber-api";
import { AppNotification } from "src/app/lib/app-notification";
import { FeedbackData } from "src/app/lib/feedback-data";
import { IdRegister } from "src/app/lib/id-register";
import { UserProfile } from "src/app/lib/user-profile";
import { Currency } from "src/app/lib/utilities/currency";
import { cloneEntity } from "src/app/lib/utilities/entity-clone";
import { AppLogger } from "src/app/services/logger/app-logger";
import { asyncSleep } from "./functions";
import { AutoReceipt, AutoReceiptGenerator } from "./generators/auto-receipt-generator";

@Injectable({providedIn: "root"})
export class MockSubscriberApiService extends SubscriberApi implements OnDestroy {
    private readonly _version = new IdRegister();
    private readonly _notificationId = new IdRegister();
    private _profile: UserProfile;
    private _notifications: AppNotification[] = [];

    constructor(
        private logger: AppLogger,
        autoReceipts: AutoReceiptGenerator
        ) {
        super();

        this._profile = {
            entityVersion: this._version.nextId(),
            dateOfBirth: new Date(1980, 1, 1),
            firstName: "John",
            lastName: "Doe",
            locale: "en-GB",
            streetAddress: "Newtown Ave",
            suburb: "Newtown",
            town: "Wellington",
            postcode: "6021",
            state: "Wellington",
            country: "New Zealand"
        };

        autoReceipts.onTransaction.subscribe((receipt: AutoReceipt) => {
            let n: AccountReceiptNotification = <any>this._notifications.find((cmp) => {
                if (cmp.notificationType !== "account-receipt") return false;
                return (<AccountReceiptNotification>cmp).accountNumber === receipt.accountNumber;
            });
            if (!n) {
                n = {
                    id: this._notificationId.nextId(),
                    notificationType: "account-receipt",
                    accountName: receipt.accountName,
                    accountNumber: receipt.accountNumber,
                    currency: receipt.currencyCode,
                    transactionCount: 0,
                    total: 0
                };
                this._notifications.push(n);
            }

            n.transactionCount++;
            n.total = Currency.add(receipt.currencyCode, n.total, receipt.transaction.value);
        });
    }

    ngOnDestroy() {
        this.logger.trace("Destroyed service.");
    }

    async getProfile(): Promise<ApiResponse<UserProfile>> {
        await asyncSleep();
        
        return ApiResponse.resolve(cloneEntity(this._profile));
    }

    async saveProfile(profile: UserProfile): Promise<ApiResponse<UserProfile>> {
        await asyncSleep();
        if (!profile || !profile.entityVersion) return ApiResponse.reject(KnownApiResponseCode.InvalidOperation);
        // tslint:disable-next-line: max-line-length
        if (this._profile && this._profile.entityVersion !== profile.entityVersion) return ApiResponse.reject(KnownApiResponseCode.ConcurrencyError);

        this._profile = cloneEntity(profile);
        this._profile.entityVersion = this._version.nextId();
        return ApiResponse.resolve(cloneEntity(this._profile));
    }

    async submitFeedback(feedbackData: FeedbackData): Promise<ApiResponse<any>> {
        await asyncSleep();
        // tslint:disable-next-line: max-line-length
        if (feedbackData.feedback.toLowerCase() === "error") return ApiResponse.reject(ApiResponse.error(KnownApiResponseCode.ApplicationError));

        return ApiResponse.resolve();
    }

    async getNotifications(_boundingNotificationId: number): Promise<ApiResponse<AppNotification[]>> {
        await asyncSleep();
        const result = cloneEntity(this._notifications);
        this._notifications = [];

        return ApiResponse.resolve(result);
    }
}
