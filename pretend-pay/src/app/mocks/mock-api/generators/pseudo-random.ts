import { argumentMessage, argumentNullMessage } from "src/app/lib/utilities/error-message";

let autoseed = 1;

export class PseudoRandom {
    private _seed: number;

    constructor(seed?: number) {
        this.reset(seed != undefined ? seed : 1);
    }

    /**
     * A basic pseudo-random-number generator function that allows a seed to be specified.
     * @param seed An optional seed (use to generate repeatable results with each invocation). If omitted, a global seed is used.
     */
    static generate(seed?: number): number {
        if (!seed) {
            seed = autoseed++;
        } else if (seed < 1) {
            seed = 1;
        }

        const x = Math.sin(seed) * 10000;
        return x - Math.floor(x);
    }

    next(): number {
        return PseudoRandom.generate(this._seed++);
    }

    nextInteger(min: number, max: number) {
        if (max < min) throw new Error(argumentMessage("max"));
        return Math.trunc(this.next() * (1 + max - min) + min);
    }

    reset(seed: number) {
        if (seed == undefined) throw new Error(argumentNullMessage("seed"));
        // tslint:disable-next-line: max-line-length
        if (seed < 1 || typeof seed !== "number") throw new Error(argumentMessage("seed", "Must be an integer greater than or equal to 1."));
        if (seed !== Math.trunc(seed)) throw new Error(argumentMessage("seed", "Must be an integer."));
        this._seed = seed;
    }
}
