import { Enumerator } from "src/app/lib/collections/enumerator";

/**
 * Iterates over the days in a date range, returning the absolute time value (in milliseconds) at midnight for each day.
 */
interface DayIterator extends Enumerator<number> {
    /**
     * Returns the current value to a date object, or undefined if the cursor is positioned outside of the range.
     */
    getDate(): Date;
}

export function DayIterator(startDate: Date, endDate: Date): DayIterator {
    const dayOfMillis = 86400000; // A day's worth of milliseconds
    const max: number =  datePart(endDate);
    let millis: number = datePart(startDate) - dayOfMillis;

    const that = {
        current: undefined,

        moveNext: () => {
            if (millis < max) {
                millis += dayOfMillis;
                that.current = millis;
            } else {
                that.current = undefined;
            }

            return that.current != undefined;
        },

        reset: () => {
            millis = startDate.getTime() - dayOfMillis;
            that.current = undefined;
        },

        getDate: () => {
            return that.current ? new Date(that.current) : undefined;
        }
    };

    return that;
}

function datePart(endDate: Date): number {
    return new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()).getTime();
}
