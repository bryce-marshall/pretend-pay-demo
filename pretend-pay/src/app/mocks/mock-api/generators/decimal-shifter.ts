/**
 * Shifts a decimal number a specified number of places to the left and returns the resulting integral component.
 */
export interface DecimalShifter {
    readonly value: number;
    reset(seedValue: number);
    pop(places: number): number;
}

export function DecimalShifter(initalValue?: number): DecimalShifter {
    const that = {
        value: initalValue,
        reset: (seedValue) => {
            that.value = seedValue;
        },
        pop: (places: number) => {
            that.value *= (Math.pow(10, places));
            const v = Math.floor(that.value);
            that.value -= v;
            return v;
        }
    };

    return that;
}
