import { AccountTransaction } from "src/app/lib/account-transaction";
import { ArrayEnumerator, asynchIterate, Enumerator } from "src/app/lib/collections/enumerator";
import { IdRegister } from "src/app/lib/id-register";
import { TransactionDataType } from "src/app/lib/transaction-data";
import { TransactionQueryArgs } from "src/app/lib/transaction-query-args";
import { TransactionQueryResult } from "src/app/lib/transaction-query-result";
import { cloneEntity } from "src/app/lib/utilities/entity-clone";
import { MockFinancialAccount } from "../mock-finanical-account";
import { DayIterator } from "./day-iterator";
import { DecimalShifter } from "./decimal-shifter";
import { PseudoRandom } from "./pseudo-random";

const SEED_DATE = generateSeedDate();
const TERMINAL_DATE = new Date(SEED_DATE.getFullYear() + 1, SEED_DATE.getMonth(), SEED_DATE.getDate() - 1);
const MIN_DATE = 0;
const MAX_DATE = 86400000 * 100000000;
const MAX_PAGE_SIZE = 250;
const TX_BATCH_SIZE = 300;


//#region Mock Text

const shortText = [
    "Te nulla nostro pro",
    "Nostro verterem moderatius",
    "Ubique iisque vim an",
    "Facer nusquam id sed",
    "Vis in vero utamur discere,",
    "Iracundia vulputate quaerendum",
    "An luptatum similique qui",
    "Impetus legendos eu pri",
    "Ea pri mundi quaeque adolescens",
    "Percipit invidunt oportere",
    "Mei ei aeque homero",
    "Tempor eruditi aliquando",
    "Ne eius invidunt reformidans",
    "Vel laudem cetero",
    "Quo copiosae tractatos",
    "Ne eum mucius habemus",
    "Possim gloriatur cu mei",
    "An latine aliquando",
    "Usu et regione tritani",
    "Nonumy propriae et pri",
    "Solum nonumy prompta",
    "Vix te soluta offendit",
    "Iudicabit periculis his cu",
    "Ius ei modus repudiandae",
    "Detracto consetetur vix ex",
    "Sed voluptatum consectetuer",
    "Euismod erroribus sit",
    "Aliquid deserunt mandamus",
    "At sint scriptorem",
    "Ei duo omittam",
    "Et vim inermis menandri",
    "Per no esse facilis",
    "Per no sint saepe",
    "In maiorum definitionem nam",
    "Abhorreant temporibus reformidans mel",
    "Zril verear torquatos est",
    "Fuisset sensibus sententiae no",
    "Dolorum theophrastus eos ea",
    "Nisl assum convenire ad",
    "Te pro rebum postulant",
    "Ei vocent option lobortis",
    "Vix semper commodo ea",
    "Ad est alia hendrerit",
    "Mel purto aliquid percipit",
    "Quo epicuri patrioque ex",
    "An nec habeo volutpat",
    "Noluisse neglegentur in",
    "Nam quot vidit",
    "Quando vidisse evertitur",
    "Vis ea labores",
    "Ne facilis suavitate",
    "Ea unum iisque",
    "Eu nec alii",
    "Ne his ludus",
    "Platonem partiendo aliquando",
    "Expetendis",
    "Rebum",
    "Latine",
    "Illud",
    "Sanctus honestatis",
    "Est primis",
    "Illud iusto",
    "Vero paulo",
    "Ne eam",
    "Ex sonet",
    "Qui aeterno",
    "His tota",
    "Iriure deleniti"
];

//#endregion

//#region Internal Interfaces

interface TxValue {
    id: number;
    time: Date;
    value: number;
    balance: number;
    txIndex: number;
}

interface TxValueIterator extends Enumerator<TxValue> {
}

//#endregion

//#region Internal Types and Functions

class GeneratorInput {
    // Adds account-specific psuedo-randomness (prevents identical transactions for each account)
    private readonly salt: number;

    constructor(public readonly ids: IdRegister, public readonly mock: MockFinancialAccount, public readonly startDate: Date,
        public readonly endDate: Date) {
        this.salt = foldString(mock.number + mock.email);
    }

    generateSeed(time: number): number {
        return PseudoRandom.generate(time + this.salt);
    }
}

function FilterFn(evalFn: () => boolean, parent?: () => boolean): () => boolean {
    return () => {
        return (parent == undefined || parent()) && evalFn();
    };
}

function TxValueIterator(input: GeneratorInput): TxValueIterator {
    // A pseudo-randomizing value to use in the seed to ensure different results for different accounts
    const day = DayIterator(input.startDate, input.endDate);
    let txCount = 0;
    const txShifter = DecimalShifter();
    let seed: number;
    let balance = input.mock.openingBalance;

    const flyweight: TxValue = {
        id: undefined,
        time: undefined,
        value: undefined,
        balance: undefined,
        txIndex: undefined
    };

    const resetIt = () => {
        // tslint:disable-next-line: no-use-before-declare
        that.current = undefined;
        balance = input.mock.openingBalance;
    };

    const popValue = () => {
        let v = txShifter.pop(5);
        // skew slightly in favour of credit transactions (we want a gradually increasing balance rather than a decreasing one)
        if (v % 16 < 7) v *= -1;
        return v / 100; // A value of up to 3 places to the left of the decimal point and 3 to the right
    };

    const txStep = () => {
        let r = true;
        // Move to the next day if there are no more transactions in the current one
        if (txCount === 0) {
            r = day.moveNext(); // Exit if there are no more days
            if (r) {
                seed = input.generateSeed(day.current);
                // Init the transaction count shifter with the seed for the current day
                txShifter.reset(seed);
                // Derive a maximum of 3 transactions per day, with the possibility of 0
                txCount = txShifter.pop(2) % 4;
            }
        } else {
            txCount--;
        }

        return r;
    };

    const that = {
        current: undefined,

        moveNext: () => {

            if (txStep()) {
                flyweight.id = input.ids.nextId();
                flyweight.time = day.getDate();
                // For this mock we assume that all currencies represent minor units to 2 decimal places
                flyweight.value = safeAdd2dp(popValue(), 0);
                balance = safeAdd2dp(balance, flyweight.value);
                flyweight.balance = balance;
                that.current = flyweight;
                flyweight.txIndex = 3 - txCount;
            } else {
                resetIt();
            }

            return that.current !== undefined;
        },

        reset: () => {
            day.reset();
            resetIt();
        }
    };

    return that;
}

class TxValueToTxConverter {
    private value: TxValue;
    private isConverted: boolean;
    private isCloned: boolean;
    private shifter = DecimalShifter();
    private prng = new PseudoRandom();

    constructor(private input: GeneratorInput) {

    }

    reset(value: TxValue) {
        this.value = value;
        this.isCloned = false;
        this.isConverted = value.hasOwnProperty("memo");
    }

    get txValue(): TxValue {
        return this.value;
    }

    get tx(): AccountTransaction {
        if (!this.isConverted) this.convert();

        return <any>this.value;
    }

    ensureClone(): any {
        if (this.isCloned) return this.value;
        this.value = cloneEntity(this.value);
        this.isCloned = true;
        return this.value;
    }

    private convert() {
        // We clone because the value that was passed to the reset method is assumed to have been the flyweight
        // instance of a TxValueIterator
        this.value = cloneEntity(this.value);
        const tx = this.value;
        const rootSeed = this.input.generateSeed(tx.time.getTime());
        this.shifter.reset(rootSeed);
        // Step past the pseudo-random seed bits used for earlier transactions on this day
        if (tx.txIndex > 0) this.shifter.pop(2 * tx.txIndex);

        delete tx.txIndex;

        const tx2: AccountTransaction = <any>tx;
        tx2.memo = shortText[Math.floor(PseudoRandom.generate(this.shifter.pop(2)) * 1000) % shortText.length];
        this.prng.reset(Math.trunc(rootSeed + foldString(tx2.memo)));
        tx2.data = [
            {
                type: tx.value >= 0 ? TransactionDataType.PayerAccount : TransactionDataType.PayeeAccount,
                // tslint:disable-next-line: no-use-before-declare
                data: MockTransactionGenerator.generateAccountNumber(this.prng)
            }
        ];

        this.isConverted = true;
        this.isCloned = true;
    }
}

function deriveMaxQueryDate(args: TransactionQueryArgs, upperBound: number): number {
    return args.maxDate ? Math.min(args.maxDate.getTime(), upperBound) : upperBound;
}

/**
 * Generates a seed date one full year prior to the current system date.
 */
function generateSeedDate(): Date {
    let now = new Date(Date.now());
    now = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    now.setFullYear(now.getFullYear() - 1);
    return now;
}

/**
 * Folds a string into a 32-bit integer
 */
function foldString(s: string): number {
    let r = 0;
    let shift = 0;
    for (let idx = 0; idx < s.length; idx++) {
        // Take the lower-order 8 bits (in reality only 7) and shift them progressively to the left;
        // tslint:disable-next-line: no-bitwise
        r ^= (s.charCodeAt(idx) & 127) << shift;
        shift += 8;
        if (shift > 24) shift = 0;
    }

    return r;
}

/**
 * Adds two floating point numbers whilst maintaining two fixed decimal places.
 */
function safeAdd2dp(x: number, y: number): number {
    return Math.round((x + y) * 100) / 100;
}

//#endregion

/*
 * Method:
 * Mock transactions start at the seed date, which is one year prior to the system date at the time the application instance started.
 * Transactions are derived for each query, and NOT retained in memory between queries.
 *
 * For each transaction, a pseudo-random seed is generated from the time at midnight on the transaction date and an account-specific salt.
 * Account balances are aggregated from the seed date.
 * No transactions are generated for the current day. This is to prevent conflicts/inconsistencies with transactions that might be created
 * within the mock session (such as randomly generated receipts and user generated outgoing payments).
 */

/**
 * Generates repeatable transactions for the Mock API
 */
export class MockTransactionGenerator {
    /**
     * Derives a mock account's balance at the specified date. This method is implemented asyncronously to allow the UI to
     * remain responsive during intialization.
     * @param openingBalance The assumed balance of the account prior to the seed date.
     */
    static async deriveAccountBalanceAsync(txIds: IdRegister, mock: MockFinancialAccount): Promise<number> {
        let balance = mock.openingBalance;
        await asynchIterate(TxValueIterator(new GeneratorInput(txIds, mock, SEED_DATE, TERMINAL_DATE)), (tx) => {
            balance = safeAdd2dp(balance, tx.value);
            return false;
        }, TX_BATCH_SIZE);

        return balance;
    }

    static async queryTransactions(mock: MockFinancialAccount, userTransactions: AccountTransaction[],
        args: TransactionQueryArgs): Promise<TransactionQueryResult> {
        args = cloneEntity(args);
        args.pageSize = args.pageSize ? Math.min(Math.max(1, args.pageSize), MAX_PAGE_SIZE) : MAX_PAGE_SIZE;
        if (typeof args.minValue !== "number") args.minValue = undefined;
        if (typeof args.maxValue !== "number") args.maxValue = undefined;
        const minDate = args.minDate ? args.minDate.getTime() : MIN_DATE;
        // Initially, maxDate the default upper-bound for maxDate is the ceiling for auto-generated transactions.
        let maxDate = deriveMaxQueryDate(args, TERMINAL_DATE.getTime());
        // let boundingTxId = args.boundingTransactionId != undefined ? args.boundingTransactionId : Number.MAX_SAFE_INTEGER;
        const text = typeof args.text === "string" ? args.text.toLowerCase().trim() : undefined;

        const input = new GeneratorInput(new IdRegister(mock.seedTxId), mock, SEED_DATE, new Date(maxDate));
        const converter = new TxValueToTxConverter(input);

        let filterFn: () => boolean;

        if (args.direction != undefined) {
            filterFn = FilterFn(() => {
                switch (args.direction) {
                    case "in":
                        return converter.txValue.value >= 0;
                    case "out":
                        return converter.txValue.value < 0;
                }

                return true;
            });
        }

        if (args.minValue != undefined) {
            filterFn = FilterFn(() => {
                return Math.abs(converter.txValue.value) >= args.minValue;
            }, filterFn);
        }

        if (args.maxValue != undefined) {
            filterFn = FilterFn(() => {
                return Math.abs(converter.txValue.value) <= args.maxValue;
            }, filterFn);
        }

        if (text && text.length > 0) {
            filterFn = FilterFn(() => {
                const s = converter.tx.memo;
                return s && s.toLowerCase().indexOf(text) >= 0;
            }, filterFn);
        }

        if (!filterFn) filterFn = () => true;

        let finished = false;

        const candidates: AccountTransaction[] = [];
        const evalFn: (tx: TxValue) => boolean = (tx: TxValue) => {
            const time = tx.time.getTime();
            // The following guard clauses are implemented in order of those most likely to evaluate to true.
            if (time > maxDate) return false;  // We haven't yet entered the specified date range so continue iterating.
            if (time < minDate) return finished = true; // We have exited the specified range so cancel the iteration.
            converter.reset(tx);
            if (!filterFn()) return false; // The transaction was excluded by a filter.
            candidates.push(converter.ensureClone());
            return false; // Continue iterating
        };

        // Generate transactions asynchronously in batches, so that the UI remains responsive in slower browsers.
        await asynchIterate(TxValueIterator(input), (tx) => {
            return evalFn(tx);
        }, TX_BATCH_SIZE);


        if (!finished && userTransactions) {
            // Reevaluate max date to include non-generated user-created transactions.
            maxDate = deriveMaxQueryDate(args, MAX_DATE);
            await asynchIterate(new ArrayEnumerator(userTransactions), (tx) => {
                return evalFn(<any>tx);
            }, TX_BATCH_SIZE);
        }

        let totalPages = Math.trunc(candidates.length / args.pageSize);
        const output: AccountTransaction[] = [];
        if (candidates.length % args.pageSize !== 0) totalPages++;

        if (args.sortAscending === true) {
            const ceiling = Math.min(args.page * args.pageSize + args.pageSize, candidates.length);
            for (let idx = Math.max(ceiling - args.pageSize, 0); idx < ceiling; idx++) {
                converter.reset(<any>candidates[idx]);
                output.push(converter.tx);
            }
        } else {
            const offset = candidates.length - (args.page * args.pageSize) - 1;
            const floor = Math.max(offset - args.pageSize, -1);
            for (let idx = offset; idx > floor; idx--) {
                converter.reset(<any>candidates[idx]);
                output.push(converter.tx);
            }
        }

        const result: TransactionQueryResult = {
            currentPage: args.page,
            totalPages: totalPages,
            totalTransactions: candidates.length,
            transactions: output
        };

        return Promise.resolve(result);
    }

    /**
     * Uses a PseudoRandom instance to generate an account number.
     */
    static generateAccountNumber(prng: PseudoRandom): string {
        let r = String.fromCharCode(48 + prng.next() * 1000 % 10);
        for (let i = 1; i < 16; i++) {
            if (i % 4 === 0) r += "-";
            r += String.fromCharCode(48 + prng.next() * 1000 % 10);
        }

        return r;
    }
}
