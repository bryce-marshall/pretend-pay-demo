import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { AccountTransaction } from "src/app/lib/account-transaction";
import { AccountsApi } from "src/app/lib/api";
import { AccountNumber, FinancialAccount } from "src/app/lib/financial-account";
import { AppTaskManager } from "src/app/services/app-task-manager/app-task-manager";
import { WhenAuthenticatedTask } from "src/app/services/app-task-manager/when-authenticated-task";
import { AppLogger } from "src/app/services/logger/app-logger";
import { AccountsStore, ActiveIdentityStore } from "src/app/store";
import { StoreStateObserver } from "src/app/store/extensions";
import { NotificationStore } from "src/app/store/notification-store";
import { MockAccountsApiService } from "../mock-accounts-api.service";
import { MockTransactionGenerator } from "./mock-transaction-generator";
import { PseudoRandom } from "./pseudo-random";

export interface AutoReceipt {
    accountNumber: AccountNumber;
    accountName: string;
    currencyCode: string;
    transaction: AccountTransaction;
}

@Injectable({
    providedIn: "root",
    deps: [
        AppTaskManager,
        AppLogger,
        ActiveIdentityStore,
        AccountsStore,
        NotificationStore,
        AccountsApi
    ]
})
export class AutoReceiptGenerator extends WhenAuthenticatedTask {
    private _rnd: PseudoRandom = new PseudoRandom(Date.now());
    private _count = 0;
    private _notificationCount: number;
    private _accounts: FinancialAccount[];
    private _onTransaction: Subject<AutoReceipt>;
    onTransaction: Observable<AutoReceipt>;

    constructor(
        taskManager: AppTaskManager,
        private logger: AppLogger,
        identity: ActiveIdentityStore,
        private accountsStore: AccountsStore,
        private notificationStore: NotificationStore,
        private accountsApi: MockAccountsApiService,
    ) {
        super("AutoReceiptGeneratorTask", identity);
        this._notificationCount = 0;
        this._accounts = [];
        this.logger = logger.contextFor(this.name);
        this._onTransaction = new Subject<AutoReceipt>();
        this.onTransaction = this._onTransaction.asObservable();
        this.registerEnabledStateEvaluator(() => {
            // Limit the number of active notifications to approximately 3. The number may exceed 3 because the notification count
            // will only increment upon execution of the NotificationPollerTask, and additional auto-receipts may be generated in
            // between those events.
            return (this._accounts && this._accounts.length > 0) && (this._notificationCount < 4);
        });
        taskManager.addTask(this);
    }

    nextInterval(): number {
        return this._count < 5 ? 5000 : 25000;
    }

    execute(): Promise<any> {
        const account = this._accounts[this._rnd.nextInteger(0, this._accounts.length - 1)];

        let number = this._rnd.nextInteger(0, 99);
        if (number < 50) number = 1;
        else if (number < 85) number = 2;
        else number = 3;

        while (number > 0) {
            number--;
            const fromAccount = MockTransactionGenerator.generateAccountNumber(this._rnd);
            let amount = this._rnd.nextInteger(5, 500);
            let fraction = this._rnd.nextInteger(0, 99);
            const mod = fraction % 10;
            fraction -= mod;
            if (mod >= 5) fraction += 5;
            amount += (fraction / 100);
            const tx = this.accountsApi.autoReceipt(account.number, `Auto-receipt ${++this._count}`, amount, fromAccount);
            this.logger.trace(`Auto-receipt of ${account.currency} ${amount} added to ${account.name}.`);
            this._onTransaction.next({
                accountNumber: account.number,
                accountName: account.name,
                currencyCode: account.currency,
                transaction: tx
            });
        }

        return Promise.resolve();
    }

    @StoreStateObserver()
    protected accountsChanged() {
        this._accounts = this.accountsStore.accounts;
        this.refreshEnabledState();
    }

    @StoreStateObserver()
    protected notificationCountChanged() {
        this._notificationCount = this.notificationStore.notificationCount;
        this.refreshEnabledState();
    }
}
