import { Injectable } from "@angular/core";
import { ApiResponse, ApiResponseCode, KnownApiResponseCode } from "src/app/lib/api-response";
import { PayeesApi } from "src/app/lib/api/payees-api";
import { IdRegister } from "src/app/lib/id-register";
import { SavedPayee } from "src/app/lib/saved-payee";
import { cloneEntity } from "src/app/lib/utilities/entity-clone";
import { VersionedEntity } from "src/app/lib/versioned-entity";
import { asyncSleep } from "./functions";

@Injectable({providedIn: "root"})
export class MockPayeesApiService extends PayeesApi {
    private readonly _payeeIds = new IdRegister();
    private readonly _version = new IdRegister();
    private readonly _payees: SavedPayee[];

    constructor() {
        super();
        this._payees = createMockPayees(this._payeeIds, this._version);
    }

    async getSavedPayees(): Promise<ApiResponse<SavedPayee[]>> {
        await asyncSleep();
        return ApiResponse.resolve(cloneEntity(this._payees));
    }

    async createPayee(payee: SavedPayee): Promise<ApiResponse<SavedPayee>> {
        await asyncSleep();
        const code = this.validate(payee, true);
        if (code !== KnownApiResponseCode.Ok) return ApiResponse.reject(code);

        payee = cloneEntity(payee);
        payee.id = this._payeeIds.nextId();
        payee.entityVersion = this._version.nextId();
        this._payees.push(cloneEntity(payee)); // Clone again so we have a unique instance locally
        return ApiResponse.resolve(payee);
    }

    async updatePayee(payee: SavedPayee): Promise<ApiResponse<SavedPayee>> {
        await asyncSleep();

        const code = this.validate(payee, false);
        if (code !== KnownApiResponseCode.Ok) return ApiResponse.reject(code);
        const idx = this._payees.findIndex((cmp) => {
            return payee.id === cmp.id;
        });

        payee = cloneEntity(payee);
        payee.entityVersion = this._version.nextId();
        this._payees[idx] = cloneEntity(payee); // Clone again so we have a unique instance locally
        return ApiResponse.resolve(payee);
    }

    async deletePayee(payeeId: VersionedEntity<number>): Promise<ApiResponse<SavedPayee>> {
        await asyncSleep();
        const idx = this._payees.findIndex((other) => other.id === payeeId.id);
        if (idx < 0) return ApiResponse.reject(KnownApiResponseCode.PayeeNotExists);
        const cmp = this._payees[idx];
        if (cmp.entityVersion !== payeeId.entityVersion) return ApiResponse.reject(KnownApiResponseCode.ConcurrencyError);
        this._payees.splice(idx, 1);
        return ApiResponse.resolve(cloneEntity(cmp));
    }

    private validate(payee: SavedPayee, isCreate: boolean): ApiResponseCode {
        let cmp = this._payees.find((other) => other.name === payee.name);
        if (cmp && cmp.id !== payee.id) return KnownApiResponseCode.PayeeExists;
        if (isCreate) return KnownApiResponseCode.Ok;

        cmp = this._payees.find((other) => other.id === payee.id);
        if (cmp == undefined) return KnownApiResponseCode.PayeeNotExists;
        if (cmp.entityVersion !== payee.entityVersion) return KnownApiResponseCode.ConcurrencyError;

        // We won't bother with validating other attributes in the mock
        return KnownApiResponseCode.Ok;
    }
}

function createMockPayees(ids: IdRegister, versions: IdRegister): SavedPayee[] {
    return [
        {
            id: ids.nextId(),
            name: "Charlemagne Godsworth",
            payeeType: "email",
            paymentTarget: "magnegod@gmail.com",
            entityVersion: versions.nextId(),
        },
        {
            id: ids.nextId(),
            name: "Junk Online",
            payeeType: "account-number",
            paymentTarget: "4387-9847-0189-3681",
            description: "My favourite online-auction trading house",
            entityVersion: versions.nextId(),
        },
        {
            id: ids.nextId(),
            name: "Spiffy Tiffy",
            payeeType: "email",
            paymentTarget: "tiffany.bradford@snottsworth.com",
            description: "Tiffany from the wine club",
            entityVersion: versions.nextId(),
        },
        {
            id: ids.nextId(),
            name: "The Other Half",
            payeeType: "account-number",
            paymentTarget: "3709-2462-0028-3351",
            entityVersion: versions.nextId(),
        }
    ];
}
