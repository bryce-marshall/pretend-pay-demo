import { Injectable } from "@angular/core";
import { AccountTransaction } from "src/app/lib/account-transaction";
import { AccountsApi } from "src/app/lib/api";
import { ApiResponse, ApiResponseCode, KnownApiResponseCode } from "src/app/lib/api-response";
import { ExecutableTransaction } from "src/app/lib/executable-transaction";
import { ExecutableTransactionResult } from "src/app/lib/executable-transaction.result";
import { AccountNumber, FinancialAccount } from "src/app/lib/financial-account";
import { IdRegister } from "src/app/lib/id-register";
import { TransactionDataType } from "src/app/lib/transaction-data";
import { TransactionQueryArgs } from "src/app/lib/transaction-query-args";
import { TransactionQueryResult } from "src/app/lib/transaction-query-result";
import { Currency } from "src/app/lib/utilities/currency";
import { cloneEntity } from "src/app/lib/utilities/entity-clone";
import { argumentMessage } from "src/app/lib/utilities/error-message";
import { AppLogger } from "src/app/services/logger/app-logger";
import { LifecycleManagedService } from "src/app/services/service-lifecycle-manager";
import { asyncSleep } from "./functions";
import { MockTransactionGenerator } from "./generators/mock-transaction-generator";
import { MockFinancialAccount } from "./mock-finanical-account";

@Injectable({ providedIn: "root" })
@LifecycleManagedService("AuthAPI (mock)")
export class MockAccountsApiService extends AccountsApi {
    private readonly _txIds = new IdRegister();
    private _accounts: MockFinancialAccount[];
    private _userTransactions: { [accountNumber: string]: AccountTransaction[] } = {};

    constructor(private logger: AppLogger) {
        super();
        this.logger = logger.contextFor("MockAccountsApiService");
    }

    async svcOnInit(): Promise<any> {
        return generateAccounts(this._txIds)
            .then((accounts) => {
                this._accounts = accounts;

                return Promise.resolve();
            })
            .catch((e) => {
                this.logger.errorDetail(e, "Error generating mock accounts.");

                return Promise.reject(ApiResponse.as(e));
            });
    }

    async getAccounts(): Promise<ApiResponse<FinancialAccount[]>> {
        await asyncSleep();

        return ApiResponse.resolve(cloneAccounts(this._accounts));
    }

    async queryTransactions(args: TransactionQueryArgs): Promise<ApiResponse<TransactionQueryResult>> {
        const account = this.findAccount(args.account);
        if (!account) return Promise.reject(ApiResponse.error(KnownApiResponseCode.InvalidAccount));

        return MockTransactionGenerator.queryTransactions(account, this._userTransactions[account.number], args)
            .then((r) => {
                return Promise.resolve(ApiResponse.ok(r));
            })
            .catch((e) => {
                this.logger.errorDetail(e, "Error deriving transactions for query.");

                return Promise.reject(ApiResponse.as(e));
            });
    }

    async executeTransaction(transaction: ExecutableTransaction): Promise<ApiResponse<ExecutableTransactionResult>> {
        await asyncSleep();
        try {
            const code = this.validateTransaction(transaction);
            if (code !== KnownApiResponseCode.Ok) return Promise.reject(ApiResponse.as(code));

            const fromAccount = this.findAccount(transaction.fromAccount);
            fromAccount.balance = Currency.trunc(fromAccount.currency, fromAccount.balance - transaction.value);
            fromAccount.available = Currency.trunc(fromAccount.currency, fromAccount.available - transaction.value);

            const tx = new AccountTransaction();
            tx.id = this._txIds.nextId();
            tx.time = new Date();
            tx.value = -transaction.value;
            tx.memo = transaction.memo;
            tx.balance = fromAccount.balance;
            const toAccount = this.findAccount(transaction.payee);
            let tx2: AccountTransaction;

            if (transaction.payeeType === "account-number") {
                if (toAccount) {
                    tx.data = [{ type: TransactionDataType.TransferToAccount, data: toAccount.number }];
                    toAccount.balance = Currency.trunc(toAccount.currency, toAccount.balance + transaction.value);
                    toAccount.available = Currency.trunc(toAccount.currency, toAccount.available + transaction.value);

                    tx2 = new AccountTransaction();
                    tx2.id = this._txIds.nextId();
                    tx2.time = tx.time;
                    tx2.value = transaction.value;
                    tx2.memo = transaction.payeeMemo;
                    tx2.balance = toAccount.balance;
                } else {
                    tx.data = [{ type: TransactionDataType.PayeeAccount, data: transaction.payee }];
                }
            } else {
                tx.data = [{ type: TransactionDataType.PayeeEmail, data: transaction.payee }];
            }

            this.addUserTransaction(fromAccount.number, tx);

            if (tx2) {
                tx2.data = [{ type: TransactionDataType.TransferFromAccount, data: fromAccount.number }];
                this.addUserTransaction(toAccount.number, tx2);
            }

            return ApiResponse.ok(
                {
                    account: cloneEntity(fromAccount),
                    transaction: cloneEntity(tx)
                });
        } catch (e) {
            this.logger.errorDetail(e, "Error executing transaction.");

            return Promise.reject(ApiResponse.as(e));
        }
    }

    autoReceipt(account: AccountNumber, memo: string, value: number, fromAccount: AccountNumber): AccountTransaction {
        const toAccount = this.findAccount(account);
        if (!toAccount) throw new Error(argumentMessage("account", "The target account does not exist."));
        toAccount.balance = Currency.trunc(toAccount.currency, toAccount.balance + value);
        toAccount.available = Currency.trunc(toAccount.currency, toAccount.available + value);
        const tx = new AccountTransaction();
        tx.id = this._txIds.nextId();
        tx.time = new Date();
        tx.value = value;
        tx.memo = memo;
        tx.balance = toAccount.balance;
        tx.data = [{ type: TransactionDataType.PayerAccount, data: fromAccount }];
        this.addUserTransaction(toAccount.number, tx);

        return cloneEntity(tx);
    }

    private validateTransaction(transaction: ExecutableTransaction): ApiResponseCode {
        if (!transaction) return KnownApiResponseCode.InvalidTransaction;
        
        const account = this.findAccount(transaction.fromAccount);

        if (!account) return KnownApiResponseCode.InvalidAccount;
        if (transaction.value <= 0) return KnownApiResponseCode.InvalidTransaction;
        if (account.available < transaction.value) return KnownApiResponseCode.InsufficientFunds;
        if (transaction.payeeType === "email") return KnownApiResponseCode.Ok;
        if (transaction.payeeType !== "account-number") return KnownApiResponseCode.InvalidTransaction;

        const toAccount = this.findAccount(transaction.payee);
        if (!toAccount) return KnownApiResponseCode.Ok;
        if (toAccount.currency !== account.currency) return KnownApiResponseCode.CurrencyMismatch;

        return KnownApiResponseCode.Ok;
    }

    private findAccount(accountNumber: AccountNumber) {
        return this._accounts.find((a) => {
            return a.number === accountNumber;
        });
    }

    private addUserTransaction(accountNumber: AccountNumber, tx: AccountTransaction) {
        let set = this._userTransactions[accountNumber];
        if (!set) {
            set = [];
            this._userTransactions[accountNumber] = set;
        }
        set.push(tx);
    }
}

function cloneAccounts(accounts: FinancialAccount[]): FinancialAccount[] {
    return cloneEntity(accounts, (src, prop) => {
        return prop !== "openingBalance";
    });
}

async function generateAccounts(txIds: IdRegister): Promise<MockFinancialAccount[]> {
    const result = [
        {
            openingBalance: 23954.34,
            number: "8758-4586-3984-3494",
            email: "john@doe.family",
            name: "Personal Account",
            currency: "NZD",
            balance: undefined,
            available: undefined
        },
        {
            openingBalance: 37582.29,
            number: "4456-3943-1106-9315",
            email: "john@doe.co.nz",
            name: "New Zealand Business Account",
            currency: "NZD",
            balance: undefined,
            available: undefined
        },
        {
            openingBalance: 1050.00,
            number: "7491-6001-2836-4109",
            email: "john-ebay@doe.co.nz",
            name: "eBay Account",
            currency: "NZD",
            balance: undefined,
            available: undefined
        },
        {
            openingBalance: 49823.79,
            number: "8331-4765-0475-2104",
            email: "john@doe.eu",
            name: "European Business Account",
            currency: "EUR",
            balance: undefined,
            available: undefined
        }
    ];

    for (let idx = 0; idx < result.length; idx++) {
        const acct: MockFinancialAccount = <any>result[idx];
        acct.seedTxId = txIds.peek();
        acct.balance = await MockTransactionGenerator.deriveAccountBalanceAsync(txIds, acct);
        acct.available = acct.balance;
    }

    return Promise.resolve(<any>result);
}
