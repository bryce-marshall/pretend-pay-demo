export async function asyncSleep(interval?: number): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, interval == undefined ? 1500 : interval);
    });
  }
