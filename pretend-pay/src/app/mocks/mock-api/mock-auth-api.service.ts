import { Injectable } from "@angular/core";
import { AuthApi, LoginData } from "src/app/lib/api";
import { ApiResponse } from "src/app/lib/api-response";
import { AppLogger } from "src/app/services/logger/app-logger";
import { LifecycleManagedService } from "src/app/services/service-lifecycle-manager";
import { asyncSleep } from "./functions";

@Injectable({ providedIn: "root" })
@LifecycleManagedService("AuthAPI (mock)")
export class MockAuthApiService extends AuthApi {

  constructor(private logger: AppLogger) {
    super();
    this.logger = logger.contextFor("MockAuthApiService");
  }

  async login(username: string, password: string): Promise<ApiResponse<LoginData>> {
    return asyncSleep()
      .then(() => {
        let result: Promise<ApiResponse<LoginData>> = this.responseFromEmailAddress(username);

        if (result == undefined) {
          result = Promise.resolve(
            ApiResponse.ok(
              {
                authToken: { token: "", expires: new Date(Date.now() + 1000 * 60 * 5) },
                identity: {
                  id: "f94c7117-22b2-4d81-9eb0-99d288845030",
                  username: username,
                  displayName: undefined
                }
              })
          );
        }

        return result;
      });
  }

  private responseFromEmailAddress<T>(address: string): Promise<ApiResponse<T>> {
    if (address == undefined) return undefined;

    const idx = address.indexOf("@app.error");
    if (idx < 0) return undefined;

    return this.generateErrorResponse(address.substr(0, idx));
  }


  private generateErrorResponse<T>(responseCode: string): Promise<ApiResponse<T>> {
    responseCode = responseCode != undefined ? responseCode.toLowerCase() : "application-error";
    this.logger.trace(`Derived api error response "${responseCode}" from email address.`);

    return Promise.reject(ApiResponse.error(responseCode));
  }
}
