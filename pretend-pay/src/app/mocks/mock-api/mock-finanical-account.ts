import { FinancialAccount } from "src/app/lib/financial-account";

export class MockFinancialAccount extends FinancialAccount {
    openingBalance: number;
    seedTxId: number;
}
