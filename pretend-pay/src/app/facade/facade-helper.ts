import { AppConstants } from "../app-constants";
import { ApiResponse, KnownApiResponseCode } from "../lib/api-response";
import { LoadingStateProvider } from "../lib/loading-state-provider";
import { argumentNullMessage } from "../lib/utilities/error-message";
import { HostedObserverSet, StoreStateObserver } from "../store/extensions";

const POLLING_FREQUENCY = 250;

/**
 * Watches the state of a LoadingStateProvider and resolves or rejects a promise depending upon its outcome.
 */
class LoadingStateWatcher {
    private _observers: HostedObserverSet;

    private constructor(private stateProvider: LoadingStateProvider, private resolve: () => void, private reject: (reason?: any) => void) {
        this._observers = new HostedObserverSet(this);
        const start = Date.now();
        const timeoutFn = () => {
            if (!this._observers) return; // The watcher has been released.
            if (Date.now() - start >= AppConstants.LoadingTimeout) {
                this.release();
                reject(ApiResponse.error(KnownApiResponseCode.Timeout));
            } else {
                // Continue to poll the loading state
                setTimeout(timeoutFn, POLLING_FREQUENCY);
            }
        };

        // Poll the loading state
        setTimeout(timeoutFn, POLLING_FREQUENCY);
    }

    static create(stateProvider: LoadingStateProvider): Promise<void> {
        return new Promise((resolve, reject) => {
            // tslint:disable-next-line: no-unused-expression
            new LoadingStateWatcher(stateProvider, resolve, reject);
        });
    }

    release() {
        HostedObserverSet.dispose(this._observers);
        this._observers = undefined;
    }

    @StoreStateObserver()
    onLoadingStateChanged() {
        // Reference the Mobx observable
        const state = this.stateProvider.loadingState;

        switch (state) {
            case "loaded":
                this.release();
                this.resolve();
                break;

            case "error":
                this.release();
                this.reject();
                break;
        }
    }
}

export class FacadeHelper {
    static awaitLoad(stateProvider: LoadingStateProvider, loadFunction: () => Promise<void>): Promise<void> {
        if (!stateProvider) throw new Error(argumentNullMessage("stateProvider"));
        if (stateProvider.loadingState === "loaded") return Promise.resolve();
        if (stateProvider.loadingState !== "loading") return loadFunction();
        // Observe the loading state and return an appropriate result when it changes.
        return LoadingStateWatcher.create(stateProvider);
    }
}
