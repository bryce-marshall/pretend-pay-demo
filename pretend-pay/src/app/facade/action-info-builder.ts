import { ActionInfo } from "../lib/action-info";
import { ApiResponseCode } from "../lib/api-response";
import { ActionResponse } from "../lib/action-response";

export type ActionNoun = "payee" | "feedback" | "transaction";
export type ActionVerb = "create" | "save" | "delete" | "submit" | "execute";
export type ActionOutcome = string; // = "ok" | "error" | "exists" | "not-exists" | "insufficient-funds";

export class ActionInfoBuilder {
    private _titleResource: string;
    private _props: { [name: string]: any } = {};
    private _path = "action-info";

    constructor(
        private readonly code: ApiResponseCode,
        private outcome?: ActionOutcome
    ) {
    }

    toInfo(): ActionInfo {
        let resourceKey: string;
        // if (!this.outcome && this.code == KnownApiResponseCode.Ok) this.outcome = "ok";
        const resourceKeyBase = this._path + ".";

        if (this.outcome) {
            resourceKey = resourceKeyBase + this.outcome;
        } else {
            resourceKey = resourceKeyBase + outcomeFromResponse(this.code);
        }

        const args = {};
        // tslint:disable-next-line: forin
        for (const key in this._props) {
            args[key] = this._props[key];
        }

        const result: ActionInfo = {
            responseCode: this.code,
            outcomeResource: resourceKey,
            titleResource: this._titleResource ? resourceKeyBase + this._titleResource : undefined,
            args: args
        };

        return result;
    }

    toActionResponse<T>(result?: T): ActionResponse<T> {
        return new ActionResponse(this.toInfo(), result);
    }

    toPromise<T>(result?: T): Promise<ActionResponse<T>> {
        return new ActionResponse(this.toInfo(), result).asPromise();
    }

    setTitleResource(value: string): ActionInfoBuilder {
        this._titleResource = value && value.length > 0 ? value : undefined;
        return this;
    }

    appendPath(value: string): ActionInfoBuilder {
        this._path += ("." + value);
        return this;
    }

    appendProperty(name: string, value: any): ActionInfoBuilder {
        this._props[name] = value;
        return this;
    }
}

function outcomeFromResponse(code: string): string {
    return code ? code : "failed";
}
