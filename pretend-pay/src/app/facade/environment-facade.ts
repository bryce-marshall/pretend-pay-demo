import { Injectable } from "@angular/core";
import { AppEnvironmentObservable, AppViewObservable } from "src/app/store";
import { ActionInfo } from "../lib/action-info";
import { ActionResponse } from "../lib/action-response";
import { LanguageDirection } from "../lib/types";
import { HtmlUtility } from "../lib/utilities/html-utility";
import { ErrorInfo } from "../lib/view/error-info";
import { ToastData } from "../presenters";
import { InternationalizationService } from "../services/i18n/internationalization.service";
import { AppEnvironmentStore, AppViewStore } from "../store";

@Injectable({
    providedIn: "root"
})
export class EnvironmentFacade {
    constructor(
        private environmentStore: AppEnvironmentStore,
        private viewStore: AppViewStore,
        private i18n: InternationalizationService
    ) {
    }

    get dir(): LanguageDirection {
        return this.environmentStore.dir;
    }

    get environmentObservable(): AppEnvironmentObservable {
        return this.environmentStore;
    }

    get viewObservable(): AppViewObservable {
        return this.viewStore;
    }

    setLanguage(languageCode: string) {
        this.environmentStore.setLocale(languageCode);
    }

    enqueueError(errorInfo: ErrorInfo) {
        this.viewStore.enqueueError(errorInfo);
    }

    enqueueToast(item: ToastData | string) {
        this.viewStore.enqueueToast(item);
    }

    enqueueActionToast(info: ActionInfo | ActionResponse<any>) {
        info = ActionInfo.convert(info);
        const outcome = this.i18n.response.getActionInfoTextInstant(info.outcomeResource, info.args);
        this.enqueueToast(HtmlUtility.stripHtml(outcome));
    }

    enqueueActionInfo(info: ActionInfo | ActionResponse<any>) {
        this.viewStore.enqueueInfo(ActionInfo.convert(info));
    }

    removeActionInfo(info: ActionInfo | number) {
        this.viewStore.removeInfo(info);
    }

    clearActionInfo() {
        this.viewStore.clearInfo();
    }

    enqueueActionInfoNext(info: ActionInfo | ActionResponse<any>) {
        this.viewStore.enqueueInfoNext(ActionInfo.convert(info));
    }
}
