import { Injectable } from "@angular/core";
import { AccountFacade } from "./account-facade";
import { AuthFacade } from "./auth-facade";
import { EnvironmentFacade } from "./environment-facade";
import { PayeeFacade } from "./payee-facade";
import { SubscriberFacade } from "./subscriber-facade";
import { NavigationFacade } from "./navigation-facade";

@Injectable({
    providedIn: "root"
})
export class Facade {
    constructor(
        readonly navigate: NavigationFacade,
        readonly account: AccountFacade,
        readonly auth: AuthFacade,
        readonly environment: EnvironmentFacade,
        readonly profile: SubscriberFacade,
        readonly payee: PayeeFacade
    ) {
    }
}
