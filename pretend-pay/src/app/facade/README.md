# Facades

## Definition

Facades expose interfaces to action implementations.

Actions perform operations that change the state of the store. They:

* May or may not involve API invocations
* Commonly involve multiple store objects
* Are commonly used by multiple view-models

## Motivation

Facades:

* Decouple view-models from actions. This eliminates the need for potentially complicated view model inheritance hierarchies arising out of efforts to reuse common action implementations.
* Eliminate direct interdependencies between store objects. This means that individial store objects need only concern themselves with managing their own state consistently, rather than with how any given high-level action might impact application state managed by other store objects.
* Enable view-models and stores to be consistent with the SOLID single responsibility principle, which helps to make code easier to manage by eliminating multiple depenencies and potentially complex inter-dependencies which can often lead to modular circular-reference conflicts.
