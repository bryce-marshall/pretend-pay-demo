import { ComponentType } from "@angular/cdk/portal";
import { Injectable, TemplateRef } from "@angular/core";
import { Router } from "@angular/router";
import { DialogController, DialogInstance } from "src/app/presenters";
import { AccountNumber } from "../lib/financial-account";

@Injectable({
    providedIn: "root"
})
export class NavigationFacade {
    constructor(
        private _router: Router,
        private _dialog: DialogController
    ) {
    }

    async init() {
        return this._router.navigate(["init"]);
    }

    async fatalError() {
        return this._router.navigate(["fatal-error"]);
    }

    async login() {
        return this._router.navigate(["login"]);
    }

    async accounts() {
        return this._router.navigate(["home/accounts"]);
    }

    async transactions(accountNumber: AccountNumber) {
        return this._router.navigate(["home/transactions", accountNumber]);
    }

    async payment() {
        return this._router.navigate(["home/pay-transfer"]);
    }

    async payees() {
        return this._router.navigate(["home/payees"]);
    }

    async editPayee(payeeId: number) {
        return this._router.navigate(["home/edit-payee", payeeId]);
    }

    async createPayee() {
        this._router.navigate(["home/create-payee"]);
    }

    async profile() {
        return this._router.navigate(["home/profile"]);
    }

    openDialog<T, R>(component: ComponentType<T> | TemplateRef<T>, data?: any): DialogInstance<T, R> {
        return this._dialog.openDialog(component, { width: "80%", maxWidth: "800px", data: data });
    }
}

