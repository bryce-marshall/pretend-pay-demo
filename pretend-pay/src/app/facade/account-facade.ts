import { Injectable } from "@angular/core";
import { AccountsApi } from "src/app/lib/api";
import { AccountsObservable } from "src/app/store";
import { AppConstants } from "../app-constants";
import { ActionResponse } from "../lib/action-response";
import { ApiResponse } from "../lib/api-response";
import { ExecutableTransaction } from "../lib/executable-transaction";
import { ExecutableTransactionResult } from "../lib/executable-transaction.result";
import { AccountNumber } from "../lib/financial-account";
import { TransactionQueryArgs } from "../lib/transaction-query-args";
import { TransactionQueryResult } from "../lib/transaction-query-result";
import { UserPaymentType } from "../lib/types";
import { argumentMessage, argumentNullMessage } from "../lib/utilities/error-message";
import { AccountsStore } from "../store";
import { ActionInfoBuilder, ActionOutcome } from "./action-info-builder";
import { FacadeHelper } from "./facade-helper";

@Injectable({
    providedIn: "root"
})
export class AccountFacade {
    constructor(
        private api: AccountsApi,
        private accountsStore: AccountsStore
    ) {
    }

    get observable(): AccountsObservable {
        return this.accountsStore;
    }

    resetAccounts(): Promise<void> {
        this.accountsStore.clearAccounts();
        return this.ensureAccounts();
    }

    ensureAccounts(): Promise<void> {
        return FacadeHelper.awaitLoad(this.accountsStore, () => this.loadAccounts());
    }

    async loadAccounts(): Promise<void> {
        this.accountsStore.setLoadingState("loading");
        try {
            const r = await this.api.getAccounts();
            this.accountsStore.loadAccounts(r.result);
        } catch (e) {
            return Promise.reject(ApiResponse.as(e));
        }
    }

    async executeTransaction(context: UserPaymentType, transaction: ExecutableTransaction):
        Promise<ActionResponse<ExecutableTransactionResult>> {
        try {
            const r = await this.api.executeTransaction(transaction);
            if (r.isOk) {
                // Ensure that the account-refresh task will reload the accounts.
                this.accountsStore.incOutgoingTxCount();
            }
            return createTxInfo(transaction, deriveInfoContext(context), r);
        } catch (e) {
            return createTxInfo(transaction, deriveInfoContext(context), ApiResponse.as(e));
        }
    }

    createDefaultTransactionQueryArgs(accountNumber: AccountNumber): TransactionQueryArgs {
        return {
            account: accountNumber,
            page: 0,
            pageSize: AppConstants.TransactionPageSize
        };
    }

    async queryTransactions(args: TransactionQueryArgs): Promise<ApiResponse<TransactionQueryResult>> {
        if (!args) throw new Error(argumentNullMessage("args"));
        return this.api.queryTransactions(args);
    }
}

function createTxInfo(transaction: ExecutableTransaction, context: string, response: ApiResponse<ExecutableTransactionResult>):
    Promise<ActionResponse<ExecutableTransactionResult>> {
    const r = new ActionInfoBuilder(response.code, getTxOutcome(response))
        .appendPath("transaction")
        .appendPath(context)
        .setTitleResource(response.isOk ? "title" : "title-error");

    if (response.isOk) {
        r.appendProperty("account", response.result.account);
        r.appendProperty("transaction", response.result.transaction);
    } else {
        r.appendProperty("transaction", transaction);
    }

    return r.toPromise(response.result);
}

function getTxOutcome(response: ApiResponse<any>): ActionOutcome {
    return undefined;
}

function deriveInfoContext(context: UserPaymentType): string {
    switch (context) {
        case "account-number":
            return "account";

        case "email":
        case "payee":
        case "transfer":
            return context;

        default:
            throw new Error(argumentMessage("context"));
    }
}
