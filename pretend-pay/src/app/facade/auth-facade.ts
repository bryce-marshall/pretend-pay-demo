import { Injectable } from "@angular/core";
import { AuthApi } from "../lib/api";
import { ActiveIdentityStore } from "../store";
import { ApiResponse, KnownApiResponseCode } from "../lib/api-response";
import { IdentityObservable } from "src/app/store";

@Injectable({
  providedIn: "root"
})
export class AuthFacade {
  private _sessionNonce = 0;

  constructor(private api: AuthApi, private store: ActiveIdentityStore) {
  }

  get observable(): IdentityObservable {
    return this.store;
  }

  async login(username: string, password: string): Promise<ApiResponse<void>> {
    const nonce = ++this._sessionNonce;
    this.store.beginAuthenticating();
    return this.api.login(username, password)
      .then((r) => {
        if (!r.isOk) return ApiResponse.reject(r);
        // Only complete authentication if this attempt is current
        if (nonce !== this._sessionNonce) return ApiResponse.reject(KnownApiResponseCode.ConcurrencyError);
        this.store.authenticated(r.result.identity);
        return ApiResponse.resolve(r.code);
      })
      .catch((e) => {
        this.store.authenticationError(ApiResponse.resolveErrorCode(e));
        return ApiResponse.reject(e);
      });
  }

  logout() {
    this.store.notAuthenticated();
  }
}
