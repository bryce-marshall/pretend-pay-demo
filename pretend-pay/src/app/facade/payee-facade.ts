import { Injectable } from "@angular/core";
import { action } from "mobx";
import { PayeesObservable, PayeesStore } from "src/app/store";
import { ActionResponse } from "../lib/action-response";
import { ApiResponse } from "../lib/api-response";
import { PayeesApi } from "../lib/api/payees-api";
import { SavedPayee } from "../lib/saved-payee";
import { argumentNullMessage } from "../lib/utilities/error-message";
import { ActionInfoBuilder, ActionOutcome, ActionVerb } from "./action-info-builder";
import { FacadeHelper } from "./facade-helper";

@Injectable({
    providedIn: "root"
})
export class PayeeFacade {
    constructor(
        private api: PayeesApi,
        private payeesStore: PayeesStore
    ) {
    }

    get observable(): PayeesObservable {
        return this.payeesStore;
    }

    ensurePayees(): Promise<void> {
        return FacadeHelper.awaitLoad(this.payeesStore, () => this.loadPayees());
    }

    async loadPayees(): Promise<void> {
        this.payeesStore.setLoadingState("loading");
        try {
            const r = await this.api.getSavedPayees();
            this.payeesStore.loadPayees(r.result);

            return Promise.resolve();
        } catch (e) {
            return Promise.reject(ApiResponse.as(e));
        }
    }

    @action async createPayee(payee: SavedPayee): Promise<ActionResponse<SavedPayee>> {
        try {
            const r = await this.api.createPayee(payee);
            payee = r.result;
            this.payeesStore.setPayee(r.result);

            return createPayeeInfo(payee, "create", r);
        } catch (e) {
            return createPayeeInfo(payee, "create", ApiResponse.as(e));
        }
    }

    @action async updatePayee(payee: SavedPayee): Promise<ActionResponse<SavedPayee>> {
        try {
            const r = await this.api.updatePayee(payee);
            payee = r.result;
            this.payeesStore.setPayee(r.result);

            return createPayeeInfo(payee, "save", r);
        } catch (e) {
            return createPayeeInfo(payee, "save", ApiResponse.as(e));
        }
    }

    async deletePayee(payee: SavedPayee): Promise<ActionResponse<SavedPayee>> {
        if (payee == undefined) throw new Error(argumentNullMessage("payee"));
        try {
            const r = await this.api.deletePayee(payee);
            payee = r.result;
            this.payeesStore.deletePayee(payee.id);

            return createPayeeInfo(payee, "delete", r);
        } catch (e) {
            return createPayeeInfo(payee, "delete", ApiResponse.as(e));
        }
    }
}

function createPayeeInfo(payee: SavedPayee, actionVerb: ActionVerb, response: ApiResponse<SavedPayee>):
    Promise<ActionResponse<SavedPayee>> {
    return new ActionInfoBuilder(response.code, getPayeeOutcome(response))
        .appendPath("payee")
        .appendPath(actionVerb)
        .setTitleResource(response.isOk ? "title" : "title-error")
        .appendProperty("payee", payee)
        .toPromise(response.result);
}

function getPayeeOutcome(response: ApiResponse<any>): ActionOutcome {
    return undefined;
}
