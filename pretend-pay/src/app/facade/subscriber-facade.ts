import { Injectable } from "@angular/core";
import { ActionResponse } from "../lib/action-response";
import { SubscriberApi } from "../lib/api";
import { FeedbackData } from "../lib/feedback-data";
import { ApiResponse } from "../lib/api-response";
import { ActionInfoBuilder, ActionVerb, ActionOutcome } from "./action-info-builder";
import { UserProfile } from "../lib/user-profile";
import { NotificationStore } from "../store/notification-store";
import { NotificationsObservable } from "../store/observables/notifications-observable";
import { AppNotification } from "../lib/app-notification";

@Injectable({
    providedIn: "root"
})
export class SubscriberFacade {
    constructor(
        private subscriberApi: SubscriberApi,
        private notificationStore: NotificationStore
    ) {
    }

    get notificationsObservable(): NotificationsObservable {
        return this.notificationStore;
    }

    async getProfile(): Promise<ApiResponse<UserProfile>> {
        try {
            const r = await this.subscriberApi.getProfile();
            return ApiResponse.resolve(r.result);
        } catch (e) {
            return ApiResponse.reject(e);
        }
    }

    async saveProfile(profile: UserProfile): Promise<ActionResponse<UserProfile>> {
        try {
            const r = await this.subscriberApi.saveProfile(profile);
            return profileActionInfo(profile, "save", r);
        } catch (e) {
            return profileActionInfo(profile, "save", ApiResponse.as(e));
        }
    }

    async submitFeedback(data: FeedbackData): Promise<ActionResponse<void>> {
        try {
            const r = await this.subscriberApi.submitFeedback(data);
            return feedbackActionInfo(r);
        } catch (e) {
            return feedbackActionInfo(ApiResponse.as(e));
        }
    }

    dismissNotification(notification: AppNotification | number) {
        this.notificationStore.remove(notification);
    }

    clearNotifications() {
        this.notificationStore.clear();
    }
}

function feedbackActionInfo(response: ApiResponse<any>): Promise<ActionResponse<void>> {
    return new ActionInfoBuilder(response.code)
        .appendPath("feedback.submit")
        .setTitleResource("title")
        .toPromise(response.result);
}

function profileActionInfo(profile: UserProfile, actionVerb: ActionVerb, response: ApiResponse<UserProfile>):
Promise<ActionResponse<UserProfile>> {
    return new ActionInfoBuilder(response.code, getProfileOutcome(response))
        .appendPath("profile")
        .appendPath(actionVerb)
        .setTitleResource(response.isOk ? "title" : "title-error")
        .appendProperty("profile", profile)
        .toPromise(response.result);
}

function getProfileOutcome(response: ApiResponse<any>): ActionOutcome {
    return undefined;
}
