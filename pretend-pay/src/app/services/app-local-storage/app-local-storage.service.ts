/**
 * The base class for injectable application local storage provider implementations.
 */
export abstract class AppLocalStorageService {
  abstract getItem<T>(key: string): Promise<T>;
  abstract setItem<T>(key: string, data: T): Promise<T>;
}
