export { ServiceLifecycleManager, LifecycleManagedService } from "./service-lifecycle-manager";
export {ManagedServiceSet, SvcOnFinalize, SvcOnInit, ServiceEventArgs, EventTypes } from "./interfaces";
