import { Observable, Subject } from "rxjs";
import { ManagedServiceSet, SvcOnInit, SvcOnFinalize, ServiceEventArgs, EventTypes } from "./interfaces";

const MANAGED_TYPE_KEY = "ed4af262f4e947e5b4312f39d05fd145";
const PROXY_KEY = "b23fdd74906e45859e4e3a7a90b003ef";
const AUTO_KEY = "7378e109fed5463881c4ba33f4b102f7";

/**
 * A decorator that marks an injectable type as a service able to be managed by a ServiceLifecycleManager instance.
 */
export function LifecycleManagedService(serviceName: string) {
    return (target) => {
        target[MANAGED_TYPE_KEY] = serviceName;
    };
}
// export const LifecycleManagedService = function (serviceName: string) {
//     return (target) => {
//         target[MANAGED_TYPE_KEY] = serviceName;
//     };
// }

interface LifecycleManagedService extends SvcOnInit, SvcOnFinalize {
    readonly MANAGED_TYPE_KEY: string;
}

/**
 * Used from within AppComponent to manage the lifecycle of application services.
 */
export class ServiceLifecycleManager {
    private _sets: LifecycleManagedService[][] = [];
    private _serviceEvent: Subject<ServiceEventArgs>;
    readonly serviceEvent: Observable<ServiceEventArgs>;

    /**
     * Creates a new AppServiceManager instance
     * @param sets the set of candidate sets containing instances to be evaluated as app services and managed by this instance.
     * Operations upon canditate sets are processed in the order that they were passed to this constructor.
     * Operations upon app-service instances within the same set are processed asynchronously.
     */
    constructor(...sets: ManagedServiceSet[]) {
        this._serviceEvent = new Subject();
        this.serviceEvent = this._serviceEvent.asObservable();
        sets.forEach(set => {
            if (set) {
                const container = [];
                set.forEach(svc => {
                    if (ServiceLifecycleManager.isAppService(svc)) {
                        container.push(svc);
                    }
                });

                if (container.length > 0) {
                    this._sets.push(container);
                }
            }
        });
    }

    /**
     * Wraps any object in a LifecyleManagedService wrapper.
     * @param name The name of the service
     * @param init The function to be invoked during the scvcOnInit phase.
     * @param finalize An optional function to be invoked during the scvcOnFinalize phase.
     */
    static proxy<T>(name: string, service: T, init: (service: T) => Promise<void>, finalize?: (service: T) => Promise<void>): any {
        return {
            [PROXY_KEY]: name,
            svcOnInit: () => {
                return init(service);
            },

            svcOnFinalize: () => {
                // Note that auto-finalizing proxies are released for garbage collection in the onServiceInit method.
                return finalize ? finalize(service) : Promise.resolve();
            }
        };
    }

    /**
     * Wraps any object in a LifecyleManagedService wrapper. The instance will be automatically released for garbage collection immediately
     * following successful execution of the svcOnInit function.
     * @param name The name of the service
     * @param init The function to be invoked during the scvcOnInit phase.
     */
    static autoProxy<T>(name: string, service: T, init: (service: T) => Promise<void>): any {
        const result = ServiceLifecycleManager.proxy(name, service, init);
        result[AUTO_KEY] = true;

        return result;
    }

    /**
     * Returns true if the specified object is an application service; otherwise returns false.
     * @param candidate the instance to test
     */
    static isAppService(candidate: any): boolean {
        return typeof candidate === "object" &&
            (
                candidate.constructor[MANAGED_TYPE_KEY] != undefined ||
                candidate[PROXY_KEY] != undefined
            );
    }

    /**
     * Returns the service name if the specified object is an application service; otherwise returns a default value.
     * @param candidate the instance to test
     */
    static getAppServiceName(candidate: any, defaultValue?: string): string {
        let result: string;
        if (typeof candidate === "object") {
            result = candidate.constructor[MANAGED_TYPE_KEY] ? candidate.constructor[MANAGED_TYPE_KEY] : candidate[PROXY_KEY];
        }

        return typeof result === "string" ? result : typeof defaultValue === "string" ? defaultValue : "(unspecified)";
    }

    /**
     * Invoked by the AppComponent instance to initialize all app services managed by this instance.
     */
    async onServiceInit(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            let idx = 0;
            let failed = false;
            while (!failed && idx < this._sets.length) {
                const candidates = [];
                let svcIdx = 0;
                this._sets[idx].forEach(svc => {
                    // Release auto-finalizing proxies for subsequent garbage collection
                    if (svc[AUTO_KEY] === true) this._sets[svcIdx] = undefined;
                    svcIdx++;

                    if (typeof svc.svcOnInit === "function") {
                        candidates.push(new Promise<void>(async (innerResolve, innerReject) => {
                            await svc.svcOnInit()
                                .then(() => {
                                    innerResolve();
                                    this._serviceEvent.next(createEventArgs("init", svc,
                                        `Initialized the ${ServiceLifecycleManager.getAppServiceName(svc)} service`
                                    ));
                                    if (svc[AUTO_KEY] === true) {
                                        this._serviceEvent.next(createEventArgs("finalize", svc,
                                            `Finalized the ${ServiceLifecycleManager.getAppServiceName(svc)} service`
                                        ));
                                    }
                                })
                                .catch((e) => {
                                    innerReject(e);
                                    this._serviceEvent.next(createEventArgs("init", svc,
                                        `Failed to initialize the ${ServiceLifecycleManager.getAppServiceName(svc)} service`, e));
                                });
                        }));
                    }
                });

                await Promise.all(candidates)
                    .then(() => {
                        idx++;
                    }).catch((e) => {
                        failed = true;
                        reject(e);
                    });
            }

            if (!failed) {
                resolve();
            }
        });
    }

    /**
     * Invoked by the AppComponent instance to finalize all app services managed by this instance.
     * Note that services should typically use the Angular ngOnDestroy lifecycle hook to implement finalization logic.
     * This method is intended for proxied services.
     */
    onServiceFinalize() {
        // Sets are processed in reverse order, in case dependencies affect any services finalization logic.
        // Note that Array.reverse is NOT used here because it is an "in-place" algorithm and mutates the original array.
        // Note also that, unlike in the onServiceInit method which uses Promise.all(), we invoke the svcOnFinalize method for each service.
        // This is because each servic instance must be given the opportunity to finalize irrespective of whether or not the operation
        // succeeded or failed on other services.
        let idx = this._sets.length;
        while (--idx >= 0) {
            this._sets[idx].forEach(async svc => {
                if (typeof svc.svcOnFinalize === "function") {
                    await svc.svcOnFinalize()
                        .then(() => {
                            this._serviceEvent.next(createEventArgs("finalize", svc,
                                `Finalized the ${ServiceLifecycleManager.getAppServiceName(svc)} service`
                            ));
                        })
                        .catch((e) => {
                            this._serviceEvent.next(createEventArgs("finalize", svc,
                                `Failed to finalize the ${ServiceLifecycleManager.getAppServiceName(svc)} service`,
                                e));
                        });
                }
            });
        }
    }
}

function createEventArgs(
    eventType: EventTypes,
    svc: any,
    message: string,
    error?: any

): ServiceEventArgs {
    return {
        eventType: eventType,
        service: svc,
        serviceName: ServiceLifecycleManager.getAppServiceName(svc),
        message: message,
        isError: error != undefined,
        error: error,
    };
}
