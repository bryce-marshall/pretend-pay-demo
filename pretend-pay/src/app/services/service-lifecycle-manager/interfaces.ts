export interface SvcOnInit {
    svcOnInit(): Promise<any>;
}

export interface SvcOnFinalize {
    svcOnFinalize(): Promise<any>;
}

export type EventTypes = "init" | "finalize";

/**
 * Represents a set of candidates for management by an AppServiceManager instance.
 * Service candidate sets provide a simple way of managing inter-service dependencies during service initialisation.
 * During the app-service initialisation process, service candidate sets are processed sequentially; initialisaiton of a
 * set does not commence until each member service of the previous set in the sequence has been initialised successfully.
 * Services that are members of the same set are assumed to have no inter-dependencies within that set, therefore appOnSvcInit()
 * is invoked on those services asynchronously, allowing for concurrent processing to occur when supported by the service.
 */
export interface ManagedServiceSet extends Array<any> {
}

export interface ServiceEventArgs {
    eventType: EventTypes;
    serviceName: string;
    service: any;
    message: string;
    isError: boolean;
    error: any;
}
