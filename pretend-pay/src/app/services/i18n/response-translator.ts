import { ActionInfo } from "src/app/lib/action-info";
import { ActionOutcomeDescriptor } from "src/app/lib/action-outcome-descriptor";
import { ActionResponse } from "src/app/lib/action-response";
import { ApiResponse } from "src/app/lib/api-response";
import { Translator } from "./translator";

const GENERIC_ERROR_KEY = "action-info.generic.APPLICATION-ERROR";
const GENERIC_ERROR_SUBKEY = "application-error";


export class ResponseTranslator extends Translator {
    getActionOutcomeInstant(info: ActionInfo | ActionResponse<any>): ActionOutcomeDescriptor {
        info = ActionInfo.convert(info);
        let res = this.getActionInfoTextInstant(info.outcomeResource, info.args);
        const r: ActionOutcomeDescriptor = {
            outcome: res,
        };

        if (info.titleResource) {
            res = this.getActionInfoTextInstant(info.titleResource, info.args);
            res = this.owner.getTextInstant(info.titleResource);
            // The context key may be defined but the resource undefined, so assign to result only if present.
            if (res && res !== info.titleResource) {
                r.title = res;
            }
        }

        return r;
    }

    /**
     * Attempts to resolve a localized text resource using ActionInfo fallback rules.
     * @param key The resource key
     * @param args The interpolation arguments
     */
    getActionInfoTextInstant(key: string, args?: any): string {
        // Lookup translation, if not exists substitute parent node with "generic" up to but not including the top node ("action-info")
        let result = this.owner.getTextInstant(key, args);
        if (result !== key) return result;

        let pLast = key.lastIndexOf(".");
        if (pLast < 0) return key;
        let pFirst = key.lastIndexOf(".", pLast - 1);
        if (pFirst < 0) return key;

        // Substitute the sub-key "generic" for the immediate parent of the leaf node
        key = key.slice(0, pFirst + 1) + "generic" + key.slice(pLast);
        result = this.owner.getTextInstant(key, args);
        if (result !== key) return result;

        pLast = pFirst;
        pFirst = key.lastIndexOf(".", pLast - 1);
        while (pFirst >= 0) {
            key = key.slice(0, pFirst) + key.slice(pLast);

            result = this.owner.getTextInstant(key, args);
            if (result !== key) {
                pFirst = -1; // exit the loop
            } else {
                pLast = pFirst;
                pFirst = key.lastIndexOf(".", pLast - 1);
            }
        }

        return result;
    }

    getErrorMessageInstant(apiResponse: string | ApiResponse<any>): string {
        let result = this.getActionInfoTextInstant("action-info.generic." + extractErrorCode(apiResponse));
        if (result == undefined || result.length === 0) {
            result = this.owner.getTextInstant(GENERIC_ERROR_KEY);
        }

        return result;
    }

    // getErrorMessageInstant(apiResponse: string | ApiResponse<any>): string {
    //     let result = this.owner.getTextInstant("apiError." + extractErrorCode(apiResponse));
    //     if (result == undefined || result.length === 0) {
    //         result = this.owner.getTextInstant(GENERIC_ERROR_KEY);
    //     }

    //     return result;
    // }

    // async getErrorMessage(apiResponse: string | ApiResponse<any>): Promise<string> {
    //     return this.owner.getText("apiError." + extractErrorCode(apiResponse))
    //         .then((s) => {
    //             if (s != undefined && s.length > 0) return s;
    //             return this.owner.getText(GENERIC_ERROR_KEY);
    //         })
    // }
}

function extractErrorCode(apiResponse: string | ApiResponse<any>): string {
    if (apiResponse == undefined) return GENERIC_ERROR_SUBKEY;

    if (typeof apiResponse !== "string") {
        apiResponse = apiResponse.code;
        if (typeof apiResponse !== "string") {
            apiResponse = GENERIC_ERROR_SUBKEY;
        }
    }

    return apiResponse;
}
