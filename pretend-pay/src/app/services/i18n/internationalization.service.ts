import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { argumentNullMessage } from "src/app/lib/utilities/error-message";
import { AppEnvironmentStore } from "src/app/store";
import { HostedObserverSet, hostedObserverSetFactory } from "src/app/store/extensions";
import { AppLogger } from "../logger/app-logger";
import { LifecycleManagedService } from "../service-lifecycle-manager";
import { Formatter } from "./formatter";
import { LocalizedQuantityDescriptor } from "./localized-quantity-descriptor";
import { ResponseTranslator } from "./response-translator";

/**
 * Provides internationization (i18n) support, including language resource translation and localized formattng, for the application.
 */
@Injectable({
  providedIn: "root"
})
@LifecycleManagedService("InternationalizationService")
export class InternationalizationService {
  private _observers: HostedObserverSet;
  readonly response: ResponseTranslator;
  readonly format: Formatter;

  constructor(
    private logger: AppLogger,
    private translate: TranslateService,
    envStore: AppEnvironmentStore
  ) {
    this.logger = logger.contextFor("InternationalizationService");
    this.response = new ResponseTranslator(this);
    this.format = new Formatter(envStore);
  }

  //#region Service Lifecycle Handlers

  protected svcOnInit(): Promise<void> {
    // This is the application default, rather than the user default which is defined in AppConfig
    this.translate.setDefaultLang("en-US");
    this._observers = hostedObserverSetFactory([this, this.format], this.logger);
    return Promise.resolve();
  }

  protected svcOnFinalize(): Promise<void> {
    HostedObserverSet.dispose(this._observers);
    return Promise.resolve();
  }

  //#endregion

  get localeChange(): Observable<any> {
    return this.translate.onLangChange;
  }

  get currentLocale(): string {
    return this.translate.currentLang;
  }

  getText(key: string, args?: Object): Promise<string> {
    if (key == undefined) throw new Error(argumentNullMessage("key"));
    return this.translate.get(key, args).toPromise();
  }

  getTextInstant(key: string, args?: Object): string {
    if (key == undefined) throw new Error(argumentNullMessage("key"));
    return this.translate.instant(key, args);
  }

  resolveAllKeys(key: string | string[]): any {
    return this.translate.instant(key);
  }

  getQuantityDescriptor(quantity: number): LocalizedQuantityDescriptor {
    // Currently, only English rules are supported
    if (quantity === 1) return "singular";
    return "plural";
  }

  /**
   * A convenience method that resolves an array of literal values and/or Promise<string> values to
   * an array of string. Note that literal strings are NOT interpreted as translation keys, but literal string values to be
   * displayed in the UI.
   * @param values an array of array of literal values and/or Promise<string> values to be resolved to an array of literal string values.
   */
  resolveAll(values: Array<string | Promise<string>>): Promise<string[]> {
    if (values == undefined || values.length === 0) return Promise.resolve([]);
    return new Promise((resolve, reject) => {
      let i = 0;
      let count = 0;
      const result = new Array<string>(values.length);
      values.forEach(v => {
        if (typeof (v) === "string") {
          result[i++] = v;
          count++;
          if (count === values.length) {
            resolve(result);
          }
        } else {
          ((v1, i1) => {
            v1
              .then((r) => {
                result[i1] = r;
                count++;
                if (count === values.length) {
                  resolve(result);
                }
              })
              .catch((e) => {
                count = -1;
                reject(e);
              });

          })(v, i++);
        }
      });
    });
  }
}
