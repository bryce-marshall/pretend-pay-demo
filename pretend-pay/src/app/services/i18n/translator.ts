import { InternationalizationService } from "./internationalization.service";

/**
 * The base class for all context-specific translators.
 */
export abstract class Translator {
    constructor(protected readonly owner: InternationalizationService) {
    }
}
