/**
 * Represents the grammatical rule that should be applied when describing a quantity in a specific locale.
 */
export type LocalizedQuantityDescriptor = "zero" | "singular" | "plural" | "many";
