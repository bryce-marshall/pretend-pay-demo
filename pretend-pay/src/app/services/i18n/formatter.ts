import { AppEnvironmentStore } from "src/app/store";
import { StoreStateObserver } from "src/app/store/extensions";

export class Formatter {
    private _currencies: { [code: string]: CurrencyFormatter } = {};
    private _currencyFormatter: CurrencyFormatter;
    private _currentLocale: string;
    private _dateFormat: Intl.DateTimeFormat;

    constructor(
        private envStore: AppEnvironmentStore) {
    }

    currency(value: number, currencyCode: string): string {
        this.ensureCurrency(currencyCode);
        return this._currencyFormatter.format(value);
    }

    shortDate(value: Date): string {
        return this._dateFormat ? this._dateFormat.format(value) : value.toDateString();
    }

    //#region Observable Properties

    @StoreStateObserver()
    protected onLocaleChanged() {
        this._currentLocale = this.envStore.locale;
        // Reset the formatters if the locale has changed
        this._currencyFormatter = undefined;
        this._currencies = {};

        this._dateFormat = undefined;
        if (Intl && Intl.DateTimeFormat) {
            this._dateFormat = new Intl.DateTimeFormat(this._currentLocale);
        }
    }

    //#endregion

    private ensureCurrency(code: string) {
        if (this._currencyFormatter && this._currencyFormatter.code === code) return;

        this._currencyFormatter = this._currencies[code];
        if (!this._currencyFormatter) {
            this._currencyFormatter = createCurrencyFormatter(this._currentLocale, code);
            this._currencies[code] = this._currencyFormatter;
        }
    }
}

interface NumberFormatter {
    format(value: number): string;
}

interface CurrencyFormatter extends NumberFormatter {
    code: string;
}

function createCurrencyFormatter(locale: string, currencyCode: string): CurrencyFormatter {
    const f: NumberFormatter = Intl && Intl.NumberFormat ?
        // Use the Internationalization API if it is supported.
        new Intl.NumberFormat(locale, { style: "currency", currency: currencyCode }) :
        // Otherwise if the Internationalization API is not supported by the client,
        // default to number.toString(), which will output a number
        // using the basic formatting of the client application's current locale.
        {
            format: (value: number) => {
                return value.toString();
            }
        };

    return {
        code: currencyCode,
        format: (value: number) => {
            // Some browsers (notably, Chrome) will insert currency codes when the currency is not native to the locale
            // (for example NZ$100.00 or US$100 or 100,00 $US). We want to format with the symbol only, so remove the code.
            return f.format(value).replace(/[A-Za-z]+/, "");
        }
    };
}
