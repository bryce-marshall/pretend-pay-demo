import { Injectable } from "@angular/core";
import { NgForage } from "ngforage";
import { AppLocalStorageService } from "../app-local-storage/app-local-storage.service";

/**
 * An application local storage provider implementation that encapsulates the NgForage (an Angular localForage wrapper) library.
 */
@Injectable()
export class ForageProviderService extends AppLocalStorageService {
  constructor(private readonly ngf: NgForage) {
    super();
  }

  getItem<T>(key: string): Promise<T> {
    return this.ngf.getItem<T>(key);
  }

  setItem<T>(key: string, data: T): Promise<T> {
    return this.ngf.setItem(key, data);
  }
}
