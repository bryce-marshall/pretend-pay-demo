import { isDevMode } from "@angular/core";

export enum LogType {
    Debug,
    Trace,
    Audit,
    Error
}

/**
 * The base class for injectable application logger implementations.
 */
export abstract class AppLogger {
    static traceEnabled = true;

    protected get context(): string {
        return "";
    }

    contextFor(contextName: string): AppLogger {
        // tslint:disable-next-line: no-use-before-declare
        return new ContextualLogger(contextName, this);
    }

    debug(header: string, ...args) {
        // Only write to the debug log in development mode
        if (isDevMode()) {
            this._write(LogType.Debug, header, args);
        }
    }

    trace(header: string, ...args: any[]) {
        if (AppLogger.traceEnabled) {
            this._write(LogType.Trace, header, args);
        }
    }

    audit(header: string, ...args: any[]) {
        this._write(LogType.Audit, header, args);
    }

    error(header: string, ...args: any[]) {
        this._write(LogType.Error, header, args);
    }

    debugDetail(details: string[], header?: string, ...args: any[]) {
        // Only write to the debug log in development mode
        if (isDevMode()) {
            this._write(LogType.Debug, header, args);
        }
    }

    traceDetail(details: string[], header?: string, ...args: any[]) {
        if (AppLogger.traceEnabled) {
            this._write(LogType.Trace, header, args,);
        }
    }

    auditDetail(details: string[], header?: string, ...args: any[]) {
        this._write(LogType.Audit, header, args);
    }

    errorDetail(details: string[] | Error | { toString: () => string }, header?: string, ...args: any[]) {
        const t: string[] = [];
        if (details && !Array.isArray(details)) {
            if (details instanceof Error) {
                details = expandError(details);
            } else if (details && typeof details.toString === "function") {
                details = [details.toString()];
            } else {
                details = undefined;
            }
        }

        this._write(LogType.Error, header, args);
    }

    private _write(logType: LogType, header: string, args: any[]) {
        this.write(this.context, logType, header, args);
    }

    protected abstract write(context: string, logType: LogType, header: string, args: any[]);
}

function expandError(e): string[] {
    const result: string[] = [];

    if (!(e instanceof Error)) return result;

    const err = <Error>e;
    result.push(err.name);
    result.push(err.message);
    result.push(err.stack);

    return result;
}

class ContextualLogger extends AppLogger {

    constructor(private localContext: string, private root: AppLogger) {
        super();
    }

    get context(): string {
        return this.localContext;
    }

    protected write(context: string, logType: LogType, header: string, details: string[]) {
        (<any>this.root).write(context, logType, header, details);
    }
}
