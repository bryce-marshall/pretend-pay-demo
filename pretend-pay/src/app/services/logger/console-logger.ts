import { Injectable } from "@angular/core";
import { AppLogger, LogType } from "./app-logger";
import { LifecycleManagedService } from "../service-lifecycle-manager";

/**
 * An application logger implementation that writes to the runtime environment's console.
 */
@Injectable()
@LifecycleManagedService("ConsoleLogger")
export class ConsoleLogger extends AppLogger {

    protected svcOnInit(): Promise<void> {
        return Promise.resolve();
    }

    protected write(context: string, logType: LogType, header: string, details: any[]) {
        let prefix: string;
        switch (logType) {
            case LogType.Trace:
                prefix = "TRACE: ";
                break;

            case LogType.Audit:
                prefix = "AUDIT: ";
                break;

            case LogType.Error:
                prefix = "ERROR: ";
                break;

            default:
                prefix = "DEBUG: ";
                break;
        }

        if (context) prefix = prefix + "[" + context + "] ";

        if (header) {
            console.log(prefix + header);
        } else {
            console.log(prefix);
        }

        if (details && details.length > 0) {
            console.log("------ log entry details start ------");

            details.forEach(s => {
                console.log(s);
            });

            console.log("------  log entry details end  ------");
        }
    }
}
