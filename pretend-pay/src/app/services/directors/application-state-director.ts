import { Injectable, OnDestroy } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { AppStateType, LanguageDirection } from "src/app/lib/types";
import { NotificationStore } from "src/app/store/notification-store";
import { AccountsStore, ActiveIdentityStore, AppEnvironmentStore, AppViewStore, PayeesStore } from "../../store";
import { HostedObserverSet, hostedObserverSetFactory, StoreStateObserver } from "../../store/extensions";
import { AppLogger } from "../logger/app-logger";
import { LifecycleManagedService } from "../service-lifecycle-manager";

@Injectable({
    providedIn: "root"
})
@LifecycleManagedService("ApplicationStateDirector")
export class ApplicationStateDirector implements OnDestroy {
    private _observers: HostedObserverSet;
    private _localAppState: AppStateType;

    constructor(
        private translate: TranslateService,
        private logger: AppLogger,
        private accounts: AccountsStore,
        private identity: ActiveIdentityStore,
        private environment: AppEnvironmentStore,
        private view: AppViewStore,
        private notification: NotificationStore,
        private payees: PayeesStore
    ) {
        this.logger = logger.contextFor("ApplicationStateDirector");
    }

    //#region Service Lifecycle Handlers

    protected svcOnInit(): Promise<any> {
        // The state should already be "initializing" by default, but just in case
        this.environment.setState("initializing");
        this._observers = hostedObserverSetFactory(this, this.logger);

        return Promise.resolve();
    }

    ngOnDestroy() {
        HostedObserverSet.dispose(this._observers);
    }

    //#endregion

    //#region Store State Observers

    @StoreStateObserver()
    protected onAppStateChanged() {
        this._localAppState = this.environment.state;
        this.logger.trace(`The application state has changed to "${this._localAppState}"`);
        switch (this._localAppState) {
            case "error":
                this.identity.notAuthenticated();
                this.resetStores();
                break;
        }
    }

    @StoreStateObserver()
    protected onAuthStateChanged() {
        const state = this.identity.state;
        this.logger.trace(`The ActiveIdentity state has changed to "${state}".`);

        switch (state) {
            case "authenticated":
                break;

            case "authenticating":
                break;

            case "error":
                break;

            case "expired":
                // TODO: Should attempt to refresh credentials in a real environment
                break;

            case "not-authenticated":
            case undefined:
                this.resetStores();
                break;
        }
    }

    @StoreStateObserver()
    protected onLocaleChanged() {
        this.translate.use(this.environment.locale).toPromise().then(() => this.environment.setDir(this.getDir()));
        this.logger.trace(`The application locale was changed to "${this.environment.locale}".`);
    }

    @StoreStateObserver()
    protected onDirChanged() {
        this.logger.trace(`The application language direction was changed to "${this.environment.dir}".`);
    }

    //#endregion

    private resetStores() {
        this.accounts.reset();
        this.environment.reset();
        this.identity.reset();
        this.view.reset();
        this.notification.reset();
        this.payees.reset();
        this.logger.trace("Reset the application store state.");
    }

    private getDir(): LanguageDirection {
        const dir = this.translate.instant("dir");
        return (dir === "ltr" || dir === "rtl") ? dir : "ltr";
    }
}
