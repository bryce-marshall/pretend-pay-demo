import { Injectable, OnDestroy } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterEvent, RoutesRecognized } from "@angular/router";
import { fromEvent } from "rxjs";
import { NavigationFacade } from "src/app/facade/navigation-facade";
import { SubscriptionManager } from "src/app/lib/subscription-manager";
import { AppStateType, IdentityStateType, ViewContext, ViewportType } from "src/app/lib/types";
import { DialogController, ToastController } from "src/app/presenters";
import { ErrorModalViewComponent } from "src/app/views/error-modal-view/error-modal-view.component";
import { ActiveIdentityStore, AppEnvironmentStore, AppViewStore } from "../../store";
import { HostedObserverSet, hostedObserverSetFactory, StoreStateObserver } from "../../store/extensions";
import { AppLogger } from "../logger/app-logger";
import { LifecycleManagedService } from "../service-lifecycle-manager";

const MIN_TOAST_DURATION = 3000;
const MAX_TOAST_DURATION = 10000;

@Injectable({
    providedIn: "root",
    deps: [
        Router, AppLogger, NavigationFacade, ActiveIdentityStore, AppEnvironmentStore, AppViewStore, DialogController, ToastController
    ]
})
@LifecycleManagedService("ViewDirector")
export class ViewDirector implements OnDestroy {
    private _subscriptions: SubscriptionManager = new SubscriptionManager();
    private _observers: HostedObserverSet;
    private _viewport: ViewportType;
    private _activeToast = false;
    private _authState: IdentityStateType;
    private _appState: AppStateType;

    constructor(
        private router: Router,
        private logger: AppLogger,
        private navigate: NavigationFacade,
        private identity: ActiveIdentityStore,
        private environment: AppEnvironmentStore,
        private view: AppViewStore,
        private dialog: DialogController,
        private toastController: ToastController
    ) {
        this.logger = logger.contextFor("ViewDirector");
        this._subscriptions.subscribeTo(this.router.events, (e: RouterEvent) => {
            if (e instanceof RoutesRecognized) {
                this.view.shiftActionInfo();
                const context = resolveViewContext((<any>e).state.root);
                if (this.view.viewContext !== context) {
                    this.view.setContext(context);
                    this.logger.trace(`Set the active view context to "${context}"`);
                }
            }
        });
    }

    //#region Service Lifecycle Handlers

    protected svcOnInit(): Promise<any> {
        this._observers = hostedObserverSetFactory(this, this.logger);
        if (window) {
            this.updateViewport();
            this._subscriptions.subscribeTo(fromEvent(window, "resize"), () => {
                this.updateViewport();
            });
        } else {
            this.view.setViewport("tablet");
        }
        return Promise.resolve();
    }

    private updateViewport() {
        const value: ViewportType = window.innerWidth >= 700 ? "tablet" : "phone";
        if (value !== this._viewport) this.view.setViewport(value);
    }

    ngOnDestroy() {
        HostedObserverSet.dispose(this._observers);

        this._subscriptions.unsubscribe();
    }

    //#endregion

    //#region Store State Observers

    @StoreStateObserver()
    protected viewportTypeChanged() {
        this.logger.trace(`The application viewport type has changed to "${this.view.viewportType}"`);
    }

    @StoreStateObserver()
    protected onAppStateChanged() {
        this._appState = this.environment.state;
        switch (this._appState) {
            case "initialized":
                if (this._authState === "authenticated") {
                    this.navigate.accounts();
                    this.logNavEvent("accounts", this.environment.state);
                }
                break;

            case "initializing":
                this.navigate.init();
                this.logNavEvent("init", this.environment.state);
                break;

            case "error":
                this.navigate.fatalError();
                this.logNavEvent("fatal-error", this.environment.state);
                break;
        }
    }

    @StoreStateObserver()
    protected onAuthStateChanged() {
        this._authState = this.identity.state;
        switch (this._authState) {
            case "authenticated":
                if (this._appState === "initialized") {
                    this.navigate.accounts();
                    this.logAuthNavEvent("accounts", this.identity.state);
                }
                break;

            case "not-authenticated":
                this.navigate.login();
                this.logAuthNavEvent("login", this.identity.state);
                break;
        }
    }

    @StoreStateObserver()
    errorQueueCountChanged() {
        if (this.view.errorCount === 0) return;
        this.showError();
    }

    @StoreStateObserver()
    toastCountChanged() {
        if (this.view.toastCount === 0 || this._activeToast) return;
        this.showToast();
    }

    private logNavEvent(target: string, state: AppStateType) {
        this.logger.trace(`Navigated to ${target} in response to the application state changing to "${state}".`);
    }

    private logAuthNavEvent(target: string, state: IdentityStateType) {
        this.logger.trace(`Navigated to ${target} in response to the authentication state changing to "${state}".`);
    }

    private showError() {
        const info = this.view.dequeueError();
        if (!info) return;

        if (info) {
            const subscription = this.dialog.openDialog(ErrorModalViewComponent, {
                data: info,
                width: "60%",
                maxWidth: "700px"
            }).afterClosed().subscribe((instance) => {
                subscription.unsubscribe();
                this.logger.trace("A modal error window was closed");
            });

            this.logger.trace("A modal error window was opened");
        }
    }

    private showToast() {
        this._activeToast = true;
        const data = this.view.dequeueToast();
        data.duration = data.duration != undefined ?
            Math.max(Math.min(data.duration, MAX_TOAST_DURATION), MIN_TOAST_DURATION) : MIN_TOAST_DURATION;

        const subscription = this.toastController.createToast(data).afterDismissed().subscribe(
            () => {
                this._activeToast = false;
                subscription.unsubscribe();
                this.logger.trace("A toast instance was closed");

                // Toast is displayed one at a time. If there are further queued toast items, display the next one
                if (this.view.toastCount > 0) {
                    setTimeout(() => {
                        if (this.view.toastCount === 0 || this._activeToast) return;
                        this.showToast();
                    }, 0);
                }
            }
        );

        this.logger.trace("A toast instance was opened");
    }

    //#endregion
}

function resolveViewContext(route: ActivatedRouteSnapshot): ViewContext {
    let context: ViewContext = "other";

    while (route != undefined) {
        if (route.data && route.data.viewContext) context = route.data.viewContext;
        route = route.firstChild;
    }

    return context;
}
