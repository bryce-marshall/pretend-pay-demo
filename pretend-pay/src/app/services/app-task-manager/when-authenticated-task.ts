import { AppTask } from "./app-task";
import { IdentityObservable } from "src/app/store";
import { StoreStateObserver } from "src/app/store/extensions";

export abstract class WhenAuthenticatedTask extends AppTask {
    constructor(name: string, private identity: IdentityObservable) {
        super(name);
        this.registerEnabledStateEvaluator(() => {
            return this.identity.isAuthenticated;
        });
    }

    @StoreStateObserver()
    protected authStateChanged() {
        // tslint:disable-next-line: no-unused-expression
        this.identity.isAuthenticated; // We must reference the mobx property
        this.refreshEnabledState();
    }
}
