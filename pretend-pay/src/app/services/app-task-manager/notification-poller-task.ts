import { WhenAuthenticatedTask } from "./when-authenticated-task";
import { ActiveIdentityStore } from "src/app/store";
import { Injectable } from "@angular/core";
import { SubscriberApi } from "src/app/lib/api";
import { NotificationStore } from "src/app/store/notification-store";
import { from } from "rxjs";
import { map, max } from "rxjs/operators";
import { AppLogger } from "../logger/app-logger";

@Injectable({ providedIn: "root" })
export class NotificationPollerTask extends WhenAuthenticatedTask {
    private _interval = 1000;
    private _lastId: number;

    constructor(
        private logger: AppLogger,
        private api: SubscriberApi,
        private store: NotificationStore,
        identity: ActiveIdentityStore
    ) {
        super("NotificationPollerTask", identity);
        this.logger = logger.contextFor(this.name);
    }

    nextInterval(): number {
        return this._interval;
    }

    async execute(): Promise<void> {
        this._interval = 30000;
        const response = await this.api.getNotifications(this._lastId);
        this.logger.trace("Polling the server for user notifications.");
        if (!response.isOk) return Promise.reject(response);

        from(response.result).pipe(
            map(ev => ev.id),
            max()
        ).subscribe(id => this._lastId = Math.max(id, this._lastId));

        if (response.result && response.result.length > 0) {
            this.logger.trace(`Retrieved ${response.result.length} notifications from the server.`);
            this.store.push(...response.result);
        }

        return Promise.resolve();
    }
}
