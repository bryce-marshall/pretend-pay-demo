import { WhenAuthenticatedTask } from "./when-authenticated-task";
import { ActiveIdentityStore, AccountsStore } from "src/app/store";
import { Injectable } from "@angular/core";
import { AccountsApi } from "src/app/lib/api";
import { NotificationStore } from "src/app/store/notification-store";
import { StoreStateObserver } from "src/app/store/extensions";
import { from } from "rxjs";
import { filter, map, max } from "rxjs/operators";
import { SyncState } from "src/app/lib/types";
import { ApiResponse } from "src/app/lib/api-response";
import { AppLogger } from "../logger/app-logger";

@Injectable({ providedIn: "root" })
export class AccountRefreshTask extends WhenAuthenticatedTask {
    private _lastNotificationId = 0;
    /** Represents the  current version of the accounts view. */
    private _version = 0;
    /** Represents the pending version of the accounts view. */
    private _pendingVersion = 0;
    /**
     * Represents the pending version when the account store loading state changes to "loading".
     */
    private _loadingVersion = 0;
    /**
     * A local copy of the account store's sync state.
     * A local copy is necessary because we read sync state within unrelated MobX action handlers (with StoreStateObserver decorators -
     * indirectly  via this.outOfSync()) and reading sync state directly from the store would cause these handlers to also fire when
     * sync state changed.
     */
    private _syncState: SyncState;

    constructor(
        private logger: AppLogger,
        identity: ActiveIdentityStore,
        private api: AccountsApi,
        private notificationStore: NotificationStore,
        private accountsStore: AccountsStore) {
        super("AccountRefreshTask", identity);
        this.logger = logger.contextFor(this.name);
        this.registerEnabledStateEvaluator(() => this.accountsStore.syncState !== "syncing");
    }

    nextInterval(): number {
        return 3000;
    }

    async execute(): Promise<void> {
        let result: Promise<void>;
        if (this._version === this._pendingVersion) {
            if (this.accountsStore.syncState !== "in-sync") this.accountsStore.setSyncState("in-sync");
            result = Promise.resolve();
        } else {
            this.accountsStore.setSyncState("syncing");
            // Assign the current pending version to a local variable, as it might change during the asynchronous getAccounts invocation.
            const nextVersion = this._pendingVersion;
            let exitState: SyncState;
            try {
                this.logger.trace(`Syncing the accounts.`);
                const response = await this.api.getAccounts();
                if (response.isOk) {
                    this.accountsStore.loadAccounts(response.result);
                    this._version = nextVersion;
                    exitState = this._version === this._pendingVersion ? "in-sync" : "out-of-sync";
                    result = Promise.resolve();
                } else {
                    exitState = "out-of-sync";
                    result = Promise.reject(response);
                }
            } catch (e) {
                this.logger.errorDetail(e, "Error syncing the accounts");
                exitState = this._version === this._pendingVersion ? "in-sync" : "out-of-sync";
                result = ApiResponse.rejectVoid(e);
            }
            finally {
                this.accountsStore.setSyncState(exitState);
            }
        }

        return result;
    }

    @StoreStateObserver()
    protected notificationsChanged() {
        from(this.notificationStore.notifications).pipe(
            filter(ev => ev.notificationType === "account-receipt"),
            map(ev => ev.id),
            max()
        ).subscribe(id => {
            if (id > this._lastNotificationId) {
                this._lastNotificationId = id;
                // tslint:disable-next-line: max-line-length
                this.logger.trace(`One or more account-receipt notifications indicate that the local accounts view is out-of-sync with the server.`);
                this.outOfSync();
            }
        });
    }

    @StoreStateObserver()
    protected outgoingTxCountChanged() {
        if (this.accountsStore.outgoingTxCount > 0) {
            this.logger.trace(`An outgoing transaction caused the local accounts view to be out-of-sync with the server.`);
            this.outOfSync();
        }
    }

    @StoreStateObserver()
    syncStateChanged() {
        this._syncState = this.accountsStore.syncState;
        this.logger.trace(`The accounts store sync state changed to "${this._syncState}".`);
        this.refreshEnabledState();
    }

    @StoreStateObserver()
    loadingStateChanged() {
        switch (this.accountsStore.loadingState) {
            case "loading":
                this._loadingVersion = this._pendingVersion;
                break;

            case "loaded":
                if (this._loadingVersion === this._pendingVersion) {
                    this._version = this._pendingVersion;
                    this.accountsStore.setSyncState("in-sync");
                }
                break;
        }
    }

    private outOfSync() {
        this._pendingVersion++;
        if (this._syncState !== "syncing") this.accountsStore.setSyncState("out-of-sync");
    }
}
