import { BackgroundTask } from "src/app/lib/background-task-manager/background-task";
import { HostedObserverSet } from "src/app/store/extensions";

export abstract class AppTask extends BackgroundTask {
    private _observers: HostedObserverSet;

    constructor(name: string) {
        super(name);
    }

    init() {
        this._observers = new HostedObserverSet(this);
        super.init();
    }

    destroy() {
        HostedObserverSet.dispose(this._observers);
        super.destroy();
    }

    get observers(): HostedObserverSet {
        return this._observers;
    }
}
