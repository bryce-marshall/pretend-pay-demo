import { BackgroundTaskManager } from "src/app/lib/background-task-manager";
import { Injectable, OnDestroy } from "@angular/core";
import { LifecycleManagedService } from "../service-lifecycle-manager";
import { AppLogger } from "../logger/app-logger";

@Injectable(
    {
        providedIn: "root"
    }
)
@LifecycleManagedService("AppTaskManager")
export class AppTaskManager extends BackgroundTaskManager implements OnDestroy {
    constructor(
        logger: AppLogger
    ) {
        logger = logger.contextFor("AppTaskManager");
        super();
        this.onTaskError.subscribe((args) => {
            logger.errorDetail(args.error, "An error occurred executing the background task \"{0}\"", args.taskName);
        });
    }

    protected svcOnInit(): Promise<void> {
        this.enabled = true;
        return Promise.resolve();
    }

    ngOnDestroy() {
        this.destroy();
    }
}
