import { Observable } from "rxjs";

export interface ToastInstance {
    readonly isClosed: boolean;
    afterDismissed(): Observable<any>;
    close();
}
