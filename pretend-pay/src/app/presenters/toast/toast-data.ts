export interface ToastData {
    text: string;
    duration?: number;
}
