import { Injectable } from "@angular/core";
import { ToastImp, ToastInstanceHandle } from "../implementors/toast-imp";
import { ToastInstance } from "./toast-instance";
import { ToastData } from "./toast-data";
import { Observable } from "rxjs";

@Injectable(
    {
        providedIn: "root",
        deps: [ToastImp]
    }
)
export class ToastController {
    private _handle: any;

    constructor(private imp: ToastImp) {
        this._handle = imp.getControllerHandle();
    }

    createToast(data: ToastData): ToastInstance {
        return EnclosedToastInstance(this.imp, this.imp.createInstance(this._handle, data));
    }
}

function EnclosedToastInstance(imp: ToastImp, instance: ToastInstanceHandle): ToastInstance {
    return {
        get isClosed(): boolean {
            return imp.isInstanceClosed(instance);
        },
        afterDismissed(): Observable<any> {
            return imp.getInstanceObservable(instance, "AfterDismissed");
        },
        close: () => {
            imp.closeInstance(instance);
        }
    };
}
