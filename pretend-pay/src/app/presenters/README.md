# Presenters

Presenters are presentation layer abstractions, the intent of which is to ensure that application presentation management components (such as view-models) do not have specfic presentation framework dependencies.

Presenters utilise the Bridge pattern to abstract components such as modal dialog controllers from their underlying presentation framework implementations.

Such abstraction is useful for scenarios such as:

* Unit testing 
* Deployment to multiple platforms

Deployment to multiple platforms would require core application logic to be packaged as separate modules, and would typically require views to be recreated in projects for each supported platform (for example: Material Design, Bootstrap, Nativescript, Ionic, etc).