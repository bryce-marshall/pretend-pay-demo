import { ToastData } from "../toast/toast-data";
import { Observable } from "rxjs";

export type ToastControllerHandle = any;
export type ToastInstanceHandle = any;
export type ToastObservableType = "AfterDismissed";

export abstract class ToastImp {
    abstract getControllerHandle(): ToastControllerHandle;
    abstract releaseController(handle: ToastControllerHandle);
    abstract createInstance(controller: ToastControllerHandle, data: ToastData): ToastInstanceHandle;
    abstract closeInstance(handle: ToastInstanceHandle);
    abstract isInstanceClosed(handle: ToastInstanceHandle): boolean;
    abstract getInstanceObservable(handle: ToastInstanceHandle, type: ToastObservableType): Observable<any>;
}
