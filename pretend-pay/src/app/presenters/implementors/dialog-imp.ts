import { TemplateRef } from "@angular/core";
import { ComponentType } from "@angular/cdk/portal";
import { Observable } from "rxjs";
import { DialogConfig } from "../dialog/dialog-config";

export type DialogControllerHandle = any;
export type DialogInstanceHandle = any;
export type DialogObservableType = "AfterClosed";

export abstract class DialogImp {
    abstract getControllerHandle(): DialogControllerHandle;
    abstract releaseController(handle: DialogControllerHandle);
    abstract createInstance(controller: DialogControllerHandle,
        componentOrTemplateRef: ComponentType<any> | TemplateRef<any>, data: DialogConfig<any>): DialogInstanceHandle;
    abstract closeInstance(handle: DialogInstanceHandle, result?: any);
    abstract isInstanceClosed(handle: DialogInstanceHandle): boolean;
    abstract getInstanceObservable(handle: DialogInstanceHandle, type: DialogObservableType): Observable<any>;
    abstract getComponentInstance(handle: DialogInstanceHandle): any;
}
