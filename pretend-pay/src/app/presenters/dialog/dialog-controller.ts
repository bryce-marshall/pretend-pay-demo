import { ComponentType } from "@angular/cdk/portal";
import { Injectable, TemplateRef } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { Stack } from "src/app/lib/collections/stack";
import { DialogImp } from "../implementors/dialog-imp";
import { DialogConfig } from "./dialog-config";
import { DialogInstance } from "./dialog-instance";

let GLOBAL_DIALOG_ID = 0;

@Injectable({
    providedIn: "root",
    deps: [DialogImp]
})
export class DialogController {
    private _handle: any;
    private _stack = new Stack<InternalDialogRef>();

    constructor(private imp: DialogImp) {
        this._handle = imp.getControllerHandle();
    }

    openDialog<T, R>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, config?: DialogConfig<any>): DialogInstance<T, R> {

        let subscription: Subscription;
        try {
            // We must push the DialogRef onto the stack before creating the dialog instance, because the component constructor
            // (which may inject it) is invoked before this.imp.createInstance returns. We utilise a proxy to manage the dialog instance.
            // Client code references the dialog instance indirectly through the proxy, which will throw if the instance has not yet
            // been assigned.
            const proxy = DialogProxy();
            this._stack.push(ProxiedDialogRef(proxy, config ? config.data : undefined));
            const instance = this.imp.createInstance(this._handle, componentOrTemplateRef, config);
            proxy.setDialog(instance);
            const r = <DialogInstance<T, R>>EnclosedDialogInstance(this.imp, proxy);
            subscription = this.imp.getInstanceObservable(instance, "AfterClosed").subscribe(() => {
                // Remove this instance from the stack.
                // We don't simply pop in case a stacked dialog has been programmatically closed out of sequence.
                this._stack.remove((cmp) => {
                    return cmp.dialog === instance;
                });
                subscription.unsubscribe();
            });
            return r;
        } catch (e) {
            this._stack.pop();
            if (subscription) subscription.unsubscribe();
            throw e;
        }
    }
}

function EnclosedDialogInstance<T, R>(imp: DialogImp, proxy: DialogProxy): DialogInstance<T, R> {
    const instance = proxy.getDialog();
    return {
        get component(): T {
            return imp.getComponentInstance(instance);
        },
        get isClosed(): boolean {
            return imp.isInstanceClosed(instance);
        },
        afterClosed(): Observable<R> {
            return imp.getInstanceObservable(instance, "AfterClosed");
        },
        close: (result?: R) => {
            imp.closeInstance(instance, result);
        }
    };
}

interface DialogProxy {
    readonly id: number;
    getDialog(): DialogInstance<any>;
    setDialog(d: DialogInstance<any>);
}

function DialogProxy(): DialogProxy {
    let instance: DialogInstance<any>;

    return {
        id: ++GLOBAL_DIALOG_ID,
        getDialog() {
            // tslint:disable-next-line: max-line-length
            if (instance == undefined) throw new Error("The current dialog reference is not yet available. Try referencing it in ngOnInit instead.");
            return instance;
        },

        setDialog(d: DialogInstance<any>) {
            instance = d;
        }
    };
}

interface InternalDialogRef {
    readonly id: number;
    readonly data: any;
    readonly dialog: DialogInstance<any>;
}

function ProxiedDialogRef(proxy: DialogProxy, data: any): InternalDialogRef {
    return {
        get id() {
            return proxy.id;
        },

        get data() {
            return data;
        },

        get dialog() {
            return proxy.getDialog();
        }
    };
}

@Injectable()
export class DialogRef<T = any> {
    private _proxy: InternalDialogRef;

    constructor(controller: DialogController) {
        const stack: Stack<InternalDialogRef> = (<any>controller)._stack;
        if (stack.count === 0) throw new Error("There is no active dialog.");
        this._proxy = stack.peek();
        // tslint:disable-next-line: max-line-length
        if (this._proxy.id !== GLOBAL_DIALOG_ID) throw new Error("The DialogRef is not associated with the active dialog. Ideally it should be injected into the dialog component (or one of its peers).");
    }

    get id(): number {
        return this._proxy.id;
    }

    get data(): T {
        return this._proxy.data;
    }

    get dialog(): DialogInstance<T> {
        return this._proxy.dialog;
    }
}
