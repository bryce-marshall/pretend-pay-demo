import { Observable } from "rxjs";

export interface DialogInstance<T, R = any> {
    component: T;
    readonly isClosed: boolean;
    afterClosed(): Observable<R>;
    close();
}
