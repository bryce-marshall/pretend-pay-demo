import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MomentDateAdapter } from "@angular/material-moment-adapter";
import { DateAdapter } from "@angular/material/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { AppCommonModule } from "./app-common.module";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AccountsApi, AuthApi, PayeesApi, SubscriberApi } from "./lib/api";
import { MaterialModule } from "./material.module";
import { MaterialDialog, MaterialToast } from "./material/presenters";
import { MockAccountsApiService, MockAuthApiService, MockPayeesApiService, MockSubscriberApiService } from "./mocks/mock-api";
import { FatalErrorPageComponent } from "./pages/fatal-error-page/fatal-error-page.component";
import { InitPageComponent } from "./pages/init-page/init-page.component";
import { DialogImp, ToastImp } from "./presenters/implementors";
import { InternationalizationService } from "./services/i18n/internationalization.service";
import { AppLogger } from "./services/logger/app-logger";
import { ConsoleLogger } from "./services/logger/console-logger";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const _components: any[] = [
  AppComponent,
  InitPageComponent,
  FatalErrorPageComponent,
];

@NgModule({
  declarations: _components,
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MaterialModule,
    AppRoutingModule,
    AppCommonModule.forRoot(),
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter },
    { provide: ToastImp, useClass: MaterialToast },
    { provide: DialogImp, useClass: MaterialDialog },

    { provide: AppLogger, useClass: ConsoleLogger },
    InternationalizationService,
    { provide: AuthApi, useExisting: MockAuthApiService },
    { provide: AccountsApi, useExisting: MockAccountsApiService },
    { provide: SubscriberApi, useExisting: MockSubscriberApiService },
    { provide: PayeesApi, useExisting: MockPayeesApiService },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
