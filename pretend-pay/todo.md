# Rename "formControls" property to "controls" on all view components (rename in markup too)

# Refactoring

# ErrorInfo to ActionInfo

Examine relationship between ErrorInfo and ActionInfo. Do we need both? Can ErrorInfo be constructed from ActionInfo?
ErrorInfo title is probably unnecessary; generic "An error has occurred" is sufficient
Both types expose a response code
Could introduce context and detail resource keys to ActionInfo (example - 'Save Payee Failed', and 'The Payee "John Smith" already exists')

# ActionInfo on ViewStore

The actionInfo and actionInfoNext fields probably do not need to be observable

# ViewModels

Should be little or no need for most mobx decorators in most view models. State watchers will work on any property that returns a mobx observable/computed decorated field or property, so indirectly referencing the underlying store will work in most cases.

In other cases, it is only necessary to duplicate the view-model property on the view when the view may modify it (via a form control).
